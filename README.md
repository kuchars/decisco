[logo]: images/cisco.jabber.png

# What is decisco

![Cisco Jabber Logo][logo]

Decisco is a small application that adds **SSO/WebexToken** authentication to an existing **Jabber** client.
It is actually a man in the middle layer between ordinary **Jabber** client and **Cisco XMPP server**.

On the local end it setup simple XMPP server that perform authentication on your behalf towards Cisco server.
After authentication is done it start data forwarding between your jabber client and Cisco server.

Cisco XMPP server is not exactly 100% compatible with open source clients so some of the features like contact
information required a translation. It can be enabled with **-e** or **--experimental** options.

# How to start?
- determine authentication method you will be using by reading [authentication methods](#system-requirements).
- choose your installation/starting solution by reading [installation](#running).
- configure your jabber client - read [client configuration](#client-configuration)
- if you plan to use native python installation test it before manually starting decisco in a terminal.

You can also try use **install.py** script to do manual runs and finalize your preferred setup.

# System requirements
System requirements depends on what authentication method you plan to use. Currently decisco supports 4 methods,
each has it's own pros and cons. Supported python versions are from 3.4 to 3.6.

- web
    - It's a pure python implementation, uses your default system browser for authentication (should work in all cases intranet, vpn, internet).
    - Decisco uses **xdg-open** command to open http://localhost:8080, so whatever mechanism you choose to run it probably needs access to your **xsession**.
    - Automatic passing of username and password to webbrowser is not implemented at the moment so at least first time you need to pass those manually (this implies -P doesn't do much).
    - Should work with systemd (--user), your desktop environment start scripts, pidgin plug-in or simple manual run. Running through docker won't work by default.
- qt (qt4 or qt5)
    - It's uses **PyQt4** or **PyQt5** packages to build a webbrowser that will do SSO authentication.
    - This method also requires you run it under **xsession**.
    - Automatic passing of user credentials might work but it's also possible it's outdated and doesn't work anymore.
    - Same running options as for web method.
- file
    - SSO authentications is not performed automatically, you need to open provided link manually, authenticate and copy resulting xml to decisco or to a file which is passed through **--xml** option.
    - Only logical running method to use is to run deciso manually.
    - It's rather a last resort solution.

## Operating System

- All Linux systems with epoll

# Running
After getting Decisco up and running proceed to [client configuration](#client-configuration).

You can also try to install Decisco using **install.py** script.

## Systemd service
Requirements:

- Decisco sources
- Linux with systemd
- Installed supported python
- Extra packages installed depending which authentication method you plan to use

Installing and starting service:

```shell
$ mkdir -p ~/.config/systemd/user
$ cp systemd-service/decisco.service ~/.config/systemd/user/
$ vim ~/.config/systemd/user/decisco.service # edit your configuration default content is invalid!
$ systemctl --user enable decisco
$ systemctl --user start decisco
```

for more details read [README](systemd-service/README) and [runtime options](#runtime-options).

## Manual
Requirements:

- Decisco sources
- Installed supported python
- Extra packages installed depending which authentication method you plan to use

Running / starting decisco (example):

```shell
./decisco.py -Pe
```

You might need to pass additional options to make it work best for you - for that see [runtime options](#runtime-options) and [authentication methods](#system-requirements).

## Desktop Environment auto-start
Requirements:

- Pidgin client
- Decisco sources
- Installed supported python
- Extra packages installed depending which authentication method you plan to use
- Desktop environment that supports auto-start of **~/.config/autostart/*.desktop** as specified by *freedesktop.org*.

Use **install.py** script and choose *"manual"* run and say yes to question *Add following command to your desktop autostart*

# Runtime Options
For full list of options check output of **--help**

1. Authentication options
   * -P or --use-client-password - use corporate password directly from your jabber client (you also need to set proper username to make it work). **This is the suggested way of configuring decisco and also default**.
   * -u or --username - this supply your corporate username (by default taken from **USER** environment variable - as long as your Linux user name is the same as your corporate user name you don't need to supply it)
   * -m qt or --method qt - start a browser window to authenticate yourself - by default **web** method is used.
   * -x /path/to/xmltokens.xml or --xml /path/to/xmltokens.xml - provide the authentication tokens yourself, by copying the XML response from your browser (with -x, takes it from the file, otherwise asks for it at the prompt) requires -m file or --method file.
   * -c or --credentials - location of credential files similar to samba credential file

     ```
     username=your_username
     password=your_password
     ```

     credential file supply your corporate user name and password so nothing else is needed

     If you only supply username decisco will ask for your password later through terminal, kdialog, zenity or Xdialog.

2. Translation of xmpp communication
   * -e or --experimental - this enable additional both way translation to support user information retrieval from Cisco server.

3. Miscellaneous settings
   * --use-syslog - redirect logs to syslog
   * -l or --log-file - redirect logs to a file
   * -v or --verbose - enable debug logs
   * -w or --wait-4-socket - wait until local bind operation is successful while address reuse is not allowed.
   * -t or --timeout - change default timeout on decisco <-> Cisco server communication - might be useful on slower connections

3. Proxy settings
   * --proxy-host - default value **10.144.1.10**
   * --proxy-port - default value **8080**

   those options will be used for connecting to Cisco XMPP server and so called swapi.

4. Local XMPP server settings
   * --local-xmpp-host - default value **localhost**
   * --local-xmpp-port - default value **5222**

   options that determine local xmpp server port

# Clients - supported features

| Client Name | File Transfer        | Group Charts | Kontact info (vcard) | Search for users | Screenshot from CJ |
|-------------|----------------------|--------------|----------------------|------------------|--------------------|
| Telepathy   | Sometimes works (-e) | sometimes    | works(-e)            | works(-e)        | ?                  |
| Pidgin      | Sometimes works      | works        | works(-e)            | works(-e)        | WIP(-eE)           |
| Jitsi       | ?                    | ?            | ?                    | ?                | ?                 |
| Psi         | ?                    | ?            | ?                    | ?                | ?                 |
| Kopete      | ?                    | ?            | ?                    | ?                | ?                 |

# Client Configuration

Server address: localhost.

Server post: 5222 - unless you selected different via **--local-xmpp-port**.

Allow an encryption-less connection, do not use proxy and allow plain text authentication.
