#!/usr/bin/env python3
import os
import sys
import logging
import unittest
import coverage


logging.basicConfig(
    level=logging.ERROR+100,
    format='%(asctime)s %(levelname)8s: %(lineno)4s %(name)s.%(message)s',
    datefmt='%m-%d %H:%M:%S')


this_dir = os.path.abspath(os.path.dirname(__file__))
argv = [sys.argv[0], 'discover', '-s', this_dir, '-p', '*TS.py'] + sys.argv[1:]
cov = coverage.Coverage()
try:
    cov.start()
    unittest.main(argv=argv, module=None)
finally:
    cov.stop()
    cov.save()
    cov.html_report(directory='covhtml')
