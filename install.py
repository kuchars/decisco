#!/usr/bin/env python3
import os
import sys
import getpass
import logging
import subprocess
import decisco.args


def run(cmd, success=None, errour=True):
    if not hasattr(subprocess, 'DEVNULL'):
        subprocess.DEVNULL = open(os.devnull)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=None if errour else subprocess.DEVNULL)
    out, err = p.communicate()
    if p.returncode == 0:
        if callable(success):
            return success(out)
        else:
            return out.decode('utf-8', errors='ignore')
    return False


def rinput(question, default):
    return input(question + ' (%s):' % default) or default


class Opt(object):
    def __init__(self, *args, **kwargs):
        object.__init__(self)
        self.args = args
        self.kwargs = kwargs
        self.dest = kwargs.get('dest')

    def name(self):
        default = ''
        defv = self.kwargs.get('default', None)
        action = self.kwargs.get('action', 'store')
        if action in {'store', 'append', 'toggle'} and defv is not None:
            default = ', \33[35mdefault: %s\33[0m' % defv
        if action in ['store_true', 'store_false'] and defv:
            default = ', \33[34menabled by default\33[0m'
        if 'help' in self.kwargs:
            default += ', help: %s' % self.kwargs['help'].replace('%default', str(defv))
        return '/'.join(self.args) + default

    def match(self, names):
        return bool(set(self.args).intersection(names))

    def need_value(self):
        action = self.kwargs.get('action', 'store')
        return action in ['store', 'append']

    def default(self):
        ret = self.kwargs.get('default', None)
        if ret is None:
            action = self.kwargs.get('action', 'store')
            if action == 'store_true':
                ret = True
            elif action == 'store_false':
                ret = False
        return ret

    def get_value(self, selected_names: set, selected_values: dict):
        is_default = True
        ret = self.kwargs.get('default', None)
        action = self.kwargs.get('action', 'store')
        selected_value = None
        for x in self.args:
            if x in selected_values:
                is_default = False
                selected_value = selected_values[x]
            if x in selected_names:
                if action == 'toggle':
                    ret = not ret
                    is_default = False
        ret = selected_value or ret
        return ret, is_default


class Container(object):
    def __init__(self, *args, **kwargs):
        object.__init__(self)
        self.options = []

    def add_argument(self, *args, **kwargs):
        self.options.append(Opt(*args, **kwargs))

    def add_argument_group(self, grp, *args):
        return self

    def parse_args(self, *args, namespace=None, **kwargs):
        return namespace


class Installer(object):
    YES = set(['Y', 'y', '1', 'yes'])

    def __init__(self):
        object.__init__(self)
        self.fake_parser = Container()
        decisco.args.PARSER_OBJ = self.fake_parser
        self.current_user = os.environ['USER']
        self.selected_user = self.current_user
        self.decisco_options = ['--cisco-features']
        self.decisco_directory = os.path.abspath(os.path.dirname(__file__))
        decisco.args.get_options_and_args()
        self.setup()

    def setup(self):
        import decisco.sso
        import decisco.sso.args
        import decisco.sso.loader
        import decisco.forward

    def options_str(self) -> str:
        return ' '.join(self.arguments_array())

    def arguments_array(self) -> list:
        return [("%s=%s" % x if isinstance(x, tuple) else x) for x in self.decisco_options]

    def get_decisco_cmd(self) -> list:
        return [os.path.join(self.decisco_directory, 'decisco.py')] + self.arguments_array()

    def add_option(self, arg, *val):
        if val:
            self.decisco_options.append((arg, val[0]))
        else:
            self.decisco_options.append(arg)

    def rem_option(self, opt):
        for i, x in enumerate(self.decisco_options):
            if isinstance(x, tuple):
                x = x[0]
            if x in opt.args:
                del self.decisco_options[i]
                break

    def get_opt_names(self) -> set:
        names = set()
        for v in self.decisco_options:
            if isinstance(v, tuple):
                names.add(v[0])
            else:
                names.add(v)
        return names

    def get_opt_name_and_value_map(self) -> dict:
        ret = {'--username': self.selected_user}
        for v in self.decisco_options:
            if isinstance(v, tuple):
                ret[v[0]] = v[1]
        return ret

    def autostart_cmd(self, cmd) -> bool:
        cmd = ' '.join(cmd)
        if rinput("Add following command to your desktop autostart: %s [y|n]?" % cmd, 'y') not in self.YES:
            return False
        dst_file = os.path.abspath(os.path.expanduser('~%s/.config/autostart/decisco.desktop' % self.selected_user))
        print("Creating file: %s" % dst_file)
        try:
            os.makedirs(os.path.dirname(dst_file))
        except OSError:
            pass
        with open(dst_file, "w") as f:
            f.write("[Desktop Entry]\n")
            f.write("Type=Application\n")
            f.write("Exec=%s\n" % cmd)
            f.write("Hidden=false\n")
            f.write("NoDisplay=false\n")
            f.write("X-GNOME-Autostart-enabled=true\n")
            f.write("Name=Decisco\n")
            f.write("Comment=Decisco Auto Start Script\n")
        return True

    def ask_run(self, cmd) -> bool:
        ccmd = ' '.join(cmd)
        if rinput("Do you like to run following script: '%s' [y|n]?" % ccmd, 'n') not in self.YES:
            return False
        ret = run(cmd)
        if ret is False:
            return False
        print(ret)
        return True

    def do_systemd(self) -> int:
        user_dir = os.path.abspath(os.path.expanduser('~%s' % self.selected_user))
        sdu = os.path.join(user_dir, '.config/systemd/user')
        try:
            os.makedirs(sdu)
        except OSError:
            pass
        with open(os.path.join(os.path.dirname(__file__), 'systemd-service/decisco.service'), 'r') as f:
            data = f.read()
        data = data.replace("<path_to_decisco>", self.decisco_directory).replace("<options>", self.options_str())
        with open(os.path.join(sdu, 'decisco.service'), 'w') as f:
            f.write(data)
        w = [run(['systemctl', '--user', 'enable', 'decisco']), run(['systemctl', '--user', 'start', 'decisco'])]
        if False in w:
            print("Failed to enable and start systemd service")
            return 1
        return 0

    def do_desktop_autostart(self) -> int:
        return 0 if self.autostart_cmd(self.get_decisco_cmd()) else 1

    def do_manual_start(self) -> int:
        return 0 if self.ask_run(self.get_decisco_cmd()) else 1

    def do_manual(self) -> int:
        def default_instaler():
            print("Execution method %s is not supported" % rmeth)
            return 1

        print("Using decisco from current workspace, choose how to run it:")
        print("  1) systemd")
        print("  2) desktop environment autostart")
        print("  3) manual")
        rmeth = rinput("Select execution method", '1')
        installer_map = {
            '1': self.do_systemd,
            '2': self.do_desktop_autostart,
            '3': self.do_manual_start}
        return installer_map.get(rmeth, default_instaler)()

    def set_credential_file(self):
        print("Set up credential file")
        cred_file = os.path.abspath(os.path.expanduser('~%s/.ssh/cred' % self.selected_user))
        with open(cred_file, 'w') as f:
            f.write("username=%s\n" % self.selected_user)
            f.write("password=%s\n" % getpass.getpass("Provide password for %s user:" % self.selected_user))
        self.add_option('--credentials', cred_file)

    def mod_options(self):
        if rinput("Modify options? [y|n]", 'N') not in self.YES:
            return
        print("Options:")
        while True:
            names = self.get_opt_names()
            for i, x in enumerate(self.fake_parser.options):
                y = '\33[32m, option already selected\33[0m' if x.match(names) else ''
                print("  %02d) %s%s" % (i, x.name(), y))
            print("  \33[33m-1) end option modification\33[0m")
            opt = int(rinput("Select option to add", '-1'))
            if opt == -1:
                break
            option = self.fake_parser.options[opt]
            if option.match(names):
                self.rem_option(option)
                continue
            nam = option.args[-1]
            if '--credentials' in option.args:
                self.set_credential_file()
            elif '--alu' in option.args:
                self.add_option('--alu')
            else:
                if option.need_value():
                    value = rinput("Provide value for %s" % option.name(), option.default())
                    self.add_option(nam, value)
                else:
                    self.add_option(nam)

    def create_configuration_file_impl(self, f):
        selected_names = self.get_opt_names()
        selected_values = self.get_opt_name_and_value_map()
        f.write('import decisco.args\n')
        f.write('import decisco.sso.credentials as dcred\n\n\n')
        f.write('def configure(opt: decisco.args.DeciscoArgs):\n')
        f.write('    assert isinstance(opt.cred_obj, dcred.Credentials)\n')
        for opt in self.fake_parser.options:
            name = opt.dest
            if name == 'config_file':
                continue
            value, is_default = opt.get_value(selected_names, selected_values)
            if name == 'organization':
                f.write('    opt.set_organization(%r)\n' % value)
                continue
            pre = ''
            if is_default:
                pre = '# '
            f.write('    %sopt.%s = %r\n' % (pre, name, value))

    def create_configuration_file(self):
        if rinput("Do you like to create configuration file[y|n]?", 'n') not in self.YES:
            return False
        default_file_name = os.path.expanduser('~/decisco_config.py')
        default_file_name = rinput("Select configuration file:", default_file_name)
        with open(default_file_name, 'wt') as f:
            self.create_configuration_file_impl(f)
        self.decisco_options = []
        self.add_option('--config-file', default_file_name)

    def run(self) -> int:
        default = 'Y'
        if self.current_user == 'root':
            print("Run me as a normal user!")
            return 1
        print("This script will help you install, setup and run decisco")
        print("Assuming your system environment is compatibile with decisco")
        self.selected_user = rinput("What's your corporate login", self.current_user)
        organization = rinput("Select organization [corporate domain]", None)
        if organization is None:
            print("Organization is required - exiting")
            return 1
        organization = organization.strip()
        self.add_option('--organization', organization)
        if self.selected_user != self.current_user:
            self.add_option('--username', self.selected_user)
        self.mod_options()
        self.create_configuration_file()
        print("Options that will be passed to decisco: %s" % self.options_str())
        return self.do_manual()


try:
    sys.exit(Installer().run())
except KeyboardInterrupt:
    sys.exit(1)
