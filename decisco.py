#!/usr/bin/env python3
'''
Author: lukasz.kucharski@nokia.com

HOW IT WORKS:

Scripts shows Nokia SSO authentication page (use your standard login and
password). After succesfull login and when nothing gone wrong a local xmpp
server will be available to connect only one client (use you favorite jabber
client :) Kopete and Pidgin seems to work ).

To configure jour client use:
your_name@nsn.com as jabber id
localhos - as server (make sure your client is able to use non secure
    connection
authentication - allow plaint text authentication
password - set whatever you want - real authentication is done by SSO earlier

Whats needed:
python 2.7.9
pycurl/libcurl-7.19.1
BeautifulSoup
PyQt4/PyQt5

Software without any warranty.

'''
import os
import sys
import logging
import datetime
ver = sys.version_info
ver = (ver.major, ver.minor, ver.micro)
if ver < (3, 0, 0):
    logging.fatal("Only python 3.x is supported")
    sys.exit(1)
import importlib
import decisco.args
decisco.args.early_setup()
import decisco.sso
import decisco.sso.args
import decisco.sso.loader
import decisco.forward


def configure(options: decisco.args.DeciscoArgs, log):
    if options.config_file is None:
        decisco.sso.loader.make_auth_object(options)
        return
    config_file = os.path.abspath(options.config_file)
    if not os.path.exists(config_file) or not os.path.isfile(config_file):
        log.fatal("Invalid configuration file: %s", config_file)
        sys.exit(1)
    sys.path.append(os.path.dirname(config_file))
    module_name = os.path.splitext(os.path.basename(config_file))[0]
    log.info("Loading module: %s", module_name)
    config_module = importlib.import_module(module_name)
    options.cred_obj = decisco.sso.credentials.Credentials(options)
    config_module.configure(options)
    options.cred_obj.load_data(not options.use_client_password)


def setup():
    opt = decisco.args.get_options_object()
    decisco.sso.args.load_handler(opt)
    options = decisco.args.get_options_and_args()
    log = opt.get_logger()
    log.info("Use at your own risk")
    if options.username is None:
        options.username = os.environ.get('USER', None)
    configure(options, log)
    if options.organization is None:
        logging.fatal("Organization must be set")
        sys.exit(1)
    options.use_proxy = True
    options.connected_directly = False
    decisco.utils.get_environ_proxy(options)
    return options


def make_log_backup(options):
    if not options.log_file:
        return
    import shutil
    import datetime
    date = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
    des_file = '%s_%s.err.log' % (options.log_file, date)
    shutil.copyfile(options.log_file, des_file)


def main(options):
    # local xmpp server - accepts only one connections and starts forwarding
    # data after authentication is OK
    try:
        xmpp = decisco.forward.XmppForwarder(options)
        xmpp.run()
        # run_again = False
    except decisco.forward.SomethingGoneTottalyWrong:
        # run_again = True
        # options.wait_for_socket = True
        make_log_backup(options)
    except Exception:
        logging.exception("Decisco failed")
        raise


if __name__ == '__main__':
    main(setup())

