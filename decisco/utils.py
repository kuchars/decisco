import re
import os
import socket
import struct
import logging
import collections

import base64
try:
    from urllib import quote
    from urllib import unquote
    from urllib import urlencode
except Exception:
    from urllib.parse import quote
    from urllib.parse import unquote
    from urllib.parse import urlencode
try:
    import urlparse
except Exception:
    import urllib.parse as urlparse
__all__ = [
    'get_input', 'urlencode', 'unquote',
    'get_environ_proxy', 'test_connectivity', 'get_url_hostname',
    'decode_client_password', 'get_python_file_and_mtime', 'get_mtime'
    ]


def get_mtime(filename):
    return os.path.getmtime(filename)


def get_python_file_and_mtime(module) -> tuple:
    r = module.__file__
    ar = r.rsplit('.', 1)
    if len(ar) > 1 and ar[1] != 'py':
        ar[1] = 'py'
        r = '.'.join(ar)
    return r, get_mtime(r)


def mangle_password(text, field_name="PASSWORD="):
    password_pattern = re.compile('%s([^&]*)' % field_name)
    m = password_pattern.search(text)
    if m is not None:
        s, e = m.start(1), m.end(1)
        text = text[:s] + '*' * (e - s) + text[e:]
    return text


def get_input(prompt):
    try:
        usr = raw_input(prompt)
    except Exception:
        usr = input(prompt)


def get_environ_proxy(options):
    try:
        https_proxy = os.environ.get('https_proxy')
        if https_proxy is None:
            return
        m = re.match("https?://([^:]+):(\d+)", https_proxy)
        host, port = m.group(1), int(m.group(2))
        if m is None:
            return
        options.proxy_host = host
        options.proxy_port = port
    except Exception:
        logging.exception("Failed to retrieve proxy from environment")


def test_connectivity(test_url):
    try:
        host = socket.gethostbyname(test_url)
        socket.create_connection((host, 80), 2)
        return True
    except Exception:
        pass
    return False


def get_url_hostname(url):
    try:
        from urllib.parse import urlparse
    except Exception:
        from urlparse import urlparse
    o = urlparse(url)
    return o.hostname


def decode_client_password_impl(data: bytes):
    import base64
    import decisco.jnod
    p = decisco.jnod.Parser()
    # send data to cisco server
    data = p.parse(data)
    if data.Tag != 'auth' or not data.text:
        logging.error('client auth data has no auth node or node has no text')
        return None, None
    dd = base64.b64decode(data.text)
    if isinstance(dd, bytes):
        ar = dd.split(b'\x00')
    else:
        ar = dd.split('\x00')
        ar = list(map(lambda x: x.decode('ascii'), ar))
    if len(ar) != 3:
        logging.error(
            "Expected that client auth data after decoding will have 3 "
            "elements, got %s" % len(ar))
        return None, None
        logging.info('Succesfully decoded user password')
    return ar[1].decode('utf-8'), ar[2].decode('utf-8')


def decode_client_password(data: bytes):
    try:
        return decode_client_password_impl(data)
    except Exception:
        logging.exception("password decoding failed")
        return None, None


def alu_target(url):
    p = urlparse.urlparse(url)
    q = urlparse.parse_qs(p.query)
    if 'TARGET' not in q:
        return ""
    target = q['TARGET'][0]
    return quote(target, '')
