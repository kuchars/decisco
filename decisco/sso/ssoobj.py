import time
import logging
import datetime
import xml.dom.minidom
import decisco.dexceptions


class UTC(datetime.tzinfo):
    ZERO = datetime.timedelta(0)

    def utcoffset(self, dt):
        return self.ZERO

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return self.ZERO


class LocalTimezone(datetime.tzinfo):
    STDOFFSET = datetime.timedelta(seconds=-time.timezone)
    DSTOFFSET = datetime.timedelta(seconds=-time.altzone) if time.daylight else STDOFFSET
    DSTDIFF = DSTOFFSET - STDOFFSET

    def utcoffset(self, dt):
        if self._isdst(dt):
            return self.DSTOFFSET
        else:
            return self.STDOFFSET

    def dst(self, dt):
        if self._isdst(dt):
            return self.DSTDIFF
        else:
            return UTC.ZERO

    def tzname(self, dt):
        return time.tzname[self._isdst(dt)]

    def _isdst(self, dt):
        tt = (dt.year, dt.month, dt.day,
              dt.hour, dt.minute, dt.second,
              dt.weekday(), 0, 0)
        stamp = time.mktime(tt)
        tt = time.localtime(stamp)
        return tt.tm_isdst > 0


local_timezone = LocalTimezone()


def datetime_from_createtime(ct, ttl):
    total_seconds = (ct + ttl) // 1000
    timed = datetime.timedelta(seconds=total_seconds)
    ret = datetime.datetime(1970, 1, 1, tzinfo=UTC()) + timed
    return ret.astimezone(local_timezone)


def _gett(node, *args):
    for a in args:
        lst = node.getElementsByTagName(a) + node.getElementsByTagName(a.lower())
        if lst:
            node = lst[0]
        else:
            logging.warning("Failed to find %s" % '.'.join(args))
            return None
    rc = []
    for node in node.childNodes:
        if node.nodeType in {node.TEXT_NODE, node.CDATA_SECTION_NODE}:
            rc.append(node.data)
    return ''.join(rc).strip()


class Container(object):
    def __init__(self, fso, cookies, **kwargs):
        object.__init__(self)
        self.token = ''
        self.status = None
        self.boshurl = None
        self.failure = None
        self.opisitem = None
        self.jabbertoken = ''
        self.cookies = cookies
        self.screenname = None
        self.createtime = None
        self.timetolive = None
        self.serviceurl = None
        self.jabbername = None
        self.jabbercluster = None
        self.xmppjabbercluster = None
        self.create_time = datetime.datetime.now()
        self.expires = self.create_time + datetime.timedelta(minutes=15)
        self._parse(fso, **kwargs)

    def _parse(self, fso, auth_failure_message=None):
        if fso is None:
            raise decisco.dexceptions.AuthenticationFailed(auth_failure_message or "No federatedsso data")
        fso_dom = xml.dom.minidom.parseString(fso)
        st = _gett(fso_dom, 'status')
        self.status = 'SUCCESS' == st
        if not self.status:
            title = _gett(fso_dom, 'title')
            reason = _gett(fso_dom, 'reason')
            code = _gett(fso_dom, 'errorcode')
            self.failure = '%s - %s(%s): %s' % (title, st, code, reason)
        try:
            self.token = _gett(fso_dom, 'token')
            self.boshurl = _gett(fso_dom, 'boshurl')
            self.opisitem = _gett(fso_dom, 'opisitem')
            self.screenname = _gett(fso_dom, 'screenname')
            self.createtime = _gett(fso_dom, 'createtime')
            self.timetolive = _gett(fso_dom, 'timetolive')
            self.serviceurl = _gett(fso_dom, 'serviceurl')
            self.jabbername = _gett(fso_dom, 'jabberName')
            self.jabbertoken = _gett(fso_dom, 'jabberToken')
            self.jabbercluster = _gett(fso_dom, 'jabberCluster')
            self.xmppjabbercluster = _gett(fso_dom, 'xmppjabbercluster')
            try:
                self.expires = datetime_from_createtime(int(self.createtime), int(self.timetolive))
            except Exception:
                logging.error("")
        except Exception:
            self.failure = "Failed to interpret content of FSO"
        if self.failure:
            raise decisco.dexceptions.AuthenticationFailed(self.failure)

    def expired(self):
        now = datetime.datetime.now(local_timezone)
        future_now = now + datetime.timedelta(seconds=10)
        return future_now > self.expires

    def __repr__(self):
        s = "status: %s, " % self.status
        s += "screenname: %s, " % self.screenname
        s += "serviceurl: %s, " % self.serviceurl
        s += "xmppjabbercluster: %s, " % self.xmppjabbercluster
        s += "create_time: %s, " % self.createtime
        s += "token: %s..., " % self.token[:25]
        s += "jabbertoken: %s" % self.jabbertoken[:25]
        return s
