import os
import sys
import time
import logging
import datetime
import decisco.args
import decisco.utils
import decisco.sso.args
import decisco.sso.ssoobj
import decisco.sso.credentials
import decisco.dexceptions


def make_auth_object(options: decisco.args.DeciscoArgs) -> decisco.sso.credentials.Credentials:
    options.cred_obj = decisco.sso.credentials.Credentials(options)
    options.cred_obj.load_data(not options.use_client_password)
    return options.cred_obj


def get_sso_data(options: decisco.args.DeciscoArgs) -> decisco.sso.ssoobj.Container:
    sso_obj = None
    log = options.get_logger('get_sso_data')
    url = decisco.utils.get_url_hostname(options.federated_sso)
    options.use_proxy = not decisco.utils.test_connectivity(url)
    log.info("Use proxy: %s", options.use_proxy)
    try:
        sso_handler = decisco.sso.args.SSO_METHOD_HANDLER[options.method]
    except KeyError:
        raise ValueError("Unsupported method: '%s'", options.method)
    sso_obj = sso_handler(options)
    try:
        conf = sso_obj.load()
    except decisco.dexceptions.AuthenticationFailed:
        raise
    except Exception:
        logging.exception("Exception occured during retrieval of WebEx Token")
        raise decisco.dexceptions.AuthenticationFailed("Exception occured during retrieval of WebEx Token")
    if conf is not None:
        s = conf.failure
        if s is not None:
            log.error(s)
            conf = None
            raise Exception("Aquiring SSO failed: %s" % s)
    return conf
