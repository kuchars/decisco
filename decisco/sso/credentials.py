import os
import logging
import decisco.sso.dialogs


class Credentials(object):
    def __init__(self, options):
        self.options = options
        self.email = None
        self.password = None
        self.username = options.username
        self.log = options.get_logger('credentials')

    def load_data(self, load_password):
        if not self.parse_credential_file(self.options.credentials):
            self.get_auth_data_from_console(load_password)
        if self.username is None or not self.username:
            raise Exception('Username is required')

    # def get_sms_code(self):
    #     return self.get_auth_data_from_console_impl(ask4='Sms code (aka OTP password) for %s:')

    def valid(self) -> bool:
        return self.username is not None and self.password is not None

    def set_emial(self, email: str):
        self.log.info("Setting user name/email from client: %s", email)
        self.email = email

    def set_password(self, password):
        self.log.debug("Setting password from client: %s", '*' * len(password))
        self.password = password

    def get_auth_data_from_console_impl(self, password=None, **kwargs):
        if password is None:
            title = 'Authentication for decisco'
            password = decisco.sso.dialogs.PasswordDialogs(
                self.options, self.username, title, **kwargs).get_pass()
        return password

    def get_username(self):
        if self.username is None:
            user_dialog = decisco.sso.dialogs.PasswordDialogs(self.options, None, '')
            self.username = user_dialog.get_username()
            print('Username: %s' % self.username)

    def get_auth_data_from_console(self, load_password):
        self.get_username()
        if load_password:
            self.password = self.get_auth_data_from_console_impl(self.password)
        return self

    def parse_credential_file(self, filename):
        if filename is None:
            return None
        filename = os.path.expanduser(filename)
        try:
            with open(filename) as f:
                self.log.debug(
                    "reading credentials from file: '%s'" % filename)
                lines = f.readlines()
                for l in lines:
                    if l.startswith("email="):
                        self.email = l[len("email="):].rstrip('\r\n')
                    if l.startswith("username="):
                        self.username = l[len("username="):].rstrip('\r\n')
                    elif l.startswith("password="):
                        self.password = l[len("password="):].rstrip('\r\n')
            if self.username is None or self.password is None:
                return False
            return True
        except Exception as e:
            self.log.exception("Unable to load credential file: %s" % filename)
        return None
