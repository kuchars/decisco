import os
import logging
import argparse
import importlib
import decisco.args


def load_handler(options: decisco.args.DeciscoArgs):
    log = options.get_logger('sso_loader')
    handlers_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'handlers')
    module_names = []
    for file_name in os.listdir(handlers_dir):
        file_ext = os.path.splitext(file_name)
        if file_ext[1] != '.py':
            continue
        mod_name = file_ext[0]
        full_file_name = os.path.join(handlers_dir, file_name)
        if mod_name in {'__init__'}:
            continue
        if os.path.isdir(full_file_name):
            continue
        module_names.append(mod_name)
    module_names = sorted(module_names)
    for mod_name in module_names:
        try:
            importlib.import_module('decisco.sso.handlers.%s' % mod_name)
        except ImportError:
            log.warning("Failed to load SSO handler: %s", mod_name)


class OrganizationAction(argparse.Action):
    def __init__(self, option_strings, dest, **kwargs):
        super(OrganizationAction, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        namespace.set_organization(values)


SSO_METHOD_HANDLER = {}
SSO_ARG_GROUP = decisco.args.add_option_group('Single sign-on generic settings')
SSO_ARG_GROUP.add_argument(
    '-o', "--organization", dest="organization", help="Domain e.g. 'nokia.com'", action=OrganizationAction)
SSO_ARG_GROUP.add_argument(
    "-u", "--username", dest="username", default=None,
    help="specifies the login username. Will prompt for password.")
SSO_ARG_GROUP.add_argument(
    "--credentials", dest="credentials",
    help="specifies the filename which contains username and password.")
SSO_ARG_GROUP.add_argument(
    '--email-input-name', dest="email_input_name", default=[], action='append',
    help="Allow to define names used in <input name='... for a email, def: %(default)s")
SSO_ARG_GROUP.add_argument(
    '--user-input-name', dest="user_input_name", default=['USER', 'USERNAME'], action='append',
    help="Allow to define names used in <input name='... for a username, def: %(default)s")
SSO_ARG_GROUP.add_argument(
    '--pass-input-name', dest="pass_input_name", default=['PASSWORD', 'TEMPPASSWORD'], action='append',
    help="Allow to define names used in <input name='... type='password' for a password, def: %(default)s")
SSO_ARG_GROUP.add_argument(
    '--login-button-value', dest="login_button_value", default=['Login'], action='append',
    help="Allow to define names used in <input name='... type='password' for a password, def: %(default)s")
SSO_ARG_GROUP.add_argument(
    '--auth-failure', dest="auth_failure_indicator", action='append',
    default=['Authentication Error', 'Incorrect Username or Password, please try again.'],
    help="Text that apears on a web page when passed credentials are not OK, def: %(default)s")
SSO_METHOD = SSO_ARG_GROUP.add_argument(
    "-m", "--method", dest="method", default=None, choices=set(),
    help="specifies the way to get the authentication token")


def add_handler(method: str, handler: object):
    global SSO_METHOD
    global SSO_METHOD_HANDLER
    SSO_METHOD.choices.add(method)
    SSO_METHOD_HANDLER[method] = handler
