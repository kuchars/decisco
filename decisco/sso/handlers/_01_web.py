import io
import re
import os
import sys
import ssl
import gzip
import json
import zlib
import socket
import logging
import optparse
import threading
import traceback
import subprocess
import xml.sax.saxutils
import decisco.args
import decisco.sso.args
import decisco.sso.ssoobj as ssoobj
import decisco.dexceptions
import decisco.sso.credentials
import decisco.sso.handlers.extras.cookies as web_cookie

import html
import socketserver as ss
import http.server as bhs
import http.client as httplib
import urllib.parse as urlparse
import html.parser as html_parser
unescape = html.unescape
RETRY_ERRORS = (httplib.BadStatusLine, ConnectionResetError, BrokenPipeError)


def with_color(c, s):
    return "\x1b[%dm%s\x1b[0m" % (c, s)


def join_with_script_dir(path):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), path)


def parse_qsl(s):
    return '\n'.join("%-20s %s" % (k, v) for k, v in urlparse.parse_qsl(s, keep_blank_values=True))


def mk_data(value, ret_type, encoding='utf8'):
    if value is None:
        return value
    if type(value) is ret_type:
        return value
    if ret_type is bytes:
        try:
            return bytes(value, encoding)
        except TypeError:
            return bytes(value)
    return value.decode('utf-8')


def process_body(body, content_type):
    if body is None:
        return
    limit = 40960
    if isinstance(body, bytes):
        body = body.decode('utf8', errors='ignore')
    if content_type.startswith('application/x-www-form-urlencoded'):
        return parse_qsl(body)
    elif content_type.startswith('application/json'):
        try:
            json_obj = json.loads(body)
            json_str = json.dumps(json_obj, indent=2)
            if json_str.count('\n') < 50:
                return json_str
            else:
                lines = json_str.splitlines()
                return "%s\n(%d lines)" % ('\n'.join(lines[:50]), len(lines))
        except ValueError:
            return body
    elif content_type.startswith('text/html'):
        m = re.search(r'<title[^>]*>\s*([^<]+?)\s*</title>', body, re.I)
        if m:
            h = html_parser.HTMLParser()
            return "HTML TITLE: %s\n%s" % (h.unescape(m.group(1)), body)  # .decode('utf-8')
    elif (content_type.startswith('text/xml') or content_type.startswith('application/xml')) and len(body) < limit:
        import xml.dom.minidom
        try:
            xml = xml.dom.minidom.parse(body)
            return xml.toprettyxml()
        except Exception:
            return body
    elif content_type.startswith('text/') and len(body) < limit:
        return body
    elif len(body) < limit:
        return "Mime: %s, Length: %s" % (content_type, len(body))


def get_set_cookies(obj):
    h = obj.headers
    if hasattr(h, 'get_all'):
        cookies = h.get_all('Set-Cookie', None)
        if cookies:
            return '\n'.join(cookies)
    else:
        return h.get('Set-Cookie', None)


def put_info(text, data, proc, col=32, cmp=None):
    if (not data and cmp is None) or (cmp is not None and not cmp(data)):
        return
    value = proc(data) if proc is not None else data
    print(with_color(col, "==== %s ====\n%s\n" % (text, value)))


def print_req_info(req, req_body):
    req_header_text = "%s %s %s\n%s" % (req.command, req.path, req.request_version, req.headers)
    print(with_color(33, req_header_text))
    put_info("QUERY PARAMETERS", urlparse.urlsplit(req.path).query, parse_qsl)

    put_info("COOKIE", req.headers.get('Cookie', ''), lambda x: parse_qsl(re.sub(r';\s*', '&', x)))

    put_info(
        "BASIC AUTH", req.headers.get('Authorization', ''), lambda x: x.split()[1].decode('base64'), 31,
        cmp=lambda x: x.lower().startswith('basic'))

    content_type = req.headers.get('Content-Type', '')
    put_info("REQUEST BODY", process_body(req_body, content_type), None, 32)


def print_res_info(res, res_body):
    res_header_text = "%s %d %s\n%s" % (res.response_version, res.status, res.reason, res.headers)
    print(with_color(36, res_header_text))
    put_info("SET-COOKIE", get_set_cookies(res), None, 31)

    content_type = res.headers.get('Content-Type', '')
    put_info("RESPONSE BODY", process_body(res_body, content_type), None, 32)


def replace_body_form_action(res_body: bytes, new_host: str):
    pattern = '<form[^>]*action="([^"]+)"'
    _pattern = mk_data(pattern, type(res_body))
    match = re.search(_pattern, res_body)
    if match is None:
        return
    url = match.group(1)
    if isinstance(url, bytes):
        url = url.decode('utf8', errors='ignore')
    url = unescape(url)
    u = urlparse.urlsplit(url)
    _, _, path = u.scheme, u.netloc, (u.path + '?' + u.query if u.query else u.path)
    new_host = new_host + path
    new_host = escape = xml.sax.saxutils.escape(new_host)
    new_target = mk_data(new_host, type(res_body))
    return res_body[:match.start(1)] + new_target + res_body[match.end(1):], path, url


def get_attribute(attr_name: str, input_def):
    pattern = mk_data(r'%s=("([^"]*)"|\'([^\']*)\')' % attr_name, type(input_def))
    match = re.search(pattern, input_def)
    return None if match is None else (match.group(2) or match.group(3))


class HtmlModifier(object):
    def __init__(self, options: decisco.args.DeciscoArgs, content: bytes, server):
        object.__init__(self)
        self.options = options
        self.content = content
        self.log = options.get_logger('sso.web')
        self.last_index = 0
        self.new_output = mk_data('', type(self.content))
        self.server = server

    def __gen_auto_click_script(self, login_button_id):
        return mk_data('<script type="text/javascript">\n'\
            'document.getElementById("%s").click();\n'\
            '</script>\n\n' % login_button_id, type(self.content))

    def __advence(self, match, group_idx=1):
        input_inner_start = None if match is None else match.start(group_idx)
        self.new_output += self.content[self.last_index: input_inner_start]
        self.last_index = input_inner_start

    def __add(self, match, value: str, pattern=' value="%s"'):
        self.__advence(match)
        self.new_output += mk_data(pattern % value, type(self.content))

    def __get_nodes(self, *args) -> list:
        candidatess = []
        cont_type = type(self.content)
        for pattern in args:
            candidatess += list(re.finditer(mk_data(pattern, cont_type), self.content))
        # candidatess = list(re.finditer(mk_data('<input([^>]*)>', cont_type), self.content))
        self.log.debug('Found %s items to process', len(candidatess))
        return sorted(candidatess, key=lambda x: x.start(0))

    @staticmethod
    def __get_str_attr(attrib_name: str, value):
        return mk_data(get_attribute(attrib_name, value), str)

    def __try_set_user_name_and_password(self):
        self.log.debug("trying to set user and password")
        allow_click = 0
        login_button_id = None
        cred = self.options.cred_obj
        cont_type = type(self.content)
        assert isinstance(cred, decisco.sso.credentials.Credentials)
        for match in self.__get_nodes('<input([^>]*)>', '<button([^>]*)>'):
            value = match.group(0)
            type_ = self.__get_str_attr('type', value)
            name_ = self.__get_str_attr('name', value)
            value_ = self.__get_str_attr('value', value)
            if type_ == 'hidden':
                continue
            if type_ in {'text', None} and name_ in self.options.user_input_name:
                allow_click += 1
                self.__add(match, cred.username)
                self.log.info("Setting username: %s in %s", cred.username, value)
            elif type_ in {'text', None} and name_ in self.options.email_input_name:
                allow_click += 1
                self.__add(match, cred.email)
                self.log.info("Setting username as email: %s in %s", cred.email, value)
            elif type_ == 'password' and name_ in self.options.pass_input_name:
                allow_click += 1
                self.__add(match, cred.password)
                self.log.info("Setting password: %s in %s", cred.password, value)
            elif (type_ == 'button' and value_ in self.options.login_button_value) or type_ == 'submit':
                login_button_id = self.__get_str_attr('id', value)
                if login_button_id is None:
                    login_button_id = "_allow_auto_login_"
                    self.__add(match, login_button_id, ' id="%s"')
                self.log.info("Found login button, id is: %s", login_button_id)
        if allow_click <= 1 or login_button_id is None:
            return
        self.server.login_atempted = True
        match = re.search(mk_data('</body>', cont_type), self.content)
        if match is None:
            self.log.error("Failed to locate </body>")
            return
        self.__advence(match, 0)
        self.new_output += self.__gen_auto_click_script(login_button_id)
        self.log.info("Succesfully added javascript for automatic click")

    def try_set_user_name_and_password(self) -> bytes:
        self.__try_set_user_name_and_password()
        self.__advence(None)
        return self.new_output

    def check_for_authentication_failure(self) -> bool:
        if not self.options.auth_failure_indicator or not self.server.login_atempted:
            return False
        for text in self.options.auth_failure_indicator:
            typed_text = mk_data(text, type(self.content))
            match = re.search(typed_text, self.content)
            if match is not None:
                self.log.error("Found '%s' in document, assuming authentication failed", match.group(0))
                return True
        return False


RESULT = """
<html>
<head>
    <title>Decisco Authentication</title>
    <script>
    function document_onload() { window.setTimeout(window.close.bind(window), 5000); }
    </script>
</head>

<body onload="document_onload()" style="font-size: 48pt; text-align: center;">
    <div style="display: table; height: 100%; width: 100%;">
        <span style="vertical-align: middle; height: 100%; display: table-cell; ">Thank you</span>
    </div>
</body>
</html>
"""


class Request(object):
    version_table = {10: 'HTTP/1.0', 11: 'HTTP/1.1'}

    def __init__(self, command, req, body, origin):
        object.__init__(self)
        self.req = req
        self.body = body
        self.origin = origin
        self.command = command

    def get(self, conn_allocator):
        conn = conn_allocator(self.origin)
        # headers = dict(self.req.headers) if IS_PYTHON2 else self.req.headers
        headers = self.req.headers
        conn.request(self.command, self.req.path, self.body, headers)
        res = conn.getresponse()
        setattr(res, 'headers', res.msg)
        setattr(res, 'response_version', self.version_table[res.version])
        res_body = res.read()
        return res, res_body


class ProxyRequestHandler(bhs.BaseHTTPRequestHandler):
    timeout = 120
    lock = threading.Lock()

    def __init__(self, request, client_address, server, *args, **kwargs):
        self.protocol_version = "HTTP/1.1"
        self.log = server.options.get_logger('web.sso.proxy')
        bhs.BaseHTTPRequestHandler.__init__(self, request, client_address, server, *args, **kwargs)

    def log_error(self, format, *args):
        if isinstance(args[0], socket.timeout):
            return
        self.log_message(format, *args)

    def _set_last_host(self):
        u = urlparse.urlsplit(self.path)
        self.server.last_host = "%s://%s" % (u.scheme, u.netloc)
        return self.server.last_host

    def fix_cookie_domain(self, res):
        h = res.headers
        import decisco.sso.handlers.extras.cookies as web_cookie
        cks = web_cookie.Cookies("")
        for n, v in h.items():
            if n.lower() == 'set-cookie':
                cks.parse(v)
        if len(cks) > 0:
            new = 'localhost'
            for cookie in cks.get_values():
                cookie.domain = new
                cookie.secure = False
            return cks

    def replace_header(self, name, value, obj=None):
        h = self.headers if obj is None else obj.headers
        if name not in h:
            h[name] = value
            return
        if hasattr(h, 'replace_header'):
            h.replace_header(name, value)
        else:
            h[name] = value

    def make_connection(self, origin):
        T = httplib.HTTPSConnection if origin[0] == 'https' else httplib.HTTPConnection
        ret = T(origin[1], timeout=self.timeout)
        o = self.server.options
        # if o.use_proxy and o.proxy_host:
        #     ret.set_tunnel(o.proxy_host, o.proxy_port)
        # ret.set_debuglevel(level)
        return ret

    def do_GET(self):
        old_path = self.path
        new_path = self.server.get_org_url_for_path(self.path)
        if new_path is not None:
            self.path = new_path
            req_host = self._set_last_host()
        elif self.path == '/':
            self.path = self.server.options.federated_sso
            req_host = self._set_last_host()
        else:
            req_host = self.server.last_host
            self.path = req_host + self.path

        req = self
        content_length = int(req.headers.get('Content-Length', 0))
        req_body = self.rfile.read(content_length) if content_length else None

        req.path = (req_host + req.path) if req.path[0] == '/' else req.path
        self.log.debug('proxying %s', req.path)

        u = urlparse.urlsplit(req.path)
        scheme, netloc, path = u.scheme, u.netloc, (u.path + '?' + u.query if u.query else u.path)
        assert scheme in ('http', 'https'), "Unsuported scheme: %s, url: %s" % (scheme, req.path)
        if netloc:
            self.replace_header('Host', netloc, req)
        self.filter_headers(req)
        url_request = Request(self.command, req, req_body, (scheme, netloc))
        try:
            if self.server.options.web_debug:
                with self.lock:
                    print_req_info(req, req_body)
            res, res_body = url_request.get(self.make_connection)
        except socket.timeout:
            self.log.exception("Request '%s' failed:" % u.geturl())
            self.end_up_with_failure(408)
            return
        except RETRY_ERRORS:
            self.log.error("Got RemoteDisconnected, do a retry")
            try:
                res, res_body = url_request.get(self.make_connection)
            except Exception:
                self.log.exception("Retry failed")
                self.end_up_with_failure(502)
                return
        except Exception as e:
            self.log.exception("Request '%s' failed:" % u.geturl())
            self.end_up_with_failure(502)
            return
        content_type = res.headers.get('Content-Type', 'text/html')
        content_encoding = res.headers.get('Content-Encoding', 'identity')
        res_body = self.decode_content_body(res_body, content_encoding)
        self.replace_header('Content-Encoding', 'identity', res)

        ret = replace_body_form_action(res_body, self.server.address)
        if isinstance(ret, tuple):
            res_body, path, res_loc = ret
            self.log.debug("sucessfully replaced form action, %s, %s", path, res_loc)
            if 'localhost' not in res_loc and path != res_loc:
                self.server.locations_map[path] = res_loc
        do_exit = False
        if 'text/html' in content_type:
            html_mod = HtmlModifier(self.server.options, res_body, self.server)
            if html_mod.check_for_authentication_failure():
                do_exit = True
                self.server.failure_message = "Authentication Failed, Found one of the failure indicatiors in document"
            else:
                res_body = html_mod.try_set_user_name_and_password()
        if 'text/xml' in content_type:
            self.log.debug("got content with type: text/xml")
            if len(res_body) == len(res_body):
                data = res_body
            else:
                data = res_body
            if isinstance(data, bytes):
                data = data.decode('utf8', errors='ignore')
            self.server.response = data
            do_exit = True
            res_body = mk_data(RESULT, type(res_body))
            self.replace_header('content-type', 'text/html', res)
        self.replace_header('content-length', str(len(res_body)), res)

        if self.server.options.web_debug:
            with self.lock:
                print_res_info(res, res_body)

        self.log.debug("sending response")
        self.filter_headers(res)
        self.fix_location(res, req_host)
        try:
            self.write_headers(res)
            self.wfile.write(res_body)
            self.wfile.flush()
            if self.server.options.web_debug:
                self.log.info("do_GET is done, %s, %s\n\n", content_type, content_encoding)
        except BrokenPipeError:
            pass
        if do_exit:
            self.server.shutdown()

    def end_up_with_failure(self, error_number):
        self.send_error(error_number)
        self.server.shutdown()

    def fix_location(self, res, req_host: str):
        if 'Location' not in res.headers:
            return
        res_loc = res.headers['Location']
        loc = self.server.get_new_location(res_loc, req_host)
        self.replace_header('Location', loc, res)

    def write_headers(self, res):
        cookies = self.fix_cookie_domain(res)
        self.w("%s %d %s\r\n" % (self.protocol_version, res.status, res.reason))
        for name, value in res.headers.items():
            if name.lower() == 'set-cookie' and cookies:
                continue
            self.w("%s: %s\r\n" % (name, value))
        if cookies:
            for ck in cookies.get_values():
                self.w('Set-cookie: %s\r\n' % ck.text())
        self.w('\r\n')

    do_PUT = do_GET
    do_HEAD = do_GET
    do_POST = do_GET
    do_DELETE = do_GET
    do_OPTIONS = do_GET

    def w(self, text):
        self.wfile.write(text.encode('ascii'))

    def filter_headers(self, obj):
        headers = obj.headers
        ENCODING = ('identity',)
        # 'gzip', 'x-gzip', 'deflate')
        # connection: keep-alive
        hop_by_hop = (
            'connection', 'keep-alive', 'proxy-authenticate', 'proxy-authorization', 'te', 'trailers',
            'transfer-encoding', 'upgrade', 'origin', 'referer')
        for k in hop_by_hop:
            del headers[k]
        if 'Accept-Encoding' in headers:
            ae = headers['Accept-Encoding']
            filtered_encodings = [x for x in re.split(r',\s*', ae) if x in ENCODING]
            self.replace_header('Accept-Encoding', ', '.join(filtered_encodings), obj)
        setattr(obj, 'headers', headers)
        return headers

    def decode_content_body(self, data, encoding):
        if encoding == 'identity':
            text = data
        elif encoding in ('gzip', 'x-gzip'):
            if isinstance(data, bytes):
                ios = io.BytesIO(data)
            else:
                ios = io.StringIO(data)
            with gzip.GzipFile(fileobj=ios) as f:
                text = f.read()
        elif encoding == 'deflate':
            try:
                text = zlib.decompress(data)
            except zlib.error:
                text = zlib.decompress(data, -zlib.MAX_WBITS)
        else:
            raise Exception("Unknown Content-Encoding: %s" % encoding)
        return text


class ThreadingHTTPServer(ss.ThreadingMixIn, bhs.HTTPServer):
    daemon_threads = True
    address_family = socket.AF_INET6

    def __init__(self, options: decisco.args.DeciscoArgs, req_hand=ProxyRequestHandler):
        server_address = ('::1', options.web_port)
        self.location_id = 1
        self.last_host = None
        self.options = options
        self.locations_map = {}
        self.response = None
        self.failure_message = None
        self.address = "http://localhost:%d" % options.web_port
        self.login_atempted = False
        bhs.HTTPServer.__init__(self, server_address, req_hand)

    def handle_error(self, request, client_address):
        # surpress socket/ssl related errors
        cls, e = sys.exc_info()[:2]
        if cls is socket.error or cls is ssl.SSLError:
            return
        return bhs.HTTPServer.handle_error(self, request, client_address)

    def get_new_location(self, res_loc: str, host: str) -> str:
        if res_loc.startswith('/'):
            res_loc = host + res_loc
        u = urlparse.urlsplit(res_loc)
        org_path = u.path + '?' + u.query if u.query else u.path
        self.locations_map[org_path] = res_loc
        return self.address + org_path

    def get_org_url_for_path(self, path: str) -> str:
        return self.locations_map.get(path, None)


class WebSsso(object):
    def __init__(self, options: decisco.args.DeciscoArgs):
        object.__init__(self)
        self.worker = ThreadingHTTPServer(options)
        self.log = options.get_logger('websso')

    def load(self):
        ret = None
        message = None
        try:
            self.log.debug('opening %s using %s', self.worker.address, self.worker.options.webbrowser)
            proc = subprocess.Popen([self.worker.options.webbrowser, self.worker.address])
            # if proc.returncode == 0:
            self.worker.serve_forever()
            proc.communicate()
            ret = ssoobj.Container(self.worker.response, None, auth_failure_message=self.worker.failure_message)
            if proc.returncode != 0:
                message = "Using %s failed - unable to authenticate" % self.worker.options.webbrowser
        except KeyboardInterrupt:
            message = "Web based Authentication canceled by user"
        except Exception:
            message = "Web based Authentication failed due to rutime exception"
        if ret is None:
            raise decisco.dexceptions.AuthenticationFailed(message)
        return ret


WEB_SSO_GROUP = decisco.args.add_option_group('SSO via Webbrowser')
WEB_SSO_GROUP.add_argument(
    "--sso-web-port", dest="web_port", default=8080, type=int,
    help="specifies the port on which webbrowser authentication will run")
WEB_SSO_GROUP.add_argument(
    "--sso-web-debug", dest="web_debug", default=False, action=decisco.args.ToggleAction,
    help="debug output for webbrowser authentication")
WEB_SSO_GROUP.add_argument(
    "--webbrowser", dest="webbrowser", default='xdg-open', help="Use non default webbrowser, default: %(default)s")
decisco.sso.args.add_handler('web', WebSsso)
