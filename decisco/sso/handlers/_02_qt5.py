#!/usr/bin/env python3
import os
import PyQt5.QtCore as core
import PyQt5.QtWidgets as widgets
import PyQt5.QtNetwork as network
import PyQt5.QtWebKitWidgets as webkit

import decisco.args
import decisco.sso.args as sso_args
import decisco.sso.handlers.extras.qtcommon as qtcommon


class Sso(qtcommon.Sso):
    def __init__(self, options):
        qtcommon.Sso.__init__(self, options, qtcommon.get_qt_gui_class(core, widgets, network, webkit))


default_cooki_file = os.path.abspath(os.path.expanduser('~/.decisco_sso.cookie'))
grp = decisco.args.add_option_group("Qt5 based SSO")
grp.add_argument(
    "-C", "--cookies", dest="cookie_filename", default=default_cooki_file,
    help="Location of cookie file, default: %(default)s.")
sso_args.add_handler('qt5', Sso)
