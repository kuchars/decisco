import os
import sys
import codecs
import logging
import decisco.args as dargs
import decisco.sso.ssoobj as ssoobj
import decisco.sso.handlers.extras.helpers


def get_network_manager_class(core, network):
    class NetworkManager(network.QNetworkAccessManager):
        def __init__(self, options, parent=None):
            super(NetworkManager, self).__init__(parent)
            self.log = logging.getLogger('sso.qt.netman')
            self.options = options
            self.sslErrors.connect(self.on_ssl_errors)
            self.authenticationRequired.connect(
                self.on_authentication_required)
            self.proxyAuthenticationRequired.connect(
                self.on_proxy_authentication_required)

        @core.pyqtSlot('QNetworkReply*', 'QList<QSslError>')
        def on_ssl_errors(self, reply, errors):
            for e in errors:
                self.log.error(e)
            reply.ignoreSslErrors()

        @core.pyqtSlot('QNetworkReply*', 'QAuthenticator*')
        def on_authentication_required(self, reply, authenticator):
            authenticator.setUser(self.options.cred_obj.username)
            authenticator.setPassword(self.options.cred_obj.password)
            self.log.debug(
                "Page authentication requested - setting up credentials")

        @core.pyqtSlot('QNetworkProxy', 'QAuthenticator*')
        def on_proxy_authentication_required(self, proxy, authenticator):
            self.log.debug("Proxy authentication called")

    return NetworkManager


def get_proxy_factory_class(network):
    class ProxyFactory(network.QNetworkProxyFactory):
        def __init__(self, options):
            super(ProxyFactory, self).__init__()
            self.options = options

        def queryProxy(self, query):
            if self.options.use_proxy and self.options.proxy_host:
                proxy = network.QNetworkProxy(
                    network.QNetworkProxy.HttpProxy,
                    self.options.proxy_host)
                proxy.setPort(self.options.proxy_port)
            else:
                proxy = network.QNetworkProxy(
                    network.QNetworkProxy.NoProxy)
            return [proxy]

    return ProxyFactory


def get_qt_gui_class(core, gui, network, webkit):
    class SsoQt(gui.QMainWindow):
        def __init__(self, options: dargs.DeciscoArgs):
            super(SsoQt, self).__init__()
            self.progress = 0
            self.running = True
            self.options = options
            self.error_message = ''
            self.log = logging.getLogger('sso.qt%s' % core.PYQT_VERSION_STR)

            proxy = get_proxy_factory_class(network)(options)
            network.QNetworkProxyFactory.setApplicationProxyFactory(proxy)

            self.view = webkit.QWebView(self)
            self.view.setPage(webkit.QWebPage(self.view))
            self.view.titleChanged.connect(self.__adjust_title)
            self.view.loadProgress.connect(self.__set_progress)
            self.view.loadFinished.connect(self.__adjust_location)
            self.view.loadFinished.connect(self.__finish_loading)
            self.__set_network_manager(self.view.page())

            locationEdit = gui.QLineEdit(self)
            locationEdit.setReadOnly(True)
            locationEdit.setSizePolicy(
                gui.QSizePolicy.Expanding,
                locationEdit.sizePolicy().verticalPolicy())

            toolBar = self.addToolBar("Navigation")
            toolBar.addAction(self.view.pageAction(webkit.QWebPage.Back))
            toolBar.addAction(self.view.pageAction(webkit.QWebPage.Forward))
            toolBar.addAction(self.view.pageAction(webkit.QWebPage.Reload))
            toolBar.addAction(self.view.pageAction(webkit.QWebPage.Stop))
            toolBar.addWidget(locationEdit)
            self.locationEdit = locationEdit
            self.setCentralWidget(self.view)
            self.setUnifiedTitleAndToolBarOnMac(True)

        def load(self, url):
            if isinstance(url, str):
                url = core.QUrl(url)
            self.view.load(url)

        def load_cookies(self):
            if not os.path.exists(self.options.cookie_filename):
                return
            cookieJar = self.view.page().networkAccessManager().cookieJar()
            with codecs.open(self.options.cookie_filename, "r", "utf-8") as f:
                text = f.read().encode('utf-8')
                cookies = network.QNetworkCookie.parseCookies(
                    core.QByteArray(text))
                cookieJar.setAllCookies(cookies)

        def save_cookies(self):
            file = core.QFile(self.options.cookie_filename)
            file.open(core.QIODevice.WriteOnly)
            cookieJar = self.view.page().networkAccessManager().cookieJar()
            for cookie in cookieJar.allCookies():
                rf = cookie.toRawForm()
                rf += '\n'
                file.write(rf)
            file.close()

        def closeEvent(self, event):
            self.running = False

        @core.pyqtSlot('QNetworkReply*')
        def __finished_reply(self, reply):
            contentMimeType = reply.header(network.QNetworkRequest.ContentTypeHeader)
            if contentMimeType == "text/xml":
                self.hide()
                self.running = False

        def __set_network_manager(self, page):
            _cookieJar = network.QNetworkCookieJar()
            _networkmanager = get_network_manager_class(core, network)(self.options)
            _networkmanager.setCookieJar(_cookieJar)
            _networkmanager.finished.connect(self.__finished_reply)
            page.setNetworkAccessManager(_networkmanager)

        def __adjust_location(self):
            self.locationEdit.setText(self.view.url().toString())

        def __adjust_title(self):
            if 0 < self.progress < 100:
                self.setWindowTitle("%s (%s%%)" % (self.view.title(), self.progress))
            else:
                self.setWindowTitle(self.view.title())

        def __set_progress(self, p):
            self.progress = p
            self.__adjust_title()

        def __set_user_password_auto_click(self, doc: 'QWebElement'):
            allow_click = 0
            login_button = None
            for input in doc.findAll('input'):
                type_ = input.attribute("type", "text")
                if input.hasAttribute("value"):
                    if type_ == "button":
                        if input.attribute("value") in self.options.login_button_value:
                            login_button = input
                    continue
                if not input.hasAttribute("name"):
                    continue
                name_ = input.attribute("name")
                if type_ == "password":
                    if name_ in self.options.pass_input_name:
                        # input.setAttribute("value", self.options.cred_obj.password)
                        allow_click += 1
                        input.evaluateJavaScript("this.value = '%s'" % self.options.cred_obj.password)
                        logging.info("Setting password")
                elif name_ in self.options.user_input_name:
                    allow_click += 1
                    input.setAttribute("value", self.options.cred_obj.username)
                    logging.info("Setting username")
                elif name_ in self.options.email_input_name:
                    allow_click += 1
                    input.setAttribute("value", self.options.cred_obj.email)
                    logging.info("Setting username as email")
            if allow_click <= 1:
                return
            if login_button is not None:
                logging.info("Performing auto click")
                login_button.evaluateJavaScript("this.click()")
            else:
                logging.info("No login button found")

        def __check_for_authentication_error(self, doc: 'QWebElement'):
            document_text = doc.toPlainText()
            for failure_ind in self.options.auth_failure_indicator:
                if failure_ind in document_text:
                    self.error_message = "Found '%s' in a document, assuming authentication failed" % failure_ind
                    self.log.error(self.error_message)
                    return True
            return False

        def __finish_loading(self, stat):
            current_url = self.view.url().toString()
            self.log.debug("Finished: %s(%s)", current_url, stat)
            self.progress = 100
            self.__adjust_title()
            if not stat:
                self.view.stop()
                self.error_message = "Failed to load page: %s" % current_url
                self.log.error(self.error_message)
                self.log.error(self.view.page().mainFrame().toHtml())
                self.hide()
                return
            ''' load status is OK but we can still have failed authentication '''
            mf = self.view.page().mainFrame()
            doc = mf.documentElement()
            self.__set_user_password_auto_click(doc)
            if self.__check_for_authentication_error(doc):
                self.running = False
                self.hide()
            self.log.debug("load.finished")

        def get_content(self):
            self.log.debug("get_content")
            if self.error_message:
                return None
            main = self.view.page().mainFrame()
            content = main.toHtml()
            return str(content)

        @classmethod
        def run(cls, options):
            app = gui.QApplication(sys.argv)
            browser = cls(options)
            browser.load_cookies()
            browser.load(options.federated_sso)
            browser.show()
            while browser.running or app.hasPendingEvents():
                app.processEvents(core.QEventLoop.AllEvents, 1000)
                if app.closingDown():
                    if app.hasPendingEvents():
                        continue
                    else:
                        break
            browser.save_cookies()
            return ssoobj.Container(browser.get_content(), None, auth_failure_message=browser.error_message)

    return SsoQt


class Sso(object):
    def __init__(self, options, gui_class):
        object.__init__(self)
        self.options = options
        self.gui_class = gui_class

    def load(self):
        return self.gui_class.run(self.options)
