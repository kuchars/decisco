import re
import logging


class Cookie(object):
    def __init__(self, name, value):
        object.__init__(self)
        self.name = name
        self.value = value
        self.expires = None
        self.secure = False
        self.httpOnly = False
        self.domain = None
        self.path = None

    def set_prop(self, prop_name, prop_value):
        prop_name = prop_name.lower()
        if prop_name == 'domain':
            self.domain = prop_value
        elif prop_name == 'path':
            self.path = prop_value
        elif prop_name == 'expires':
            self.expires = prop_value
        elif prop_name == 'secure':
            self.secure = True
        elif prop_name == 'httponly':
            self.httpOnly = True
        else:
            logging.error("Unknown property: '%s'" % prop_name)

    def text(self):
        r = '%s=%s' % (self.name, self.value)
        if self.expires:
            r += '; expires=%s' % self.expires
        if self.domain:
            r += '; domain=%s' % self.domain
        if self.path:
            r += '; path=%s' % self.path
        if self.secure:
            r += '; secure'
        if self.httpOnly:
            r += '; HTTPOnly'
        return r

    def __repr__(self):
        return self.text()


class Cookies(object):
    NV = re.compile(r'([^=]+)=([^;,]*)(;|,)?')
    EXP = re.compile(r'\s*expires=(\w{3}, \d+[ \-]\w+[ \-][\d:\ ]+ GMT)(;|,)?\s*', re.IGNORECASE)
    PROPS = re.compile(r'\s*(domain|path|secure|httponly)(=([^,;]+))?(.)?\s*', re.IGNORECASE)

    def __init__(self, text=None):
        object.__init__(self)
        self.cookies = {}
        if text:
            if isinstance(text, bytes):
                return self.parse(text.decode('utf8', errors='ignore'))
            elif isinstance(text, list):
                for x in text:
                    self.parse(x)
            else:
                self.parse(text)

    def parse_args(self, cookie, text):
        while text:
            m = self.EXP.match(text)
            if m:
                cookie.expires, sep = m.group(1), m.group(2)
                text = text[m.end(0):]
                if sep == ';':
                    continue
                return text
            m = self.PROPS.match(text)
            if not m:
                logging.error("Unable to parse cookie property: '%s'" % text)
                return text
            prop_name, _, prop_value, sep = m.groups()
            cookie.set_prop(prop_name, prop_value)
            text = text[m.end(0):]
            if sep == ';':
                continue
            return text
        return text

    def parse(self, text):
        while text:
            m = self.NV.match(text)
            if m is None:
                logging.error("Unable to parse: '%s'" % text)
                break
            n, v, sep = m.group(1), m.group(2), m.group(3)
            cookie = Cookie(n, v)
            self.cookies[cookie.name] = cookie
            text = text[m.end(0):]
            if sep != ';':
                break
            text = self.parse_args(cookie, text)

    def get_names(self):
        return self.cookies.names()

    def get_values(self):
        return self.cookies.values()

    def __iter__(self):
        return iter(self.cookies)

    def __getitem__(self, key):
        return self.cookies[key]

    def __len__(self):
        return len(self.cookies)


if __name__ == '__main__':
    s = "Max-Age=1296000; expires=Wed, 10 Oct 2018 21:27:14 GMT; Path=/stepupAuth; HttpOnly; Secure"
    x = Cookies()
    x.parse(s)
    print(x.cookies)
