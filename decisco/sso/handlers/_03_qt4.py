#!/usr/bin/env python3
import os
import sys
if 'PyQt5' in sys.modules:
    raise ImportError("Can't load PyQt4 and PyQt5 at the same time")
import PyQt4.QtGui as gui
import PyQt4.QtCore as core
import PyQt4.QtWebKit as webkit
import PyQt4.QtNetwork as network

import decisco.sso.handlers.extras.qtcommon as qtcommon


class Sso(qtcommon.Sso):
    def __init__(self, options):
        qtcommon.Sso.__init__(self, options, qtcommon.get_qt_gui_class(core, gui, network, webkit))


def register_help():
    import decisco.args
    import decisco.sso.args as sso_args
    default_cooki_file = os.path.abspath(os.path.expanduser('~/.decisco_sso.cookie'))
    grp = decisco.args.add_option_group("Qt5 based SSO")
    grp.add_argument(
        "-C", "--cookies", dest="cookie_filename", default=default_cooki_file,
        help="Location of cookie file, default: %(default)s.")
    sso_args.add_handler('qt4', Sso)


if __name__ == '__main__':
    import decisco.tests
    print(Sso(decisco.tests.Options()).load())
else:
    register_help()
