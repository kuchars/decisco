import sys
import logging
import decisco.args
import decisco.sso.ssoobj
import decisco.dexceptions as dexceptions


class FileSso(object):
    """Just make the user get the token manually by giving the returned XML"""
    def __init__(self, options):
        self.url = options.federated_sso
        self.filename = options.xml

    def load(self):
        if self.filename is None:
            if not sys.stdin.isatty():
                raise dexceptions.AuthenticationFailed("Unable to load federated_sso xml from stdin")
            print("Go to {} and copy the resulting XML, then do enter and ctrl+d".format(self.url))
            self.content = sys.stdin.read()
        else:
            with open(self.filename, 'r') as f:
                self.content = f.read()
        return decisco.sso.ssoobj.Container(self.content, {})


FILE_SSO_GROUP = decisco.args.add_option_group('Manual SSO')
FILE_SSO_GROUP.add_argument(
    "-x", "--xml", dest="xml", default=None,
    help="specifies the filename which contains the XML response with the tokens (use with file method).")
decisco.sso.args.add_handler('file', FileSso)
