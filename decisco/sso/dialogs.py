import os
import sys
import getpass
import logging
import subprocess


class PasswordDialogs(object):
    def __init__(self, options, username, title, ask4="Password for %s:"):
        object.__init__(self)
        self.username = username
        self.q = ask4 % username
        self.title = title
        self.option_map = [
            ('kdialog', self._get_kde_pass),
            ('zenity', self._get_zenity),
            ('Xdialog', self._get_xdialog)]
        self.log = logging.getLogger(options.AppName + '.XPasswordDialogs')

    def _get_kde_pass(self, title, info, ispass, default):
        cmd = ['kdialog', '--title', title]
        if ispass:
            cmd += ['--password', info]
        else:
            cmd += ['--inputbox', info]
            if default is not None:
                cmd += [default]
        return cmd

    def _get_xdialog(self, title, info, ispass, default):
        cmd = ['Xdialog', '--title', title]
        if ispass:
            cmd += ['--password']
        cmd += ['--stdout']
        cmd += ['--inputbox', info, '0', '0']
        if default is not None:
            cmd += [default]
        return cmd

    def _get_zenity(self, title, info, ispass, default):
        cmd = ['zenity']
        if ispass:
            tit = title + '\n' + info
            cmd += ['--title', tit, '--password']
        else:
            cmd += ['--title', title, '--text', info, '--entry']
            if default is not None:
                cmd += ['--entry-text', default]
        return cmd

    def _get_terminal(self, title, info, ispass=True, default=None):
        print(title)
        if ispass:
            return getpass.getpass(info)
        sys.stdout.write(info)
        return sys.stdin.readline()

    def _run_app_cmd(self, title, info, ispass=True, default=None):
        import distutils.spawn
        for dialog in self.option_map:
            if distutils.spawn.find_executable(dialog[0]) is not None:
                self.log.debug('Found %s' % dialog[0])
                return dialog[1](title, info, ispass, default)
        return None

    def _run_app(self, cmd):
        if cmd is None:
            self.log.warning('No graphical dialog app found')
            return None
        try:
            text = subprocess.check_output(cmd)
            return text.rstrip('\n')
        except Exception:
            self.log.warning('dialog failed - probably application returned 1')
            return None

    def get_pass(self):
        if sys.stdout.isatty():
            self.log.info("Trying to acquire credencials from console")
            return self._get_terminal(self.title, self.q)
        self.log.info("stdout is not a tty, unable to get pass from term")
        cmd = self._run_app_cmd(self.title, self.q)
        return self._run_app(cmd)

    def get_username(self):
        user_name_tip = "Add -u or --username= to your decisco.py"\
            " command line if this annoys you"
        title = 'Decisco - User name: '
        default = os.environ.get('USER', '')
        if sys.stdout.isatty():
            return self._get_terminal(user_name_tip, title, False, default)
        else:
            self.log.info("stdout is not a tty, unable to get user from term")
            cmd = self._run_app_cmd(title, user_name_tip, False, default)
            return self._run_app(cmd)
