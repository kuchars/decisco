import decisco.jnod
import decisco.shared as dshared
import decisco.dsockets as dsockets
import decisco.extensions.extra
import decisco.extensions.const as const


class Client2Server(object):
    def __init__(self, client: dsockets.Socket, server: dsockets.Socket, shared: dshared.SharedResource):
        object.__init__(self)
        self.shared = shared
        self.client = client
        self.server = server
        self.log = self.shared.options.get_logger('forwarder')
        self.data_server = dsockets.OutBuffer(self.server, self.shared.selector, self.shared.options.jam_thres)
        self.data_client = dsockets.OutBuffer(self.client, self.shared.selector, self.shared.options.jam_thres)
        sel = self.shared.selector
        sel.register(self.client, self.client.name, self.ready_client_read, self.ready_client_write).for_inout().\
            set_close_action(self.dissconect).apply()
        sel.register(self.server, self.server.name, self.ready_server_read, self.ready_server_write).for_in().\
            set_close_action(self.dissconect).apply()
        self.shared.extra_stuff.register_slot('altered_to_client', self.__extra_altered_data2client)
        self.shared.extra_stuff.register_slot('altered_to_server', self.__extra_altered_data2server)
        self.dissconnecting = False

    def __extra_altered_data2client(self, data: bytes):
        self.log.debug('got extra data to client: %s', data)
        self.data_client.schedule(data)

    def __extra_altered_data2server(self, data):
        self.log.debug('got extra data to server: %s', data)
        self.data_server.schedule(data)

    def dissconect(self, sock=None):
        if self.dissconnecting:
            return
        self.log.info(
            "Disconnecting(%s) ...", self.shared.selector.get_name(sock))
        end_stream = decisco.shared.get_connection_timeout_error()
        if sock is None:
            self.data_client.schedule(end_stream)
            self.data_server.schedule(end_stream)
        else:
            if self.data_client.socket == sock:
                self.data_client.clear(True)
                self.data_server.schedule(end_stream)
            elif self.data_server.socket == sock:
                self.data_server.clear(True)
                self.data_client.schedule(end_stream)
        self.dissconnecting = True
        self.shared.emit('stop_forwarding')

    def __schedule(self, data, out: dsockets.OutBuffer):
        out.schedule(data)
        if out.checkJam(len(data)):
            self.dissconect(out.socket)

    def cleanup(self):
        self.log.info("Cleanup")
        sel = self.shared.selector
        sel.unregister(self.client)
        sel.unregister(self.server)
        self.shared.extra_stuff.unregister_slot('altered_to_client')
        self.shared.extra_stuff.unregister_slot('altered_to_server')
        self.data_client = None
        self.data_server = None
        self.client.close()
        self.server.close()

    def ready_server_read(self):
        data = self.server.read()
        if b"</stream:stream>" in data:
            self.log.warning("Server stream ended it's stream")
            self.dissconect(self.server)
            return
        else:
            d = decisco.extensions.extra.ExtraStuff.SERVER
            if self.shared.extra_stuff.process_experimental(data, d, self.data_client, self.data_server):
                return
        self.log.debug("client.schedule: %s" % data)
        self.__schedule(data, self.data_client)

    def ready_client_read(self):
        data = self.client.read()
        if b"</stream:stream>" in data:
            self.log.warning("Client stream ended it's stream")
            self.dissconect(self.client)
            return
        else:
            d = decisco.extensions.extra.ExtraStuff.CLIENT
            if self.shared.extra_stuff.process_experimental(data, d, self.data_server, self.data_client):
                return
        self.log.debug("server.schedule: %s" % data)
        self.__schedule(data, self.data_server)

    def ready_server_write(self):
        self.data_server and self.data_server.send()
        self.dissconnecting and self.shared.emit('stop_forwarding')

    def ready_client_write(self):
        self.data_client and self.data_client.send()
        self.dissconnecting and self.shared.emit('stop_forwarding')
