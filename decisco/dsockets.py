import ssl
import time
import fcntl
import types
import socket
import select
import struct
import logging
import datetime
import traceback
import collections
import decisco.args
import decisco.dexceptions
try:
    from IN import IP_RECVERR
except ImportError:
    IP_RECVERR = 11
# extra ioctl numbers
SIOCINQ = 0x541B
SIOCOUTQ = 0x5411


class TraceCommunication(object):
    SEND = 1
    RECIEVE = 2

    def __init__(self):
        object.__init__(self)

    def addSendTrace(self, content: bytes, *args):
        self.addTrace(content, self.SEND, *args)

    def addReadTrace(self, content: bytes, *args):
        self.addTrace(content, self.RECIEVE, *args)

    def addTrace(self, content: bytes, operation, name, color=None):
        if operation == self.SEND:
            logging.debug("%s sends %s bytes", name, len(content))
        elif operation == self.RECIEVE:
            logging.debug("%s reads %s bytes", name, len(content))
        else:
            logging.info("%s op_code(%s) %s bytes", name, operation, len(content))


class SocketBase(object):
    '''
    Unfortunately inheriting from socket.socket is not documented in Python 2
    then it's better not to use it.
    '''
    def __init__(self, sock=None, family=socket.AF_INET, type=socket.SOCK_STREAM, name='', tracer=None):
        object.__init__(self)
        self.socket = sock
        self.name = name
        if tracer is not None:
            assert isinstance(tracer, TraceCommunication)
        self.tracer = tracer
        self.added_methods = set()
        if self.socket is None:
            self.socket = socket.socket(family, type)

    def get_socket_object(self):
        sock = self.socket
        while sock and isinstance(sock, SocketBase):
            sock = sock.socket
        return sock

    def _make_method(self, name, override=False):
        def generic_func(s, *args, **kwargs):
            method = getattr(s.socket, name)
            # print("calling", method, "with", args, kwargs)
            return method(*args, **kwargs)

        old_method = getattr(self, name, None)
        if old_method is not None and name not in self.added_methods and not override:
            return old_method
        if old_method:
            print("add method", name, override, old_method)
        new_method = types.MethodType(generic_func, self)
        setattr(self, name, new_method)
        self.added_methods.add(name)
        return new_method

    def _copy_methods(self, override=False):
        sock = self.get_socket_object()
        for name in dir(sock):
            value = getattr(sock, name)
            if name.startswith('_'):
                continue
            if isinstance(value, (types.MethodType, types.BuiltinMethodType)):
                self._make_method(name, override)


class ExtendSocket(SocketBase):
    def __init__(self, sock=None, **kwargs):
        SocketBase.__init__(self, sock, **kwargs)
        # self.convert = StrAndBytes()

    def recv_bytes(self, *args, **kwargs) -> bytes:
        ret_data = self.socket.recv(*args, **kwargs)
        if self.tracer:
            self.tracer.addReadTrace(ret_data, self.name)
        return ret_data

    def recv(self, *args, **kwargs) -> bytes:
        '''
        print('Socket.recv: %s, socket: %s, fno: %s' % (
            self.__class__.__name__,
            self.socket.__class__.__name__,
            self.socket.fileno()))
        '''
        return self.socket.recv(*args, **kwargs)

    def send(self, data: bytes):
        '''
        print('Socket.send: %s, socket: %s, fno: %s' % (
            self.__class__.__name__,
            self.socket.__class__.__name__,
            self.socket.fileno()))
        '''
        assert isinstance(data, bytes)
        send_data = data
        # send_data = self.convert(data)
        if self.tracer:
            self.tracer.addSendTrace(send_data, self.name)
        return self.socket.send(send_data)

    def read(self) -> bytes:
        ''' print('Socket.read') '''
        count = 1024
        data = self.recv(count)
        if data is None or data == b'':
            '''
            print(dir(self))
            print(dir(self.socket))
            '''
            raise decisco.dexceptions.EmptyRead(self)
        ret = data
        while len(data) == count:
            try:
                data = None
                data = self.recv(count)
            except Exception:
                break
            if data is None or data == b'':
                break
            ret += data
        ret_data = ret
        # ret_data = self.convert(ret)
        if self.tracer:
            self.tracer.addReadTrace(ret_data, self.name)
        return ret_data

    def close(self):
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except Exception:
            logging.debug("Failed to shutdown socket")
        try:
            self.socket.close()
        except Exception:
            logging.debug("Failed to close socket")

    def get_unsend_bytes_count(self):
        return struct.unpack("I", fcntl.ioctl(self.fileno(), SIOCOUTQ, '\0\0\0\0'))[0]

    def set_tcp_user_timeout(self, timeout=2000):
        TCP_USER_TIMEOUT = 18
        self.setsockopt(socket.SOL_TCP, TCP_USER_TIMEOUT, timeout)
        return self

    def set_tcp_no_delay(self, val=1):
        self.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, val)
        return self

    def set_error_receiving(self, val=1):
        self.setsockopt(socket.IPPROTO_IP, IP_RECVERR, val)


class Socket(ExtendSocket):
    def __init__(self, sock, **kwargs):
        ExtendSocket.__init__(self, sock, **kwargs)
        self._copy_methods()


class SelectorData(object):
    def __init__(self, name, sock, read_h, write_h, flags, cs=None):
        object.__init__(self)
        self.name = name
        self.sock = sock
        self.flags = flags
        self.read_h = read_h
        self.write_h = write_h
        self.close_action = cs
        ''' This is for SSL SSLWantReadError/SSLWantWriteError '''
        self.safe_read = getattr(sock, 'safe_recv', None)
        self.safe_write = getattr(sock, 'safe_send', None)


class RegisterOptions(object):
    def __init__(self, parent, sock, name, read_h, write_h, flags, modify):
        object.__init__(self)
        self.parent = parent
        self.sock = sock
        self.name = name
        self.read_h = read_h
        self.write_h = write_h
        self.flags = flags
        self.modify = modify
        self.close_action = None

    def for_in(self):
        self.flags = self.parent.flags
        return self

    def for_out(self):
        self.flags = self.parent.basef | select.EPOLLOUT
        return self

    def for_inout(self):
        self.flags = self.parent.flags | select.EPOLLOUT
        return self

    def set_read_handler(self, handler):
        self.read_h = handler
        return self

    def set_write_handler(self, handler):
        self.write_h = handler
        return self

    def set_handlers(self, read_h, write_h):
        self.read_h = read_h
        self.write_h = write_h
        return self

    def set_close_action(self, cs):
        self.close_action = cs
        return self

    def apply(self):
        if self.sock is None:
            return self
        fno = self.parent.extract_fd(self.sock)
        assert(isinstance(fno, int))
        if self.modify:
            if fno not in self.parent.known_sockets:
                self.sock = None
                return self
            data = self.parent.known_sockets[fno]
            if self.name:
                data.name = self.name
            if self.read_h:
                data.read_h = self.read_h
            if self.write_h:
                data.write_h = self.write_h
            if self.close_action:
                data.close_action = self.close_action
            data.flags = self.flags
            self.parent.poll.modify(fno, self.flags)
            self.sock = None
            return self
        self.parent.known_sockets[fno] = SelectorData(
            self.name, self.sock, self.read_h, self.write_h, self.flags,
            self.close_action)
        self.parent.poll.register(fno, self.flags)
        self.sock = None
        return self

    def __del__(self):
        self.apply()


class SocketSelector(object):
    EPOLLRDHUP = 0x2000

    def __init__(self, options: decisco.args.DeciscoArgs):
        object.__init__(self)
        self.basef = select.EPOLLHUP | select.EPOLLERR | self.EPOLLRDHUP
        self.flags = select.EPOLLIN | self.basef
        self.poll = select.epoll()
        self.known_sockets = {}
        self.log = options.get_logger('client<->server')

    def extract_fd(self, socket):
        if socket is None:
            return socket
        if isinstance(socket, int):
            return socket
        if hasattr(socket, 'fileno'):
            return socket.fileno()
        raise Exception("Unable to retrieve fd from %s" % socket)

    def get_name(self, sock):
        fno = self.extract_fd(sock)
        if fno in self.known_sockets:
            return self.known_sockets[fno].name

    def known(self, socket):
        return socket and self.extract_fd(socket) in self.known_sockets

    def register(self, sock: Socket, name: str, read_handler, write_handler):
        return RegisterOptions(
            self, sock, name, read_handler, write_handler, self.flags, False)

    def modify(self, sock: Socket):
        return RegisterOptions(self, sock, None, None, None, self.flags, True)

    def unregister(self, sock: Socket):
        if sock is None:
            return
        fno = self.extract_fd(sock)
        if fno in self.known_sockets:
            self.known_sockets.pop(fno)
            self.poll.unregister(fno)
            logging.debug("Unregistered: %s, %s", fno, sock.name)
        else:
            logging.debug("Failed to unregister socket: %s, name: %s", fno, self.get_name(sock))
            logging.debug("Known sockets are: %s", self.known_sockets.keys())
            for fd, o in self.known_sockets.items():
                logging.debug("\tfd: %s, name: %s", fd, o.name)
            try:
                ''' just in case epoll::unregister throws an exception on unknown fd's '''
                self.poll.unregister(fno)
            except IOError:
                logging.error("Fall back, epoll.unregister of fd: %s failed", fno)

    def __handle_error(self, data: SelectorData):
        if data.close_action:
            data.close_action(data.sock)
        else:
            self.log.error('Error, socket: %s unregister & close', data.name)
            try:
                self.unregister(data.sock)
            except Exception:
                logging.exception("Failed to unregister: %s", data.sock)
            data.sock.close()

    def __handle_socket(self, events, data: SelectorData):
        all_flags = ~0
        if events & select.EPOLLIN or events & select.EPOLLPRI:
            all_flags &= ~(select.EPOLLIN | select.EPOLLPRI)
            try:
                if data.safe_read is None:
                    data.read_h()
                else:
                    data.safe_read(data.read_h, self, data)
            except decisco.dexceptions.EmptyRead:
                self.log.error('Got empty data on %s - un-registering', data.name)
                self.__handle_error(data)
                return
            except socket.error as se:
                if se.errno == 110:
                    self.log.info("Timeout on %s", data.name)
                else:
                    self.log.exception("Other exception while handling socket")
                self.__handle_error(data)
                return
        if events & select.EPOLLOUT:
            all_flags &= ~(select.EPOLLOUT)
            if data.safe_write is None:
                data.write_h()
            else:
                data.safe_write(data.write_h, self, data)
        if events & select.EPOLLHUP:
            all_flags &= ~(select.EPOLLHUP)
            self.log.warning("%s hung up", data.name)
            self.__handle_error(data)
        elif events & select.EPOLLERR:
            all_flags &= ~(select.EPOLLERR)
            self.log.warning("%s - error", data.name)
            self.__handle_error(data)
        elif events & self.EPOLLRDHUP:
            all_flags &= ~(self.EPOLLRDHUP)
            self.log.error("%s - EPOLLRDHUP", data.name)
            self.__handle_error(data)
        if events & all_flags:
            events = events & all_flags
            self.log.warning("unhandled event: %s on %s", all_flags, data.name)

    def __main_loop_element_events(self, fd, events):
        if fd not in self.known_sockets:
            self.log.warning("Unknown fd: %s - un-register", fd)
            self.unregister(fd)
            return
        data = self.known_sockets[fd]
        self.__handle_socket(events, data)

    def select(self, *args):
        '''
        epool - timeout in seconds as float
        pool - timeout in milliseconds
        select - timeout in seconds as float
        '''
        list = self.poll.poll(*args)
        for fd, events in list:
            self.__main_loop_element_events(fd, events)


class OutBuffer(object):
    def __init__(self, socket, selector: SocketSelector, jam_thres=None):
        object.__init__(self)
        self.socket = socket
        self.selector = selector
        self.jam_thres = jam_thres
        self.data_2_send = collections.deque()

    def __len__(self):
        return len(self.data_2_send)

    def is_old(self, seconds=None):
        seconds = seconds or self.jam_thres
        if not self.data_2_send or not seconds:
            return False
        time = self.data_2_send[0][1]
        delta = datetime.timedelta(seconds=seconds)
        return time + delta < datetime.datetime.now()

    def clear(self, remove=False):
        self.data_2_send = collections.deque()
        remove and self.remove()

    def remove(self):
        self.selector.unregister(self.socket)
        self.socket = None

    def schedule(self, data: bytes):
        assert isinstance(data, bytes)
        self.data_2_send.append((data, datetime.datetime.now()))
        self.socket and self.selector.modify(self.socket).for_inout()

    def send(self):
        if not self.socket:
            return False
        if self.data_2_send:
            data, tim = self.data_2_send.popleft()
            blen = self.socket.send(data)
            if blen < len(data):
                self.data_2_send.appendleft((data[blen:], tim))
        if not self.data_2_send:
            self.selector.modify(self.socket).for_in()
            return True
        return False

    def checkJam(self, recent_data_length: int=None):
        cnt = self.socket.get_unsend_bytes_count()
        if len(self) > 1 and self.is_old():
            now = datetime.datetime.now()
            seconds = None or self.jam_thres
            logging.warning("Looks like %s is jammed and not responding - start shutdown", self.socket.name)
            logging.warning("There are %d un-sended items, jam_threshold is %ss", len(self.data_2_send), seconds)
            for index, item in enumerate(self.data_2_send):
                payload, date = item
                diff = now - date
                logging.warning("\titem: %d, waits: %ds", index, diff.seconds)
                logging.warning("\t%s", payload)
            return True
        elif recent_data_length and cnt > recent_data_length:
            logging.debug('There are unsent bytes(%s) on %s pipe', cnt, self.socket.name)
        return False


class FakeXmppServer(object):
    def __init__(self, local_host: str, local_port: int, wait_for_socket: bool, log):
        object.__init__(self)
        self.log = log
        self.local_host = local_host
        self.local_port = local_port
        self.wait_for_socket = wait_for_socket
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def bind(self):
        if not self.wait_for_socket:
            self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        waiting = True
        while True:
            try:
                self.server.bind((self.local_host, self.local_port))
            except socket.error:
                if self.wait_for_socket:
                    if waiting:
                        waiting = False
                        self.log.info('Socket in use - waiting')
                    time.sleep(1)
                    continue
                else:
                    raise
            else:
                break

    def fileno(self):
        return self.server.fileno()

    def listen(self, *args):
        return self.server.listen(*args)

    def accept(self, *args):
        return self.server.accept(*args)


class SocketProxyImpl(ExtendSocket):
    def __init__(self, options: decisco.args.DeciscoArgs, proxy_is_optional: bool, **kwargs):
        ExtendSocket.__init__(self, **kwargs)
        self.options = options
        self.proxy_is_optional = proxy_is_optional
        self.connect_timeout = 2
        self.connected_directly = False
        self.log = options.get_logger("SocketProxy")

    def __get_connect_request(self, addr_pair) -> bytes:
        if addr_pair[1] == 80:
            ret = "GET http://%s/ HTTP/1.1\n" % addr_pair[0]
        else:
            ret = "CONNECT %s:%s HTTP/1.1\n" % addr_pair
        ret += "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7;"\
            " rv:15.0) Gecko/20100101 Firefox/15.0.1\n"
        ret += "Proxy-Connection: keep-alive\n"
        ret += "Host: %s\n\n" % addr_pair[0]
        return bytes(ret, 'utf-8')

    def __disable_proxy(self):
        self.connected_directly = True
        self.log.info('Trying to connect directly...')
        self.options.connected_directly = True
        # self.options.use_proxy = False
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except Exception:
            ''' hide error if not connected '''
            pass

    def __direct_connect(self, addr_pair):
        ''' Connect directly '''
        self.socket.settimeout(self.connect_timeout)
        try:
            self.socket.connect(addr_pair)
        except (socket.timeout, socket.error, socket.gaierror) as e:
            self.log.exception("Direct connect %s:%s failed" % addr_pair)
            raise decisco.dexceptions.ProxyError(addr_pair, (), str(e))
        else:
            self.log.debug("proxy skipped - connection successful")

    def __enable_proxy(self):
        opt = self.options
        return opt.proxy_host is not None and opt.use_proxy and not opt.connected_directly

    def __safe_proxy_request(self, proxy_addr, addr_pair: tuple):
        second_try_method = None
        second_try_method_args = None
        try:
            connect_request = self.__get_connect_request(addr_pair)
            self.log.debug('sending connect request: %s', connect_request)
            self.send(connect_request)
        except socket.timeout as e:
            self.log.error("Timeout: Failed to send proxy connect cmd")
            if self.proxy_is_optional:
                self.__disable_proxy()
                second_try_method = self.__direct_connect
                second_try_method_args = (addr_pair,)
            else:
                raise decisco.dexceptions.ProxyError(addr_pair, proxy_addr, str(e))
        else:
            ''' wait for proxy server response '''
            data = self.recv(1000)
            # data = self.convert(self.recv(1000))
            if b"200" not in data:
                self.log.error("proxy response:\n%s\n", data)
                raise decisco.dexceptions.ProxyError(addr_pair, proxy_addr, data)
            self.log.debug("proxy response successful")
        return second_try_method, second_try_method_args

    def __safe_proxy_connect(self, proxy_addr, addr_pair):
        second_try_method = None
        second_try_method_args = None
        try:
            self.socket.settimeout(self.connect_timeout)
            self.socket.connect(proxy_addr)
        except socket.timeout as e:
            self.log.exception("Proxy %s:%s failed" % proxy_addr)
            raise decisco.dexceptions.ProxyError(addr_pair, proxy_addr, str(e))
        except socket.error as e:
            '''
            ('ECONNREFUSED', 111),
            ('ECONNRESET', 104),
            ('EHOSTUNREACH', 113),
            ('ENETUNREACH', 101),
            ('ETIMEDOUT', 110),
            '''
            if e.errno == 113:  # EHOSTUNREACH
                self.log.exception("Proxy %s:%s failed" % proxy_addr)
                if self.proxy_is_optional:
                    self.__disable_proxy()
                    second_try_method = self.__direct_connect
                    second_try_method_args = (addr_pair,)
            raise decisco.dexceptions.ProxyError(addr_pair, proxy_addr, str(e))
        except socket.gaierror as e:
            self.log.exception("Proxy %s:%s failed" % proxy_addr)
            raise decisco.dexceptions.ProxyError(addr_pair, proxy_addr, str(e))
        else:
            second_try_method, second_try_method_args = self.__safe_proxy_request(proxy_addr, addr_pair)
        if second_try_method:
            second_try_method(*second_try_method_args)

    def connect(self, addr_pair):
        if self.__enable_proxy():
            proxy_host = self.options.proxy_host
            proxy_port = self.options.proxy_port
            proxy_addr = (proxy_host, proxy_port)
            self.log.debug("Proxy connection to %s through %s", addr_pair, proxy_addr)
            self.__safe_proxy_connect(proxy_addr, addr_pair)
        else:
            self.__direct_connect(addr_pair)


class SocketProxy(SocketProxyImpl):
    def __init__(self, options: decisco.args.DeciscoArgs, proxy_is_optional=True, **kwargs):
        SocketProxyImpl.__init__(self, options, proxy_is_optional, **kwargs)
        self._copy_methods()


class SafeAsyncSsl(object):
    def safe_recv(self, read_op, sselect: SocketSelector, data: SelectorData):
        try:
            return read_op()
        except ssl.SSLWantReadError:
            '''
            Do nothing - this is SSL thing, this will be repeated next time
            SocketSelector kicks in
            '''
            pass

    def safe_send(self, write_op, sselect: SocketSelector, data: SelectorData):
        try:
            return write_op()
        except ssl.SSLWantWriteError:
            '''
            Do nothing - this is SSL thing, this will be repeated next time
            SocketSelector kicks in
            '''
            pass


class SocketSslImpl(ExtendSocket):
    def __init__(self, options: decisco.args.DeciscoArgs, **kwargs):
        ExtendSocket.__init__(self, **kwargs)
        kwargs.pop('name', None)
        kwargs.pop('tracer', None)
        kwargs['cert_reqs'] = ssl.CERT_NONE
        kwargs['do_handshake_on_connect'] = False
        kwargs['ssl_version'] = ssl.PROTOCOL_SSLv23
        self.kwargs = kwargs
        self.log = options.get_logger("SocketSsl." + self.name)

    def connect(self, addr_pair: tuple):
        self.setblocking(1)
        self.socket.connect(addr_pair)
        self.log.debug("perform SSL handshake")
        self.kwargs['sock'] = self.get_socket_object()
        self.socket.settimeout(6)
        self.socket = ssl.SSLSocket(**self.kwargs)
        try:
            self.socket.context.check_hostname = False
        except Exception:
            pass
        self.socket.do_handshake()
        self.setblocking(0)


class SocketSsl(SocketSslImpl, SafeAsyncSsl):
    def __init__(self, options: decisco.args.DeciscoArgs, **kwargs):
        SocketSslImpl.__init__(self, options, **kwargs)
        SafeAsyncSsl.__init__(self)
        self._copy_methods()


class ProxySslSocket(ExtendSocket, SafeAsyncSsl):
    def __init__(self, options: decisco.args.DeciscoArgs, **kwargs):
        ExtendSocket.__init__(self, **kwargs)
        SafeAsyncSsl.__init__(self)
        self.options = options
        kwargs.pop('sock', None)
        self.kwargs = kwargs
        self.connected_directly = False
        self._copy_methods()

    def connect(self, addr_pair: tuple):
        orginal_socket = self.socket
        proxy = SocketProxy(self.options, sock=orginal_socket, **self.kwargs)
        if addr_pair[1] != 443:
            proxy.connect(addr_pair)
            return
        ssl = SocketSsl(self.options, sock=proxy, **self.kwargs)
        ssl.connect(addr_pair)
        self.connected_directly = proxy.connected_directly
        self.socket = ssl.socket


'''
opt = self.getsockopt(socket.SOL_SOCKET, socket.SO_RCVTIMEO, 30)
print("Existing read timeout is '%s'" % str(struct.unpack('LL', opt)))

opt = self.getsockopt(socket.SOL_SOCKET, socket.SO_SNDTIMEO, 30)
print("Existing send timeout is '%s'" % str(struct.unpack('LL', opt)))

self.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
self.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 5)
self.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 5)
self.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 3)
sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
sock.setsockopt(socket.SOL_TCP, socket.TCP_QUICKACK, 1)
sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 5)
sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 1)
sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT, 5)
sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)

# reading socket values
opt = self.getsockopt(socket.SOL_TCP, TCP_USER_TIMEOUT, 30)
print("Existing TCP_USER_TIMEOUT is '%s'" % str(struct.unpack('I', opt)))
opt = self.getsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 30)
print("Existing TCP_NODELAY is '%s'" % str(struct.unpack('I', opt)))
'''


def test_SocketProxy(opt, addr):
    ss = SocketBase()
    s = SocketProxy(opt, sock=ss)
    s.connect(addr)
    s.send('GET /index.html HTTP/1.1\r\n\r\n')
    while True:
        print(s.read())
    s.getsockname()


def test_ssl(opt, addr):
    ss = SocketBase()
    s = SocketSsl(opt, sock=ss)
    s.connect(addr)
    s.send('GET /index.html HTTP/1.1\r\n\r\n')
    while True:
        try:
            print(s.read())
            break
        except ssl.SSLWantReadError:
            pass
    s.getsockname()


def test_proxy_ssl(opt, addr, text=None):
    s = ProxySslSocket(opt)
    s.setblocking(1)
    s.connect(addr)
    print(s.getsockname())
    if text:
        s.send(text)
    else:
        s.send('GET /index.html HTTP/1.1\r\n\r\n')
    # while True:
    #     try:
    #         print(s.read())
    #         break
    #     except ssl.SSLWantReadError:
    #         pass
    # s.setblocking(0)
    return s


def get_data(s):
    while True:
        try:
            print('READ', s.read())
            s.setblocking(0)
        except ssl.SSLWantReadError:
            pass
        except decisco.dexceptions.EmptyRead:
            break


def test_cisco_sever_connection(opt):
    pair4 = ('isj7cmx.webexconnect.com', 443)
    t = "<?xml version='1.0' ?><stream:stream to='nokia.com' xmlns="\
        "'jabber:client' xmlns:stream='http://etherx.jabber.org/streams'"\
        " xml:lang='en' version='1.0'>\n"
    s = test_proxy_ssl(opt, pair4, t)
    get_data(s)


if __name__ == "__main__":
    import logging
    log_level = logging.DEBUG
    largs = {}
    log_formatting = '%(asctime)s.%(msecs)d %(levelname)8s: %(filename)s:'\
        '%(lineno)4s %(name)s.%(message)s'
    logging.basicConfig(
        level=log_level,
        format=log_formatting,
        datefmt='%m-%d %H:%M:%S', **largs)

    class Opt(object):
        def __init__(self):
            object.__init__(self)
            self.proxy_host = '10.144.1.10'
            self.proxy_port = 8080
            self.use_proxy = False  # True
            self.AppName = 'ds'
            self.connected_directly = False

    pair1 = ('www.google.com', 443)
    pair2 = ('mail.tlen.pl', 80)
    pair3 = ('inside.nokiasiemensnetworks.com', 443)

    test_cisco_sever_connection(Opt())
    # test_SocketProxy(Opt(), pair2)
    # test_ssl(Opt(), pair3)
