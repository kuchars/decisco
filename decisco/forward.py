import os
import sys
import time
import fcntl
import errno
import socket
import logging
import datetime

import decisco.args
import decisco.shared
import decisco.colors as colors
import decisco.client2server
import decisco.client2decisco
import decisco.decisco2server
import decisco.dsockets as dsockets


class SomethingGoneTottalyWrong(Exception):
    pass


class XmppForwarderBase(object):
    def __init__(self, options: decisco.args.DeciscoArgs):
        object.__init__(self)
        self.options = options
        self.selector = dsockets.SocketSelector(options)
        self.shared = decisco.shared.SharedResource(self.options, self, self.selector)
        ''' helps to get jabberToken '''
        self.log = options.get_logger('XmppForwarderBase')
        opt = self.options
        slogger = options.get_logger("decisco.server")
        self.server = dsockets.FakeXmppServer(opt.local_xmpp_host, opt.local_xmpp_port, opt.wait_for_socket, slogger)
        self.selector.register(self.server, "decisco", self.__handle_server_accept_client, None)

        ''' communication handlers '''
        self.client2server = None
        self.client2decisco = None
        self.decisco2server = None
        self.shared.register_slot('start_forwarding', self.__start_forwarding_slot)
        self.shared.register_slot('stop_forwarding', self.__stop_forwarding_slot)
        self.shared.register_slot('stop_setup', self.__stop_setup)

    def cleanup(self):
        if self.client2server is not None:
            self.client2server.cleanup()
            self.client2server = None
        if self.decisco2server is not None:
            self.decisco2server.cleanup()
            self.decisco2server = None
        if self.client2decisco is not None:
            self.client2decisco.cleanup()
            self.client2decisco = None

    def __del__(self):
        self.cleanup()

    def __stop_setup(self):
        self.log.info('Stoping setup, No network or something else ...')
        self.cleanup()
        self.options.connected_directly = False

    def __stop_forwarding_slot(self):
        if self.client2server is None:
            return
        self.log.info('Stopping Forwarding ...')
        if self.client2server is not None:
            self.client2server.cleanup()
            self.client2server = None
        self.options.connected_directly = False

    def __start_forwarding_slot(self, data: dsockets.OutBuffer):
        self.log.info('Switching to Forward mode ...')
        try:
            self.client2server = None
            client_sock = self.client2decisco.cleanup(False)
            server_sock = self.decisco2server.cleanup(False)
            client2server_obj = decisco.client2server.Client2Server(client_sock, server_sock, self.shared)
            self.log.debug("data: %s", data.data_2_send)
            client2server_obj.data_client.data_2_send = data.data_2_send
            self.client2server = client2server_obj
        finally:
            self.client2decisco = None
            self.decisco2server = None

    def __handle_server_accept_client(self):
        client, addr = self.server.accept()
        if any([self.client2server, self.client2decisco, self.decisco2server]):
            client.close()
            s = 'Got another connection from %s - rejecting' % str(addr)
            self.log.warning(s)
            return
        self.shared.client_auth.reset()
        self.client_disconnected = False
        client_sock = dsockets.Socket(client, name='Client<-->Decisco', tracer=self.shared.tracing)
        client_sock.setblocking(0)

        self.client2decisco = decisco.client2decisco.Client2Decisco(self.shared, client_sock)
        self.decisco2server = decisco.decisco2server.Decisco2Server(self.shared, None)
        self.log.info('accepted new client')

    def handle_exception(self):
        self.log.exception('Caught exception in main loop:\n')
        raise SomethingGoneTottalyWrong("Caught exception in main loop")

    def _main_loop_element(self):
        # self.__check_for_reload()
        try:
            cnt = self.shared.delayed_count()
            self.selector.select(1)
            self.shared.process_delayed(cnt)
        except IOError as e:
            if e.errno != errno.EINTR:
                self.handle_exception()
        except Exception:
            self.handle_exception()

    def _main_loop(self):
        while True:
            self._main_loop_element()

    def run(self):
        if not self.options.use_client_password:
            try:
                ret = self.shared.client_auth.renew_token()
            except Exception:
                self.log.fatal("Unable to retrieve webex token")
                ret = False
            if not ret:
                sys.exit(1)
        self.server.listen(0)
        self.log.info(
            "%sLocal (mediating) XMPP server is up and running on port: %s%s%s",
            colors.cGreen, colors.cYellow, self.options.local_xmpp_port, colors.cDefault)
        self.log.info(
            "%sServer is bound to '%s%s%s' address%s", colors.cGreen,
            colors.cYellow, self.options.local_xmpp_host, colors.cGreen, colors.cDefault)
        self.log.info("%sNow use your favorite XMPP/jabber client to connect to it.%s", colors.cGreen, colors.cDefault)
        self.log.info(
            "%sMake sure that your client will allow SSL-less connection and plain-text authentication%s",
            colors.cRed, colors.cDefault)
        self.log.info("%sAlso make sure that your client won't try proxy here.%s", colors.cRed, colors.cDefault)
        if self.options.credentials:
            self.log.info(
                "%sYour corporate username will be taken from file: %s%s%s",
                colors.cGreen, colors.cYellow, self.options.credentials, colors.cDefault)
        else:
            self.log.info(
                "%sUsername: %s%s%s would be used as your corporate account, if it's incorrect rerun with -u option"
                " or use credentials file.%s", colors.cGreen, colors.cYellow, self.options.username, colors.cGreen,
                colors.cDefault)
        if self.options.use_client_password:
            self.log.info(
                "%sYour corporate password will be taken directly from jabber client so make sure it's correct.%s",
                colors.cGreen, colors.cDefault)
        elif self.options.credentials:
            self.log.info(
                "%sYour corporate password will be taken from file: %s%s%s",
                colors.cGreen, colors.cYellow, self.options.credentials, colors.cDefault)
        else:
            self.log.info(
                "%sYour corporate password had already been acquired, connect before Webex token expires.%s",
                colors.cGreen, colors.cDefault)
        try:
            self._main_loop()
        except KeyboardInterrupt:
            self.log.info("Exiting ...")
            self.cleanup()


class XmppForwarder(XmppForwarderBase):
    ''' Accepts only one client and then connects to Cisco server and then
    forwards in both directions '''
    def __init__(self, options: decisco.args.DeciscoArgs):
        XmppForwarderBase.__init__(self, options)
        self.server.bind()


def register_help():
    import decisco.args
    grp = decisco.args.add_option_group("XMPP related options")
    grp.add_argument(
        "-p", "--port", "--local-xmpp-port", dest="local_xmpp_port", type=int, default=5222,
        help="local xmpp port: %(default)s")
    grp.add_argument(
        "--bind", "--local-xmpp-host", dest="local_xmpp_host", default="localhost",
        help="local xmpp host: %(default)s")
    grp.add_argument(
        "-e", "--experimental", dest="experimental", default=True, action='toggle', help="enable experimental stuff.")
    grp.add_argument(
        "-E", "--cisco-features", dest="cisco_features", default=False, action='toggle', help="enable cisco features.")

    grp = decisco.args.add_option_group("Network Conditioning")
    grp.add_argument("--proxy-host", dest="proxy_host", default=None, help="proxy server: %(default)s")
    grp.add_argument("--proxy-port", dest="proxy_port", type=int, default=8080, help="proxy port: %(default)s")
    grp.add_argument(
        "-t", "--timeout", dest="timeout", type=int, default=5,
        help="Timeout on client/server communication in seconds, default: %(default)s.")
    grp.add_argument(
        "-j", "--jamming-threshold", dest="jam_thres", type=int, default=10,
        help="Jamed client/server communication in seconds, default: %(default)s.")
    grp.add_argument(
        "-w", "--wait-4-socket", dest="wait_for_socket", default=False, action='toggle',
        help="enable waiting for still used socket.")


register_help()
