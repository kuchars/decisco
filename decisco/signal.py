import collections


class DelayedObj(object):
    def __init__(self, name, args, kwargs):
        self.name = name
        self.args = args
        self.kwargs = kwargs


class Signals(object):
    def __init__(self):
        object.__init__(self)
        self.known_slots = {}
        self.unhandled_signals = []
        self.delayed_execution = collections.deque()

    def register_slot(self, name: str, handler):
        self.known_slots[name] = handler
        to_remove = []
        for i, pair in enumerate(self.unhandled_signals):
            if pair[0] == name:
                handler(*pair[1], **pair[2])
                to_remove.append(i)
        for i in reversed(to_remove):
            del self.unhandled_signals[i]

    def unregister_slot(self, name: str):
        if name in self.known_slots:
            return self.known_slots.pop(name)

    def __emit(self, name: str, data, kwargs):
        if name in self.known_slots:
            self.known_slots[name](*data, **kwargs)
        else:
            self.unhandled_signals.append((name, data, kwargs))

    def emit(self, name: str, *data, **kwargs):
        delayed = kwargs.pop('delayed', False)
        if delayed:
            self.delayed_execution.append(DelayedObj(name, data, kwargs))
            return
        self.__emit(name, data, kwargs)

    def delayed_count(self):
        return len(self.delayed_execution)

    def process_delayed(self, count: int=None):
        if count is None:
            count = len(self.delayed_execution)
        while self.delayed_execution and count > 0:
            first = self.delayed_execution.popleft()
            self.__emit(first.name, first.args, first.kwargs)
            count -= 1
