import ssl
import socket


class AuthenticationFailed(Exception):
    pass


class CouldNotRenewToken(Exception):
    pass


class EmptyRead(Exception):
    pass


class ClientDisconnected(Exception):
    pass


class ServerDisconnected(Exception):
    pass


class ProxyError(Exception):
    def __init__(self, destination, via, info):
        Exception.__init__(self)
        self.via = via
        self.destination = destination
        self.info = info

    def __repr__(self):
        return "Proxy connection to %s via %s failed:\n%s" %\
            (self.destination, self.via, self.info)


SslSocketError = ssl.SSLError
AddressError = socket.gaierror
