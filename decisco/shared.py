import os
import datetime
import decisco.jnod as jnod
import decisco.utils
import decisco.swapi as swapi
import decisco.signal as dsignal
import decisco.sso.ssoobj as ssoobj
import decisco.args as dargs
import decisco.dsockets as dsockets
import decisco.dexceptions as dexceptions
import decisco.extensions.extra
import decisco.extensions.loader


def get_sasl_auth_failure(extra) -> bytes:
    return bytes(
        '<sasl:failure>Plaintext authentication failed (Incorrect username or password) - %s</sasl:failure>\n'
        '</stream:stream>\n' % extra, 'utf-8')


def get_connection_timeout_error() -> bytes:
    connection_timeout = jnod.Node('stream:error').\
        add_child('connection-timeout').set_attr('xmlns', 'urn:ietf:params:xml:ns:xmpp-streams')
    return connection_timeout.to_bytes() + b"</stream:stream>\n"


class BothWayAuthentication(object):
    Client = 1
    Server = 2
    Both = 3

    def __init__(self, parent: 'SharedResource', conf_getter: swapi.GetJabberToken, options: dargs.DeciscoArgs):
        object.__init__(self)
        self.conf = None
        self.parent = parent
        self.options = options
        self.authentication = 0
        self.renew_needed = False
        self.conf_getter = conf_getter
        self.need_to_invoke_sam = False
        self.log = options.get_logger('Auth')

    def reset(self):
        self.authentication = 0

    def need_renewal(self):
        self.renew_needed = True
        self.log.info("Token renewal required")

    def renew_token(self) -> bool:
        self.log.info("Renewing your jabber token ...")
        try:
            self.conf = self.conf_getter.aquire()
        except dexceptions.AuthenticationFailed as e:
            raise dexceptions.CouldNotRenewToken(get_sasl_auth_failure(e))
        except Exception as e:
            self.log.exception("Got exception while retrieving webex token")
            raise dexceptions.CouldNotRenewToken(get_sasl_auth_failure(e))
        self.renew_needed = self.conf is None
        return not self.renew_needed

    def get_conf(self) -> ssoobj.Container:
        if self.renew_needed or (self.conf and self.conf.expired()):
            if self.renew_token():
                self.log.info("jabberToken aquired")
                return self.conf
        return self.conf

    def finished(self):
        pass

    def __authenticate(self, way: int):
        self.authentication |= way
        if self.authentication == self.Both:
            self.finished()

    def authenticated_server(self):
        self.__authenticate(self.Server)

    def authenticated_client(self, data: bytes, fromTo: str) -> ssoobj.Container:
        self.__authenticate(self.Client)
        if self.conf is None or self.renew_needed:
            ''' no configuration so we need to acquire '''
            user_name, password = decisco.utils.decode_client_password(data)
            user_email = '%s@%s' % (user_name, self.options.organization)
            self.options.cred_obj.set_emial(user_email)
            self.options.cred_obj.set_password(password)
            if self.renew_token():
                self.log.info("jabberToken aquired")
        return self.conf


class XmppTracer(dsockets.TraceCommunication):
    def __init__(self, path):
        dsockets.TraceCommunication.__init__(self)
        if not os.path.exists(path):
            raise IOError("Path '%s' does not exists" % path)
        self.output_path = path
        self.files_map = {}
        # if bytes is str:
        #     self.get_date = self.get_date_bytes_is_str
        #     self.get_bytes = self.get_bytes_bytes_is_str

    def get_file(self, name: str):
        name = name.replace("<-->", "_")
        if name in self.files_map:
            return self.files_map[name]
        file_name = os.path.join(self.output_path, 'decisco.xmpp.traces.%s.xml' % name)
        ret_file = open(file_name, "wb")
        self.files_map[name] = ret_file
        return ret_file

    # def get_date_bytes_is_str(self):
    #     return datetime.datetime.now().strftime("%d-%H:%M:%S.%f")

    # def get_bytes_bytes_is_str(self, content):
    #     return content

    def get_date(self):
        return bytes(datetime.datetime.now().strftime("%d-%H:%M:%S.%f"), encoding='ascii')

    # def get_bytes(self, content):
    #     if isinstance(content, bytes):
    #         return content
    #     return bytes(content, encoding='utf8', errors='ignore')

    def addSendTrace(self, content: bytes, name: str, color=None):
        # self.addTrace(content, self.SEND, name, color=color)
        f = self.get_file(name)
        f.write(b"<!-- %s send -->\n" % self.get_date())
        f.write(content)
        f.write(b"\n")
        f.flush()

    def addReadTrace(self, content: bytes, name: str, color=None):
        # self.addTrace(content, self.RECIEVE, name, color=color)
        f = self.get_file(name)
        f.write(b"<!-- %s receive -->\n" % self.get_date())
        f.write(content)
        f.write(b"\n")
        f.flush()


class SharedResource(dsignal.Signals):
    def __init__(self, options: dargs.DeciscoArgs, parent, selector: dsockets.SocketSelector):
        dsignal.Signals.__init__(self)
        self.options = options
        self.selector = selector
        self.parent = parent
        self.swapi = swapi.GetJabberToken(options)
        self.client_auth = BothWayAuthentication(self, self.swapi, options)
        self._id = None
        if options.xmpp_trace is not None:
            self.tracing = XmppTracer(options.xmpp_trace)
        else:
            self.tracing = dsockets.TraceCommunication()
        self.log = options.get_logger('shared')
        self.extra_stuff = decisco.extensions.extra.ExtraStuff(self, selector)
        decisco.extensions.loader.load(self.extra_stuff)
        self.register_slot('got_ip_address', self.extra_stuff.got_ip_address_slot)

    def handle_experimental(self, pdata, method, name) -> list:
        if not self.options.experimental:
            return []
        try:
            return method(pdata, self.extra_stuff)
        except Exception:
            self.log.exception("%s experimental", name)
            self.log.debug(pdata)
        return []
