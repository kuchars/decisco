import logging
import collections


AppName = 'DeCisco'


class Node(object):
    def __init__(self, Tag, **kwargs):
        object.__init__(self)
        self.Tag = Tag
        self.attribs = {}
        self.childs = []
        self.text = None
        self.__set_attr(**kwargs)

    def __getitem__(self, key):
        return self.attribs[key]

    def __getattr__(self, name):
        if name.startswith('__'):
            return object.__getattr__(name)
        if name.startswith('a_'):
            return self.get(name[2:], '')
        else:
            for c in self.childs:
                if c.Tag == name:
                    return c
        return self

    def rename_attr(self, name, newname):
        if name in self.attribs:
            v = self.attribs[name]
            del self.attribs[name]
            self.attribs[newname] = v
        return self

    def get(self, key, *arg):
        if key in self.attribs:
            return self.attribs[key]
        if not arg:
            raise KeyError(key)
        return arg[0]

    def has_attr(self, name, **kwargs):
        ret = name in self.attribs
        if ret and 'value' in kwargs:
            val = kwargs['value']
            _val = self.attribs[name]
            ret = (_val == val) or (val is any) or (isinstance(val, tuple) and _val in val)
        return ret
        # self.__check_attrib(name, kwargs.get('value', any))

    def __check_attrib(self, name, value):
        return name in self.attribs and (
            value is any or
            (isinstance(value, tuple) and self.attribs[name] in value) or
            self.attribs[name] == value)

    def has_attribs(self, **kwargs):
        if 'from_' in kwargs:
            kwargs['from'] = kwargs.pop('from_')
        return all(self.__check_attrib(n, v) for n, v in kwargs.items())

    def remove_attr(self, name):
        if name in self.attribs:
            del self.attribs[name]

    def set_text(self, val):
        self.text = val
        return self

    def __set_attr(self, **kwargs):
        # for k, v in kwargs.items():
        #     self.attribs[k] = v
        self.attribs.update(kwargs)

    def set_attr(self, name=None, val=None, **kwargs):
        if name is not None:
            kwargs[name] = val
        self.__set_attr(**kwargs)
        return self

    def add_child(self, tag, **kwargs):
        n = Node(tag)
        self.childs.append(n)
        if kwargs:
            n.__set_attr(**kwargs)
        return n

    def insert_child(self, tag, index):
        n = Node(tag)
        self.childs.insert(index, n)
        return n

    def index(self, name, attr=None):
        for idx, c in enumerate(self.childs):
            if c.Tag == name:
                if attr is None:
                    return idx
                if all([c.has_attr(k, value=v) for k, v in attr.items()]):
                    return idx
        return None

    def remove_node(self, name):
        if isinstance(name, int):
            del self.childs[name]
            return
        idx = 0
        for c in self.childs:
            if c.Tag == name:
                del self.childs[idx]
                return
            idx += 1

    def __match(self, node, name, attr=None):
        return node.Tag == name and (attr is None or node.has_attribs(**attr))

    def node(self, name, attr=None, recursive=False):
        for c in self.childs:
            if self.__match(c, name, attr):
                return c
            elif recursive:
                r = c.node(name, attr=attr, recursive=True)
                if r is not None:
                    return r
        return None

    def nodes(self, name, attr=None):
        ret = []
        for n in self.childs:
            if self.__match(n, name, attr):
                ret.append(n)
        return ret

    def append_attr(self, name, wv):
        if name in self.attribs:
            self.attribs[name] += wv
        return self

    def __len__(self):
        return len(self.childs)

    def __bool__(self):
        return True

    def __str__(self):
        return self.__repr__()

    def __eq__(self, other):
        if not isinstance(other, Node):
            return False
        return self.Tag == other.Tag and\
            self.attribs == other.attribs and\
            self.text == other.text and\
            self.childs == other.childs

    def __repr__(self):
        a = ' '.join(['%s="%s"' % (n, v) for n, v in self.attribs.items()])
        if a:
            a = ' ' + a
        end = '/'
        end2 = ''
        if self.text or self.childs:
            end = ''
            if self.text:
                end2 += self.text
            if self.childs:
                end2 += ''
                for n in self.childs:
                    end2 += str(n)
            end2 += '</%s>' % self.Tag
        s = '<%s%s%s>%s' % (self.Tag, a, end, end2)
        return s.replace('\n', '')

    def to_bytes(self) -> bytes:
        return bytes(str(self), 'utf-8')


class Parser(object):
    def __init__(self):
        object.__init__(self)
        self.log = logging.getLogger(AppName + '.Parser')
        self.reset()

    def reset(self):
        self.__tag_name = ""
        self.__node_stack = []
        self.__attrs = {}
        self.__attr_val = ""
        self.__attr_name = ""
        self.__can_exit = False
        self.__node_root = None
        self.__attr_val_start = None
        self.__parsers = [self.__parse_base]
        self.extras = []
        self.problems = False
        self.isCloseTag = False
        self.inLineNode = False

    def __pop(self):
        if self.__node_stack and self.__node_stack[-1].Tag == self.__tag_name:
            self.__node_stack.pop()
        self.__tag_name = ""

    def __add_node(self, tag, add=True):
        n = Node(tag)
        n.attribs = self.__attrs
        if self.__node_root is None:
            self.__node_root = n
        else:
            if self.__node_stack:
                parent_node = self.__node_stack[-1]
                parent_node.childs.append(n)
                parent_node.text = None
            else:
                self.extras.append(n)
        if add:
            self.__node_stack.append(n)
        return n

    def __parse_base(self, c):
        if c == '<':
            self.__tag_name = ''
            self.__attrs = {}
            self.isCloseTag = False
            self.inLineNode = False
            self.__log("goto tag_name")
            self.__parsers.append(self.__parse_tag_name)
        elif self.__node_stack:
            current_node = self.__node_stack[-1]
            if current_node.childs and c in [' ', '\n', '\r', '\t']:
                ''' don't add text if we already have child nodes '''
                return
            try:
                current_node.text += c
            except Exception:
                current_node.text = c

    def __parse_xml_head(self, c):
        if c == '?':
            self.__can_exit = True
        elif c == '>' and self.__can_exit:
            self.__parsers.pop()
            self.__can_exit = False
        else:
            self.__can_exit = False

    def __parse_tag_name(self, c):
        if c in [' ', '\n', '\t']:
            self.__log("go to attributes")
            self.__parsers.append(self.__parse_attributes)
        elif c == '/':
            self.isCloseTag = (self.__tag_name == "")
            self.inLineNode = not self.isCloseTag  # (self.__tag_name != "")
        elif c == '>':
            ret = None
            if not self.isCloseTag:
                ret = self.__add_node(self.__tag_name, not self.inLineNode)
            elif self.__node_root is None:
                ret = self.__add_node(self.__tag_name).is_only_a_close = True
            self.__log(
                "go back ", tag=self.__tag_name, ie=self.isCloseTag,
                inl=self.inLineNode)
            self.__parsers.pop()
            if self.isCloseTag:
                self.__pop()
                if not self.__node_stack:
                    return self.__node_root
            return ret
        elif not self.__tag_name and c == '?':
            self.__parsers[-1] = self.__parse_xml_head
        else:
            self.__tag_name += c

    def __parse_attributes(self, c):
        if c in ['>', '/']:
            self.__log("go back")
            self.__parsers.pop()
            self.__execute(c)
        elif c == '=':
            # print("goto attribute_value %s" % self.__attr_name)
            self.__parsers.append(self.__parse_attribute_value)
        elif c == ' ':
            pass
        else:
            self.__attr_name += c

    def __parse_attribute_value(self, c):
        if self.__attr_val_start is None:
            if c == ' ':
                pass
            elif c in ['\'', '"']:
                self.__attr_val_start = c
            else:
                self.log.error("Expected ' or \" got '%s'" % c)
                self.problems = True
        else:
            if self.__attr_val_start == c:
                self.__parsers.pop()
                self.__attrs[self.__attr_name] = self.__attr_val
                self.__attr_val = ""
                self.__attr_name = ""
                self.__attr_val_start = None
            else:
                self.__attr_val += c

    def __log(self, s, **k):
        return
        i = '  ' * len(self.__parsers)
        v = ', '.join(['%s=%s' % (x, y) for x, y in k.items()])
        d = ' nodes:%s' % len(self.__node_stack)
        ''' self.log.debug("%s%s%s%s", i, s, v, d) '''

    def __execute(self, c):
        self.__parsers[-1](c)

    def is_full(self):
        # for p in self.__parsers:
        #     print(p.__name__, len(self.__parsers))
        return len(self.__parsers) == 1 and\
            self.__parsers[0].__func__ is self.__parse_base.__func__ and\
            not self.__node_stack

    def parse(self, text):
        self.reset()
        if isinstance(text, bytes):
            text = text.decode('utf-8')
        self.update(text)
        return self.__node_root

    def update(self, text):
        if isinstance(text, bytes):
            text = text.decode('utf-8')
        for c in text:
            self.__parsers[-1](c)
        return self.__node_root

    def iterate(self, text):
        for c in text:
            obj = self.__parsers[-1](c)
            if obj is not None and self.is_full():
                self.__node_root = None
                yield obj
