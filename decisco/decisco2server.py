import decisco.jnod
import decisco.colors as colors
import decisco.shared
import decisco.sso.ssoobj
import decisco.dsockets as dsockets
import decisco.dexceptions as dexceptions


class Decisco2Server(object):
    def __init__(self, shared: decisco.shared.SharedResource, server: decisco.dsockets.Socket):
        object.__init__(self)
        self.shared = shared
        self.server = None
        self.log = self.shared.options.get_logger('decisco<->server')
        self._id = None
        self.data = None
        self.fromTo = None
        self.shared.register_slot('got_conf', self.__got_conf_slot)
        self.shared.register_slot('got_fromTo', self.__got_fromTo_slot)
        if server is not None:
            self.__set_server_socket(server)

    def cleanup(self, socked_del=True):
        self.shared.unregister_slot('got_conf')
        self.shared.unregister_slot('got_fromTo')
        self.shared.selector.unregister(self.server)
        ret = self.server
        if not socked_del:
            self.server = None
        if self.server is not None:
            self.server.close()
        return ret

    def __on_socket_error(self, sock: dsockets.Socket):
        self.__stop_setup()

    def __set_server_socket(self, sock: dsockets.Socket):
        self.server = sock
        self.data = dsockets.OutBuffer(self.server, self.shared.selector, self.shared.options.jam_thres)
        self.shared.selector.register(self.server, self.server.name, self.ready_read, self.ready_write).\
            for_out().set_close_action(self.__on_socket_error)
        self.shared.emit('got_ip_address', sock.getsockname()[0])

    def __get_start(self, fromTo: str) -> bytes:
        return bytes(
            "<?xml version='1.0' ?><stream:stream to='%s' xmlns='jabber:client'"
            " xmlns:stream='http://etherx.jabber.org/streams' xml:lang='en' version='1.0'>" % fromTo, 'utf-8')

    def __stop_setup(self):
        self.shared.emit('client_disconnect')
        self.shared.emit('stop_setup', delayed=True)

    def __create_cisco(self, addr: tuple, fromTo: str):
        self.log.info("Connecting to cisco server: %s", addr)
        tracer = self.shared.tracing
        cisco = dsockets.ProxySslSocket(options=self.shared.options, name='Decisco<-->Server', tracer=tracer)
        try:
            cisco.connect(addr)
            cisco.set_tcp_no_delay()
            cisco.set_error_receiving()
            cisco.set_tcp_user_timeout(self.shared.options.timeout)
            cisco.setblocking(0)
            self.__set_server_socket(cisco)
        except (dexceptions.ProxyError, dexceptions.SslSocketError, dexceptions.AddressError):
            self.log.exception("Unable to connect cisco server")
            self.__stop_setup()
            return False
        self.data.schedule(self.__get_start(fromTo))

    def __got_conf_slot(self, conf: decisco.sso.ssoobj.Container):
        self.log.info('got conf')
        if self.fromTo is not None and self.server is None:
            self.shared.extra_stuff.jabberid = conf.screenname
            self.__create_cisco((conf.xmppjabbercluster, 443), self.fromTo)

    def __got_fromTo_slot(self, fromTo: str):
        self.log.info('got fromTo')
        self.fromTo = fromTo
        if self.server is None:
            conf = self.shared.client_auth.get_conf()
            if conf is not None:
                self.__create_cisco((conf.xmppjabbercluster, 443), fromTo)
        else:
            self.data.schedule(self.__get_start(fromTo))

    def __get_auth(self, jabberToken: str) -> bytes:
        return bytes(
            "<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='WEBEX-TOKEN'>%s</auth>\n" % jabberToken,
            'utf-8')

    def __got_features(self):
        to_send = self.__get_auth(self.shared.client_auth.conf.jabbertoken)
        self.data.schedule(to_send)

    def __got_start_token(self, data: decisco.jnod.Node):
        dom = data.get('from')
        if dom != self.fromTo:
            self.log.error(
                "Server requested different domain: %s,"
                " expected: %s" % (dom, self.fromTo))
        self._id = data.get('id', None)
        self.log.debug("Received id: %s" % self._id)
        if data.childs:
            features = data.childs[0]
            if features.Tag != 'stream:features':
                self.log.error('Expected "stream:features", got %s', features.Tag)
                return
            else:
                self.__got_features()
        else:
            self.log.debug('Server did not send authentication method info')

    def __got_authentication_success(self, data: bytes):
        self.shared.client_auth.authenticated_server()
        self.shared.emit('server_finished_auth', data, False)

    def __got_authentication_failure(self, data: decisco.jnod.Node, pdata: bytes):
        ''' Forward failure to client '''
        self.shared.emit('server_finished_auth', pdata, True)
        if data.node('not-authorized') is not None:
            self.log.error('Not Authorized')
        else:
            self.log.error('Cisco jabber rejected - data: %s', data)
            self.log.error('Cisco jabber rejected - raw data: %s', pdata)
        self.data.schedule(b'</stream:stream>')
        self.__stop_setup()

    def __process_server_data(self, data: bytes):
        p = decisco.jnod.Parser()
        pdata = data
        data = p.parse(data)
        if data is None:
            self.log.warning("Failed to parse: %s", pdata)
            return
        if data.Tag == 'stream:stream':
            self.__got_start_token(data)
        elif data.Tag == 'stream:features':
            self.__got_features()
        elif data.Tag == 'success':
            self.__got_authentication_success(pdata)
        elif data.Tag == 'failure':
            self.__got_authentication_failure(data, pdata)

    def ready_read(self):
        data = self.server.read()
        self.log.debug("%sServer->decisco:\n%s%s", colors.cLiRed, data, colors.cDefault)
        self.__process_server_data(data)

    def ready_write(self):
        self.data.send()
