import os
import sys
import logging
import argparse


CISCO_FEDERATED_SSO = "https://loginp.webexconnect.com/cas/FederatedSSO?org=%s&type=connect2"


class DeciscoArgs(argparse.Namespace):
    def __init__(self, **kwargs):
        argparse.Namespace.__init__(self, **kwargs)
        self.AppName = "decisco"
        self.verbose = 0
        self.method = 'web'
        self.syslog = False
        self.log_file = None
        self.use_proxy = False
        self.xmpp_trace = None
        self.connected_directly = False
        self.organization = None
        self.federated_sso = None

    def set_organization(self, organization: str):
        self.organization = organization
        self.federated_sso = CISCO_FEDERATED_SSO % organization

    def get_logger(self, extra_name: str=None) -> logging.Logger:
        name = self.AppName
        if extra_name is not None:
            name += '.%s' % extra_name
        return logging.getLogger(name)


OPTIONS_OBJ = DeciscoArgs()
PARSER_OBJ = argparse.ArgumentParser(description='Setup Decisco suitable for your needs.')


def add_option_group(group_name: str, *args, parent: argparse._ArgumentGroup=None) -> argparse._ArgumentGroup:
    global PARSER_OBJ
    return (parent or PARSER_OBJ).add_argument_group(group_name, *args)


def add_argument(*args, group: argparse._ArgumentGroup=None, **kwargs):
    if group is None:
        global PARSER_OBJ
        group = PARSER_OBJ
    return group.add_argument(*args, **kwargs)


def get_logger_args(options: DeciscoArgs, fmt, datefmt, s_handlers):
    ret = {}
    handlers = []
    handler = None
    formater = logging.Formatter(fmt, datefmt=datefmt)
    if options.syslog:
        handler = logging.handlers.SysLogHandler(address='/dev/log')
        handlers.append(handler)
        s_handlers.append('syslog')
    if options.log_file is not None:
        handler = logging.FileHandler(options.log_file, mode='w')
        # handler = logging.handlers.RotatingFileHandler(
        #     options.log_file, mode='w', maxBytes=15 * 1024 ** 2,
        #     backupCount=1, encoding=None, delay=0)
        handler.setFormatter(formater)
        handlers.append(handler)
        s_handlers.append('file[%s]' % options.log_file)
    if handler is None:
        handler = logging.StreamHandler()
        handler.setFormatter(formater)
        handlers.append(handler)
        s_handlers.append('stream')
    ret['handlers'] = handlers
    return ret


def setup_logger(options: DeciscoArgs):
    s_handlers = []
    datefmt = '%m-%dT%H%M%S'
    fmt = '%(asctime)s.%(msecs)03d %(levelname)8s: %(filename)s:%(lineno)4s %(name)s.%(message)s'
    args = get_logger_args(options, fmt, datefmt, s_handlers)
    log_level = logging.INFO
    if options.verbose > 0:
        log_level = logging.DEBUG
    args['format'] = fmt
    args['level'] = log_level
    args['datefmt'] = datefmt
    logging.basicConfig(**args)
    logging.debug("Logging to %s", ', '.join(s_handlers))


class ToggleAction(argparse.Action):
    def __init__(self, option_strings, dest, **kwargs):
        kwargs['nargs'] = 0
        helpmsg = kwargs.get('help', '')
        if helpmsg:
            helpmsg += ' '
        import decisco.colors as c
        g, y, d = c.cGreen, c.cLiYellow, c.cDefault
        kwargs['help'] = helpmsg + '%sdefault: %s%%(default)s%s' % (g, y, d)
        super(ToggleAction, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, not self.default)


PARSER_OBJ.register('action', 'toggle', ToggleAction)


def get_options_object():
    global OPTIONS_OBJ
    return OPTIONS_OBJ


def early_setup():
    global OPTIONS_OBJ
    setup_logger(OPTIONS_OBJ)


def set_debugability_options():
    grp = add_option_group("Logging & Debugability")
    grp.add_argument("-v", "--verbose", dest="verbose", type=int, default=0, help="verbose mode, default: %(default)s")
    grp.add_argument("-l", "--log-file", dest="log_file", default=None, help="Location of log file location.")
    grp.add_argument("--use-syslog", dest="syslog", default=False, action="store_true", help="enable use of syslog")
    grp.add_argument("-T", "--xmpp-trace", dest="xmpp_trace", default=None, help="Specifies path to xmpp traces.")


def get_options_and_args() -> DeciscoArgs:
    global PARSER_OBJ, OPTIONS_OBJ
    grp = add_option_group("Configuration & Set-up")
    grp.add_argument(
        "-P", "--use-client-password", dest="use_client_password", default=True, action='toggle',
        help="client provided password is used in authenticate against federated SSO.")
    grp.add_argument(
        "-c", "--config-file", dest="config_file",
        help="specifies the python module file which contains configure method.")

    set_debugability_options()
    options = PARSER_OBJ.parse_args(namespace=OPTIONS_OBJ)
    logging.root.handlers = []  # clear logger and setup again
    setup_logger(options)
    return options


__ALL__ = ["DeciscoArgs", "get_options_and_args"]
