import sys

# constants
cRed = "\33[1;31m"
cLiRed = "\33[31m"
cGreen = "\33[1;32m"
cYellow = "\33[1;33m"
cBlue = "\33[1;34m"
cLiBlue = "\33[34m"
cPurple = "\33[1;35m"
cWhite = "\33[1;37m"
cDefault = "\33[0m"
cLiGreen = "\33[32m"
cLiYellow = "\33[33m"


if not sys.stdout.isatty():
    cRed = ""
    cLiRed = ""
    cGreen = ""
    cYellow = ""
    cBlue = ""
    cLiBlue = ""
    cPurple = ""
    cWhite = ""
    cDefault = ""
    cLiGreen = ""
    cLiYellow = ""
