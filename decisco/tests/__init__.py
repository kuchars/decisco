import os
import logging
import decisco.args
# import decisco.sso.helpers
import decisco.sso.credentials


class Options(object):
    def __init__(self, organization='example.com'):
        object.__init__(self)
        self.xml = None
        self.alu = False
        self.verbose = 0
        self.timeout = 50
        self.jam_thres = 10
        self.syslog = False
        self.log_file = None
        self.method = 'curl'
        self.AppName = "Test"
        self.web_port = 8080
        self.use_proxy = True
        self.web_debug = False
        self.xmpp_trace = None
        self.proxy_port = 8080
        self.experimental = True
        self.auto_js_auth = False
        self.override_xmpp = None
        self.cisco_features = True
        self.override_swapi = None
        self.cookie_filename = None
        self.local_xmpp_port = 5222
        self.wait_for_socket = False
        self.organization = organization
        self.use_client_password = False
        self.local_xmpp_host = 'localhost'
        self.username = os.environ.get('USER', None)
        self.proxy_host = "plwrprx-fiesprx.glb.nsn-net.net"
        self.credentials = os.path.expanduser('~/.ssh/cred')
        self.local_mes_storage = '~/.decisco_offline_storage.pkl'
        self.federated_sso = decisco.args.CISCO_FEDERATED_SSO % self.organization
        self.cred_obj = decisco.sso.credentials.Credentials(self)
        self.webbrowser = 'xdg-open'
        self.user_input_name = ['USER']
        self.login_button_value = ['Login']
        self.email_input_name = ['USERNAME']
        self.pass_input_name = ['PASSWORD', 'TEMPPASSWORD']
        self.auth_failure_indicator = ['Authentication Error']
        if os.path.exists(self.credentials):
            self.cred_obj.load_data(True)
        else:
            self.cred_obj.get_auth_data_from_console(True)
        # self.auth_error_retr = decisco.sso.helpers.get_domain_error_retriever(self.organization)
        self.cookie_filename = os.path.abspath(os.path.expanduser('~/.decisco_sso.cookie'))

    def get_logger(self, name):
        return logging.getLogger(name)


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)8s: %(lineno)4s %(name)s.%(message)s',
    datefmt='%m-%d %H:%M:%S')
