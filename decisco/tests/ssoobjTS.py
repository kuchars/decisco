import datetime
import unittest
import decisco.sso.ssoobj


class SsoObjTestCase(unittest.TestCase):
    def assertRaisesWithMessage(self, msg, func, *args, **kwargs):
        try:
            func(*args, **kwargs)
            self.assertFail()
        except Exception as inst:
            self.assertEqual(str(inst), msg)

    def test_simple_valid(self):
        T = """
        <federatedSSO>
        <title>Connect Client Single Sign On</title>
        <status>SUCCESS</status>
        <screenname>john.smith@domain.com</screenname>
        <createtime>1538232391624</createtime>
        <token>
        ABC/+DEF
        </token>
        <timetolive>1209600</timetolive>
        <serviceurl>x03swapi.webexconnect.com/wbxconnect</serviceurl>
        <opisitem>WebExJabber</opisitem>
        <jabberToken>
        jabber_tocken/
        </jabberToken>
        <jabberCluster>isj7cmx-gw.webexconnect.com</jabberCluster>
        <jabberName>isj7</jabberName>
        <xmppjabbercluster>
        <![CDATA[ isj7cmx.webexconnect.com ]]>
        </xmppjabbercluster>
        <boshurl>
        <![CDATA[ https://im7.ciscowebex.com/http-bind ]]>
        </boshurl>
        </federatedSSO>
        """
        sut = decisco.sso.ssoobj.Container(T, None)
        self.assertTrue(sut.status)
        self.assertEqual(sut.opisitem, 'WebExJabber')
        self.assertEqual(sut.screenname, 'john.smith@domain.com')
        self.assertEqual(sut.boshurl, 'https://im7.ciscowebex.com/http-bind')
        self.assertEqual(sut.token, 'ABC/+DEF')
        self.assertEqual(sut.createtime, '1538232391624')
        self.assertEqual(sut.timetolive, '1209600')
        self.assertEqual(sut.serviceurl, 'x03swapi.webexconnect.com/wbxconnect')
        self.assertEqual(sut.jabbername, 'isj7')
        self.assertEqual(sut.jabbertoken, 'jabber_tocken/')
        self.assertEqual(sut.jabbercluster, 'isj7cmx-gw.webexconnect.com')
        self.assertEqual(sut.xmppjabbercluster, 'isj7cmx.webexconnect.com')
        tz = decisco.sso.ssoobj.LocalTimezone()
        self.assertEqual(sut.expires, datetime.datetime(2018, 9, 29, 17, 6, 41, tzinfo=tz))

    def test_failure_mode(self):
        T = """
        <federatedSSO>
        <title>Connect Client Single Sign On</title>
        <status>FAILURE</status>
        <errorcode>12345</errorcode>
        <reason>Failure reason</reason>
        </federatedSSO>
        """
        failure = 'Connect Client Single Sign On - FAILURE(12345): Failure reason'
        self.assertRaisesWithMessage(failure, decisco.sso.ssoobj.Container, T, None)

