import base64
import logging
import unittest
import unittest.mock
import decisco.forward
import decisco.sso.credentials


class FakeOpt(object):
    def __init__(self):
        object.__init__(self)
        self.xmpp_trace = None
        self.jam_thres = 10
        self.cisco_features = True
        self.local_host = "local_host"
        self.local_port = "local_port"
        self.use_client_password = True
        self.credentials = "credentials"
        self.organization = "example.com"
        self.local_xmpp_host = "local_xmpp_host"
        self.local_xmpp_port = "local_xmpp_port"
        self.wait_for_socket = "wait_for_socket"
        self.local_mes_storage = 'storage.pkl'
        self.experimental = True
        self.username = None
        self.proxy_host = None
        self.timeout = None
        self.cred_obj = decisco.sso.credentials.Credentials(self)

    def get_logger(self, *args):
        return logging


class ForwardestCase(unittest.TestCase):
    @unittest.mock.patch('decisco.dsockets.SocketSelector')
    @unittest.mock.patch('decisco.swapi.GetJabberToken')
    @unittest.mock.patch('decisco.dsockets.FakeXmppServer')
    def setUp(self, xmppserv_mock, gjt_mock, ss_mock):
        self.ss_obj_mock = unittest.mock.MagicMock()
        self.gjt_obj_mock = unittest.mock.MagicMock()
        self.xmppserv_obj_mock = unittest.mock.MagicMock()
        ss_mock.return_value = self.ss_obj_mock
        gjt_mock.return_value = self.gjt_obj_mock
        xmppserv_mock.return_value = self.xmppserv_obj_mock

        opt = FakeOpt()
        self.sut = decisco.forward.XmppForwarder(opt)
        ss_mock.assert_called_once_with(opt)
        gjt_mock.assert_called_once_with(opt)
        xmppserv_mock.assert_called_once_with(opt.local_xmpp_host, opt.local_xmpp_port, opt.wait_for_socket, logging)
        self.xmppserv_obj_mock.bind.assert_called_once_with()

    def test_run(self):
        ANY = unittest.mock.ANY
        self.ss_obj_mock.register.assert_called_once_with(ANY, 'decisco', ANY, None)
        self.sut._main_loop = unittest.mock.MagicMock()
        self.sut.run()
        self.sut._main_loop.assert_called_once_with()

    def test_main_loop_iteration(self):
        self.sut._main_loop_element()
        self.ss_obj_mock.select.assert_called_once_with(1)

    def generate_methods(self, index: int):
        entry = self.ss_obj_mock.register.call_args_list[index][0]
        socket = entry[0]
        socket_read_ready = entry[2]
        socket_write_ready = entry[3]

        def simulate_socket_write_ready():
            socket_write_ready()
            socket_write_ready()

        def simulate_socket_read(data):
            socket.read.return_value = data
            socket_read_ready()
            simulate_socket_write_ready()

        def check_socket_sends(*args):
            calls = [unittest.mock.call(a) for a in args]
            socket.send.assert_has_calls(calls)
            self.assertEqual(len(calls), len(socket.send.call_args_list), socket.send.call_args_list)
            socket.send.call_args_list = []

        return simulate_socket_write_ready, simulate_socket_read, check_socket_sends

    @unittest.mock.patch('decisco.dsockets.Socket')
    @unittest.mock.patch('decisco.dsockets.ProxySslSocket')
    def authenticate_client(self, proxy_sock_mock, sock_mock):
        client_sock_mock = unittest.mock.MagicMock()
        client_sock_mock.name = 'Socket.Name'
        client_sock_mock.send.side_effect = lambda d: len(d)

        sock_mock.return_value = client_sock_mock
        self.xmppserv_obj_mock.accept.return_value = ('CLIENT', 'ADDRESS')

        # Simulate Client connect
        self.ss_obj_mock.register.call_args_list[0][0][2]()

        sock_mock.assert_called_once_with('CLIENT', name='Client<-->Decisco', tracer=unittest.mock.ANY)

        simulate_client_write_ready, simulate_client_read, check_client_sends = self.generate_methods(1)

        # Client sends bonjour
        simulate_client_read(
            b"<?xml version='1.0' encoding='UTF-8'?>\n"
            b"<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams'"
            b" to='example.com' version='1.0'>\n")

        # check decisco response
        check_client_sends(
            b'<?xml version="1.0"?>\n<stream:stream from="example.com" id="0000" xmlns:stream="http://etherx.'
            b'jabber.org/streams" version="1.0" xmlns="jabber:client">\n',
            b'<stream:features><mechanisms xmlns="urn:ietf:params:xml:ns:xmpp-sasl"><mechanism>PLAIN</mechanism>'
            b'</mechanisms></stream:features>\n')

        # prep mocks
        cisco_sock_mock = unittest.mock.MagicMock()
        cisco_sock_mock.send.side_effect = lambda d: len(d)
        proxy_sock_mock.return_value = cisco_sock_mock

        auth_container = unittest.mock.MagicMock()
        self.gjt_obj_mock.aquire.return_value = auth_container

        self.assertEqual(2, len(self.ss_obj_mock.register.call_args_list))

        # client starts authentication
        simulate_client_read(
            b'<auth wocky-zb:client-uses-full-bind-result="true" mechanism="PLAIN" '
            b'xmlns:wocky-zb="http://www.google.com/talk/protocol/auth" xmlns="urn:ietf:params:xml:ns:xmpp-sasl">' +
            base64.b64encode(b'\x00john.smith\x00password') +
            b'</auth>')

        self.gjt_obj_mock.aquire.assert_called_once_with()
        self.assertEqual(client_sock_mock.send.call_args_list, [])

        # prep
        self.assertEqual(3, len(self.ss_obj_mock.register.call_args_list))
        simulate_cisco_write_ready, simulate_cisco_read, check_cisco_sends = self.generate_methods(2)

        # check hello message from decisco to cisco server
        simulate_cisco_write_ready()
        check_cisco_sends(
            b"<?xml version='1.0' ?><stream:stream to='example.com' xmlns='jabber:client' xmlns:stream='http://etherx"
            b".jabber.org/streams' xml:lang='en' version='1.0'>")

        auth_container.jabbertoken = 'ABC_OK'
        simulate_cisco_read(
            b"<stream:stream xmlns='jabber:client' xml:lang='en-US.UTF-8' xmlns:stream='http://etherx.jabber.org/"
            b"streams' from='example.com' id='0001' version='1.0'>\n"
            b"<stream:features><mechanisms xmlns='urn:ietf:params:xml:ns:xmpp-sasl'><mechanism>WEBEX-TOKEN</mechanism>"
            b"<mechanism>PLAIN</mechanism></mechanisms></stream:features>")
        # handle auth req to cisco
        check_cisco_sends(b"<auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' mechanism='WEBEX-TOKEN'>ABC_OK</auth>\n")
        # cisco_sock_mock.send.call_args_list = []

        # authentication is succesfull - switch to forwarding mode
        simulate_cisco_read(b"<success xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>")

        self.simulate_clinet_write_ready, self.simulate_client_read, self.check_client_sends = self.generate_methods(3)
        self.simulate_cisco_write_ready, self.simulate_cisco_read, self.check_cisco_sends = self.generate_methods(4)
        self.client_socket = self.ss_obj_mock.register.call_args_list[3][0][0]
        self.cisco_socket = self.ss_obj_mock.register.call_args_list[4][0][0]
        self.simulate_clinet_write_ready()
        self.simulate_cisco_write_ready()

        self.check_client_sends(b"<success xmlns='urn:ietf:params:xml:ns:xmpp-sasl'/>")
        self.check_cisco_sends()

    def test_client_connects_and_authentication(self):
        self.authenticate_client()

    def test_client_disconnect(self):
        self.authenticate_client()
        self.simulate_client_read(b"</stream:stream>")

    def test_server_disconnect(self):
        self.authenticate_client()
        self.simulate_cisco_read(b"</stream:stream>")

    def test_second_client(self):
        self.authenticate_client()

        client_sock_mock = unittest.mock.MagicMock()
        self.xmppserv_obj_mock.accept.return_value = (client_sock_mock, 'ADDRESS')

        # Simulate Client connect
        self.ss_obj_mock.register.call_args_list[0][0][2]()
        client_sock_mock.close.assert_called_once_with()

    def test_main_loop_element_error_handling(self):
        def raise_io_err(*args):
            raise IOError()

        self.ss_obj_mock.select.side_effect = raise_io_err

        with self.assertRaises(decisco.forward.SomethingGoneTottalyWrong):
            self.sut._main_loop_element()

    def test_main_loop_element_error_handling_other_exception(self):
        def raise_io_key_error(*args):
            raise KeyError()

        self.ss_obj_mock.select.side_effect = raise_io_key_error

        with self.assertRaises(decisco.forward.SomethingGoneTottalyWrong):
            self.sut._main_loop_element()

    def test_main_loop_element_error_handling_interupt(self):
        import errno

        def raise_io_err(*args):
            e = IOError()
            e.errno = errno.EINTR
            raise e

        self.ss_obj_mock.select.side_effect = raise_io_err
        self.sut._main_loop_element()

    def test_main_loop_element_exit_by_keyboard(self):
        def raise_io_int(*args):
            raise KeyboardInterrupt()

        self.ss_obj_mock.select.side_effect = raise_io_int
        self.sut.run()

    def test_continue_forwarding(self):
        self.authenticate_client()
        self.client_socket.get_unsend_bytes_count.return_value = 100
        self.cisco_socket.get_unsend_bytes_count.return_value = 100
        self.simulate_client_read(
            b"<?xml version='1.0' encoding='UTF-8'?>\n"
            b"<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' to='example.com'"
            b" version='1.0'>\n")

        self.simulate_cisco_read(
            b"<stream:stream xmlns='jabber:client' xml:lang='en-US.UTF-8' xmlns:stream='http://etherx.jabber.org/"
            b"streams' from='example.com' id='0001' version='1.0'>\n")
        self.simulate_cisco_read(
            b"<stream:features><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'/><session xmlns='urn:ietf:params:xml:ns:xmpp-session'/><sm xmlns='urn:xmpp:sm:3'><optional/></sm><mdm xmlns='http://protocols.cisco.com/mdm:1'><optional/></mdm><push xmlns='http://protocols.cisco.com/push:2'><service>WNS</service><service>WNS:dev</service><service>APNS</service><service>APNS:beta</service><service>APNS:dev</service><service>FCM</service><service>FCM:dev</service><encrypt><algorithm>A256GCM</algorithm></encrypt><optional/></push></stream:features>"
            )

        self.simulate_client_read(b'<iq type="set" id="0001"><bind xmlns="urn:ietf:params:xml:ns:xmpp-bind"><resource>resource_name</resource></bind></iq>')
        self.simulate_cisco_read(b"<iq id='0001' type='result'><bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'><jid>john.smith@example.com/resource_name</jid></bind></iq>")






