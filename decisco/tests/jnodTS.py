import decisco.jnod as jnod
import unittest


class ParserTestCase(unittest.TestCase):
    def setUp(self):
        self.sut = jnod.Parser()

    def __check(self, ret, **kwargs):
        self.assertTrue(ret is not None)
        self.assertIsInstance(ret, jnod.Node)
        if 'tag' in kwargs:
            self.assertEqual(ret.Tag, kwargs.get('tag'))
        self.assertEqual(ret.text, kwargs.get('text', None))
        self.assertEqual(len(ret.childs), kwargs.get('child_count', 0))
        if 'attrs' in kwargs:
            self.assertEqual(ret.attribs, kwargs.get('attrs'))
        else:
            self.assertEqual(len(ret.attribs), kwargs.get('attr_count', 0))
        self.assertEqual(self.sut.problems, kwargs.get('problems', False))
        self.assertEqual(self.sut.isCloseTag, kwargs.get('is_close', True))
        self.assertEqual(self.sut.inLineNode, kwargs.get('in_line', False))
        self.assertEqual(self.sut.is_full(), kwargs.get('is_full', True))

    def test_simple_open_close(self):
        s = "<a></a>"
        ret = self.sut.parse(s)
        self.__check(ret, tag='a')

    def test_simple_open_close_with_text(self):
        s = "<a>simple text</a>"
        ret = self.sut.parse(s)
        self.__check(ret, tag='a', text='simple text')

    def test_simple_open_close_with_attributes(self):
        s = "<a a='attr a' b='attr b'></a>"
        ret = self.sut.parse(s)
        self.__check(ret, tag='a', attrs={'a': 'attr a', 'b': 'attr b'})

    def test_simple_open(self):
        s = "<a>"
        ret = self.sut.parse(s)
        self.__check(ret, tag='a', is_close=False, is_full=False)

    def test_simple_close(self):
        s = "</a>"
        ret = self.sut.parse(s)
        self.__check(ret, tag='a', is_close=True)

    def test_complex_split_into_ppieces(self):
        s1 = "<a a='attr a' b"
        s2 = "='attr b'>"
        s3 = "</a>"
        ret = self.sut.parse(s1)
        self.assertEqual(ret, None)
        self.assertEqual(self.sut.is_full(), False)

        ret = self.sut.update(s2)
        self.assertTrue(ret is not None)
        self.assertEqual(self.sut.is_full(), False)

        ret = self.sut.update(s3)
        self.assertTrue(ret is not None)
        self.assertEqual(self.sut.is_full(), True)
        self.__check(ret, tag='a', attrs={'a': 'attr a', 'b': 'attr b'})

    def test_xml_header(self):
        s = "<?xml version='1.0' ?>"
        ret = self.sut.parse(s)
        self.assertEqual(ret, None)

    def test_complex_(self):
        s = "<iq componentid='...' from='node' id='uid' to='resource' "\
            "type='result'>"\
            "<query xmlns='jabber:iq:search'>"\
            "<x type='result' xmlns='jabber:x:data'>"\
            "<field type='hidden' var='FORM_TYPE'>"\
            "<value>http://webex.com/connect/gs</value>"\
            "</field>"\
            "<reported>...</reported>"\
            "<item>"\
            "<field var='jid'><value>...</value></field>"\
            "<field var='userid'><value>...</value></field>"\
            "</item>"\
            "</x>"\
            "</query>"\
            "</iq>"
        splitters = [2, 6, 10, 20, 30, 35, 50, 123, 165, 233, 299, 303, None]
        prev = 0
        self.sut.parse('')
        for split in splitters:
            a = s[prev: split]
            prev = split
            ret = self.sut.update(a)
            self.assertEqual(self.sut.is_full(), split is None)
        a = {'componentid': '...', 'from': 'node', 'id': 'uid',
             'to': 'resource', 'type': 'result'}
        self.__check(ret, tag='iq', child_count=1, attrs=a)
        self.assertEqual(len(self.sut.extras), 0)
        self.__check(
            ret.childs[0], tag='query', attrs={'xmlns': 'jabber:iq:search'},
            child_count=1)
        self.__check(
            ret.childs[0].childs[0], tag='x', child_count=3,
            attrs={'type': 'result', 'xmlns': 'jabber:x:data'})
        c0 = ret.childs[0].childs[0].childs[0]
        c1 = ret.childs[0].childs[0].childs[1]
        c2 = ret.childs[0].childs[0].childs[2]
        self.__check(
            c0, tag='field', child_count=1,
            attrs={'type': 'hidden', 'var': 'FORM_TYPE'})
        self.__check(c1, tag='reported', text='...')
        self.__check(c2, tag='item', child_count=2)
        self.__check(
            c0.childs[0], tag='value', text='http://webex.com/connect/gs')
        self.__check(
            c2.childs[0], tag='field', child_count=1, attrs={'var': 'jid'})
        self.__check(c2.childs[0].childs[0], tag='value', text='...')
        self.__check(
            c2.childs[1], tag='field', child_count=1, attrs={'var': 'userid'})
        self.__check(c2.childs[1].childs[0], tag='value', text='...')

    def test_complex_multiple_nodes(self):
        s = "<a></a> <b></b>"
        ret = self.sut.parse(s)
        self.__check(ret, tag='a')
        self.assertEqual(len(self.sut.extras), 1)
        self.__check(self.sut.extras[0], tag='b')

    def test_generator(self):
        s = '<a>...</a> <b>...</b> <c>...</c> <d>...</d>'
        lst = list(self.sut.iterate(s))
        self.assertEqual(len(lst), 4)
        self.__check(lst[0], tag='a', text='...')
        self.__check(lst[1], tag='b', text='...')
        self.__check(lst[2], tag='c', text='...')
        self.__check(lst[3], tag='d', text='...')

    def test_parse_presence_unavail(self):
        s = "<presence from='some.user1@example.com/jabber_1519' to='some.user2@example.com/xmpp' type='unavailable'/>"
        ret = self.sut.parse(s)
        # print(self.sut.problems)
        # print(ret)


class NodeTestCase(unittest.TestCase):
    def setUp(self):
        self.sut = jnod.Node('a')

    def test_simple_empty(self):
        self.assertEqual(self.sut.Tag, 'a')

    def test_simple_set_attrs(self):
        # def set_attr(self, name=None, val=None, **kwargs):
        self.sut.set_attr('aname', 'avalue', b='B', c='C')
        self.assertEqual(
            self.sut.attribs, {'aname': 'avalue', 'b': 'B', 'c': 'C'})
        self.assertEqual(self.sut['b'], 'B')
        self.assertEqual(self.sut['c'], 'C')
        self.assertEqual(self.sut.get('b'), 'B')
        self.assertEqual(self.sut.get('c'), 'C')
        self.assertEqual(self.sut.get('d', None), None)
        with self.assertRaises(KeyError):
            self.sut.get('d')

    def test_set_text(self):
        self.sut.set_text('text')
        self.assertEqual(self.sut.text, 'text')

    def test_has_attr(self):
        self.sut.set_attr('aname', 'avalue', b='B', c='C')
        self.assertTrue(self.sut.has_attr('b'))
        self.assertFalse(self.sut.has_attr('b', value=''))
        self.assertTrue(self.sut.has_attr('b', value='B'))

    def test_remove_attr(self):
        self.sut.set_attr(b='B', c='C')
        self.assertTrue(self.sut.attribs, {'b': 'B', 'c': 'C'})
        self.sut.remove_attr('b')
        self.assertTrue(self.sut.attribs, {'c': 'C'})

    def test_rename_attr(self):
        self.sut.set_attr(b='B', c='C')
        self.assertTrue(self.sut.attribs, {'b': 'B', 'c': 'C'})
        self.sut.rename_attr('b', 'B')
        self.assertTrue(self.sut.attribs, {'B': 'B', 'c': 'C'})

    def test_append_attr(self):
        self.sut.set_attr(b='B')
        self.assertTrue(self.sut.attribs, {'b': 'B'})
        self.sut.append_attr('b', 'B')
        self.assertTrue(self.sut.attribs, {'B': 'BB'})
        self.sut.append_attr('c', 'B')
        self.assertTrue(self.sut.attribs, {'B': 'BB'})

    def test_repr_empty(self):
        self.assertEqual(str(self.sut), '<a/>')

    def test_repr_attrs(self):
        self.sut.set_attr('a', 'A')
        self.assertEqual(str(self.sut), '<a a="A"/>')

    def test_repr_text(self):
        self.sut.set_text('text')
        self.assertEqual(str(self.sut), '<a>text</a>')

    def test_repr_child(self):
        self.sut.add_child('b')
        self.assertEqual(str(self.sut), '<a><b/></a>')

    def test_insert_child(self):
        self.sut.add_child('b')
        self.sut.insert_child('c', 0)
        self.assertEqual(str(self.sut), '<a><c/><b/></a>')

    def test_child_index(self):
        self.sut.add_child('b')
        self.sut.insert_child('c', 0)
        self.assertEqual(self.sut.index('c'), 0)
        self.assertEqual(self.sut.index('b'), 1)

    def test_child_index_with_attr(self):
        self.sut.add_child('b')
        self.sut.add_child('b').set_attr('a', 'A')
        self.assertEqual(self.sut.index('a'), None)
        self.assertEqual(self.sut.index('b'), 0)
        self.assertEqual(self.sut.index('b', {'a': 'A'}), 1)

    def test_remove_child(self):
        self.sut.add_child('b')
        self.sut.add_child('c')
        self.assertEqual(len(self.sut), 2)
        self.sut.remove_node('c')
        self.assertEqual(len(self.sut), 1)

    def test_node(self):
        self.sut.add_child('a')
        self.sut.add_child('b').set_attr('b', 'B')
        self.sut.add_child('c')
        c = self.sut.node('c')
        self.assertIsNotNone(c)
        self.assertEqual(c.Tag, 'c')

        b = self.sut.node('b', {'b': 'B'})
        self.assertIsNotNone(b)
        self.assertEqual(b.Tag, 'b')
        # self.sut.remove_node('c')
        # self.assertEqual(len(self.sut), 1)

        self.assertEqual(self.sut.node('d', {'b': 'B'}), None)

    def test_node_as_attribute(self):
        self.sut.add_child('a')
        self.sut.add_child('b').set_attr('b', 'B')
        self.sut.add_child('c')
        self.assertTrue(self.sut.a is not None)
        self.assertTrue(self.sut.d is self.sut)

    def test_attr_as_attribute(self):
        self.sut.set_attr('a', 'A')
        self.sut.add_child('a')

        self.assertEqual(self.sut.a_a, 'A')

    def test_no_such_attribute(self):
        self.sut.set_attr('a', 'A')
        self.sut.add_child('a')

        with self.assertRaises(AttributeError):
            self.assertFalse(self.sut.__gil__)
