# import pycurl
import logging
import xml.dom.minidom
try:
    # Python 3 implementation:
    import http.client as httplib
    import urllib.parse as urlparse
    # import html.parser as html_parser
except ImportError:
    # Python 2 implementation:
    import httplib
    import urlparse
    # import HTMLParser as html_parser

import decisco.sso
import decisco.args
import decisco.utils
import decisco.sso.loader


def _gett(node, *args):
    for a in args:
        lst = node.getElementsByTagName(a)
        if lst:
            node = lst[0]
        else:
            return None
    rc = []
    for node in node.childNodes:
        if node.nodeType in {node.TEXT_NODE, node.CDATA_SECTION_NODE}:
            rc.append(node.data)
    return ''.join(rc).strip()


class Swapi(object):
    def __init__(self, conf, options):
        object.__init__(self)
        self.conf = conf
        self.options = options
        self.resp_code = None
        self.log = options.get_logger('Swapi')
        self.url = "https://%s/op.do" % self.conf.serviceurl

    def login(self):
        d = {
            'isp': 'WBX',
            'cmd': 'login',
            'autocommit': 'true',
            'token': self.conf.token,
            'username': self.conf.screenname,
        }
        s = decisco.utils.urlencode(d)
        self.log.info("swapi: %s", self.url)
        self.log.info("issue login command to swapi: %s...", s)  # [:25])

        u = urlparse.urlsplit(self.url)
        conn = httplib.HTTPSConnection(u.netloc, timeout=self.options.timeout)
        if self.options.use_proxy and self.options.proxy_host:
            conn.set_tunnel(self.options.proxy_host, port=self.options.proxy_port)
        # conn.set_debuglevel(self.options.verbose)
        conn.request('POST', self.url, s, {'Content-Type': 'application/x-www-form-urlencoded;charset:utf-8'})
        res = conn.getresponse()
        content = res.read()
        if res.status != 200:
            self.log.error("Did not get OK(200) from server: %s, %s", res.status, res.reason)
            self.log.debug(content)
        if content is None:
            return False
        mdom = xml.dom.minidom.parseString(content)
        # <wbxapi>
        #   <securityContext>
        #     <cred/>
        #   </securityContext>
        #   <response>
        #     <result>FAILURE</result>
        #     <exceptionID>wapi.missing_cmd</exceptionID>
        #     <reason>wapi.missing_cmd - A call was made without specifying which command to use.</reason>
        # </response>
        # </wbxapi>
        st = _gett(mdom, 'response', 'result')
        try:
            if st == "SUCCESS":
                self.log.info("Swapi login succesfull")
                return True
            self.cred = _gett(mdom, 'securityContext', 'cred')
        except Exception:
            pass
        self.log.error("Swapi login failed")
        return False


class GetJabberToken(object):
    def __init__(self, options: decisco.args.DeciscoArgs):
        object.__init__(self)
        self.options = options
        self.log = options.get_logger('GetJabberToken')
        self.swapi = None

    def aquire(self) -> decisco.sso.ssoobj.Container:
        self.swapi = None
        conf = decisco.sso.loader.get_sso_data(self.options)
        if conf is None:
            self.log.fatal("Could not get configuration")
            return None
        self.log.info("aquire token for %s", conf.screenname)
        # login/authenticate in swapi
        self.swapi = Swapi(conf, self.options)
        if not self.swapi.login():
            self.log.fatal("Login operation failed @ swapi")
            return None
        return conf


if __name__ == '__main__':
    import decisco.tests
    o = decisco.tests.Options()
    o.method = 'web'
    gt = GetJabberToken(o)
    print(gt.aquire())
