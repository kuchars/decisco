import hashlib
import decisco.jnod as jnod
import decisco.extensions.const as const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes
import decisco.extensions.verificationstring as verstr


class UserCapabilities(object):
    CISCO_CAPS = [
        const.webex_ds,
        const.webex_ssl_gw,
        const.aes_file_transfer,
        const.cisco_desktopshare,
        const.webex_picture_share,
        const.webex_picture_share_mix,
    ]

    def __init__(self, *args):
        object.__init__(self)
        self.identity = []
        self.features = []
        self.caps_node = None
        self.org_ver_string = None
        self.caps_request_id = set()
        self.decisco_ver_string = None

    def reset(self):
        self.identity = []
        self.features = []

    def add_feature(self, v):
        if v not in self.features:
            self.features.append(v)

    def add_identity(self, i):
        if i not in self.identity:
            self.identity.append(i)


def get_default_identity():
    return verstr.mk_identity(verstr.DEFAULT_CLIENT)


def get_default_ver_str(caps: UserCapabilities):
    return verstr.mk_verification_string(get_default_identity(), verstr.DEFAULT_CAPABILITIES + caps.CISCO_CAPS)


def add_identity(parent: jnod.Node, id_: verstr.Identity):
    idn = parent.add_child('identity').set_attr(category=id_.category, type=id_.type)
    if id_.lang:
        idn.set_attr('lang', id_.lang)
    if id_.name:
        idn.set_attr('name', id_.name)
    return idn


def add_features(parent: jnod.Node, caps: UserCapabilities):
    features = list(set(verstr.DEFAULT_CAPABILITIES + caps.CISCO_CAPS))
    for f in sorted(features):
        parent.add_child('feature').set_attr('var', f)


def publish_caps_version_string(extra: dextra.ExtraStuff, data: jnod.Node):
    if data.Tag != 'presence' or not data.childs or data.has_attr('from'):
        return
    c = data.node('c', {'node': any})
    if c is None:
        return
    ci = extra.get_container(UserCapabilities)
    ci.org_ver_string = c.get('ver')
    ci.caps_node = c.get('node', None)
    ci.decisco_ver_string = get_default_ver_str(ci)
    c.set_attr('hash', 'sha-1', ver=ci.decisco_ver_string)
    # print("Publish caps, %s, new: %s" % (ci.org_ver_string, ci.decisco_ver_string))
    # print(data)
    return dtypes.SendForward(data)


def return_modified_capabilities(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='user_jid' id='20e:42d3b8c0' to='user_jid/resource' type='get'>
        <query node='node/new_version' xmlns='http://jabber.org/protocol/disco#info'/>
    </iq>
    '''
    if data.Tag != 'iq':
        return
    if not data.has_attr('from') or not data.has_attr('to') or not data.has_attr('type', value='get'):
        return
    to_ = data.get('to')
    from_ = data.get('from')
    if from_ not in to_:
        return
    q = data.node('query', {'xmlns': const.disco_info, 'node': any})
    if q is None:
        return
    ci = extra.get_container(UserCapabilities)
    if ci.decisco_ver_string not in q.get('node'):
        return
    ci.caps_request_id.add(data.get('id'))
    q.set_attr('node', '%s#%s' % (ci.caps_node, ci.org_ver_string))
    # print("Forward disco#info req", ci.org_ver_string, ci.decisco_ver_string)
    # print(data)
    return dtypes.SendForward(data)


def remember_client_caps(extra: dextra.ExtraStuff, data: jnod.Node):
    ci = extra.get_container(UserCapabilities)
    if data.Tag != 'iq' or not data.has_attr('type', value='result'):
        return
    id_ = data.get('id')
    if id_ not in ci.caps_request_id:
        return
    q = data.node('query', {'node': any, 'xmlns': const.disco_info})
    if q is None:
        return
    q.set_attr('node', '%s#%s' % (ci.caps_node, ci.decisco_ver_string))
    ci.caps_request_id.remove(id_)
    ci.identity = []
    for id_ in q.nodes('identity'):
        i = verstr.Identity(id_.get('category', 'client'), id_.get('type', 'pc'), '', id_.get('name', ''))
        ci.add_identity(i)
    for feature in q.nodes('feature'):
        ci.add_feature(feature.get('var'))
    q.childs = []
    for id_ in get_default_identity():
        add_identity(q, id_)
    add_features(q, ci)
    # print("Remember: %r, %s, %s" % (ci.identity, ci.org_ver_string, ci.decisco_ver_string))
    # print("Remember: %r" % len(ci.features))
    # print(data)
    return dtypes.SendForward(data)


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_container(UserCapabilities)
    extra.register_server_handler(return_modified_capabilities)
    extra.register_client_handler(publish_caps_version_string, remember_client_caps)
