import logging
import decisco.jnod as jnod
import decisco.extensions.const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes
import decisco.extensions.utils
import decisco.extensions.imagedesc


class VcardData(object):
    def __init__(self, sockets, uid, to, attrs={}):
        object.__init__(self)
        _from = attrs.get('jid', None)
        if _from is None:
            _from = attrs.get('email', None)
        self.sockets = sockets
        self.uid = uid
        self.to = to
        self.jid = None
        self._from = _from
        self.ssoid = None
        self.email = None
        self.userid = None
        self.picture = None
        self.jobtitle = None
        self.lastname = None
        self.username = None
        self.firstname = None
        self.pictureurl = None
        self.displayname = None
        self.phonenum_csf = None
        self.phonenum_home = None
        self.phonenum_office = None
        self.phonenum_mobile = None
        for k, v in attrs.items():
            setattr(self, k, v)

    def get_vcard_response(self) -> jnod.Node:
        return prepare_vcard_response(self)


def prepare_vcard_response(data: VcardData) -> jnod.Node:
    '''
    <field var='email'><value>jakub.janowski@nsn.com</value></field>
    <field var='ssoid'><value>jjanowsk</value></field>
    <field var='displayname'><value>Jakub Janowski</value></field>
    <field var='firstname'><value>Jakub</value></field>
    <field var='lastname'><value>Janowski</value></field>
    <field var='phonenum_home'><value/></field>
    <field var='phonenum_mobile'><value/></field>
    <field var='phonenum_office'><value>8028388</value></field>
    <field var='phonenum_csf'><value>8028388</value></field>
    <field var='pictureurl'><value>picture_url</value></field>
    <field var='username'><value>jakub.janowski@nsn.com</value></field>
    <field var='jobtitle'><value/></field>
    <field var='jid'><value>jakub.janowski@nsn.com</value></field>
    <field var='userid'><value>U5PFXLWF5BSOKDGWZE2YE7S5NC</value></field>
    '''
    iq = jnod.Node('iq').set_attr('id', data.uid).set_attr('to', data.to).\
        set_attr('type', 'result').set_attr('from', data._from)
    vc = iq.add_child('vCard').set_attr('xmlns', 'vcard-temp')
    if data.displayname:
        vc.add_child('FN').set_text(data.displayname)
    n = vc.add_child('N')
    if data.lastname:
        n.add_child('FAMILY').set_text(data.lastname)
    if data.firstname:
        n.add_child('GIVEN').set_text(data.firstname)
    if data.jobtitle:
        vc.add_child('TITLE').set_text(data.jobtitle)
    if data.email:
        e = vc.add_child('EMAIL')
        e.add_child('INTERNET')
        e.add_child('PREF')
        e.add_child('USERID').set_text(data.email)
    if data.ssoid:
        vc.add_child('NICKNAME').set_text(data.ssoid)
    if data.jid:
        vc.add_child('JABBERID').set_text(data.jid)
    if data.phonenum_office:
        t = vc.add_child('TEL')
        t.add_child('WORK')
        t.add_child('VOICE')
        t.add_child('NUMBER').set_text(data.phonenum_office)
    if data.phonenum_mobile:
        t = vc.add_child('TEL')
        t.add_child('CELL')
        t.add_child('VOICE')
        t.add_child('NUMBER').set_text(data.phonenum_mobile)
    if data.phonenum_home:
        t = vc.add_child('TEL')
        t.add_child('HOME')
        t.add_child('VOICE')
        t.add_child('NUMBER').set_text(data.phonenum_home)
    if data.picture:
        photo = vc.add_child('PHOTO')
        photo.add_child('TYPE').set_text(data.picture[0])  # content type as str
        photo.add_child('BINVAL').set_text(data.picture[1])  # picture in base64 encoding
    elif data.pictureurl:
        vc.add_child('PHOTO').add_child('EXTVAL').set_text(data.pictureurl)
    return iq


def cisco_vcard_request(email: str, uid: str, sockets: dextra.ExtraStuff):
    '''
    <iq to='consvr.isj1.webex.com' id='uid' type='set'>
        <query xmlns='jabber:iq:search'>
            <x xmlns='jabber:x:data' type='submit'>
                <field type='hidden' var='FORM_TYPE'>
                    <value>http://webex.com/connect/cs</value>
                </field>
                <field var='jidoremail'>
                    <value>jid</value>
                </field>
            </x>
        </query>
    </iq>
    '''
    add_field = decisco.extensions.utils.add_field
    to = sockets.users_directory
    r = jnod.Node('iq').set_attr('to', to, id=uid, type='set')
    q = r.add_child('query').set_attr('xmlns', 'jabber:iq:search')
    x = q.add_child('x').set_attr('xmlns', 'jabber:x:data', type='submit')
    add_field(x, 'FORM_TYPE', type='hidden').set_text('http://webex.com/connect/cs')
    add_field(x, 'jidoremail').set_text(email)
    return dtypes.SendForward(r)


def vcard_request(sockets: dextra.ExtraStuff, data: jnod.Node):
    ''' cisco server do not seems to support xmpp vcard protocol so translate
    it to jabber:iq:search requests
    <iq xmlns="jabber:client" type="get" to="user" id="aac5a">
       <vCard xmlns="vcard-temp"/>
    </iq>
    '''
    if data.Tag != 'iq' or not data.has_attribs(type='get', id=any):
        return
    vcard = data.node('vCard', attr={'xmlns': "vcard-temp"})
    if vcard is None:
        return
    uid = data.get('id')
    email = data.get('to', sockets.jabberid)
    logging.debug("GOT vcard request: %s" % vcard)
    return cisco_vcard_request(email, uid, sockets)


def get_display_name(attrs):
    dn = attrs.get('displayname', None)
    if dn is not None:
        return dn
    sur = attrs.get('lastname', None)
    nam = attrs.get('firstname', None)
    if nam and sur:
        dn = '%s, %s' % (sur, nam)
    elif sur:
        dn = sur
    else:
        dn = nam
    return dn


def parse_search_result(extra, uid, to, item: jnod.Node) -> VcardData:
    attrs = {}
    for fld in item.childs:
        var = fld.get('var', None)
        if fld.childs:
            val = fld.childs[0].text
        else:
            val = None
        attrs[var] = val
    r = VcardData(extra, uid, to, attrs)
    if r._from in extra.roster_list:
        dn = get_display_name(attrs)
        re = extra.roster_list[r._from]
        if re.name and dn and r._from and (re.name in r._from or dn not in re.name):
            re.name = dn
            re.update_name = True
            extra.log.info(
                "Detected need for name/nick change %s->%s", r._from, dn)
            # 'from', data.get('to'),
            iq = jnod.Node('iq').set_attr(
                type='set', id=extra.get_next_uid())
            q = iq.add_child('query').set_attr(xmlns='jabber:iq:roster')
            i = q.add_child('item').set_attr("name", dn, jid=r._from)
            if re.group is not None:
                i.add_child('group').set_text(re.group)
            extra.log.debug(iq)
            extra.emit('altered_to_server', iq.to_bytes())
            if re.subscription is not None:
                iq = jnod.Node('iq').set_attr(type='set', id='xcp-pushid')
                q = iq.add_child('query').set_attr(xmlns='jabber:iq:roster')
                i = q.add_child('item').set_attr('name', dn, jid=re.jid, subscription=re.subscription)
                if re.group is not None:
                    i.add_child('group').set_text(re.group)
                extra.emit('altered_to_server', iq.to_bytes())
    return r


def search_result(uid, to, extra: dextra.ExtraStuff, ritems: list):
    '''
    <iq type='result' from='characters.shakespeare.lit' to='romeo@montague.net/home' id='search2'>
        <query xmlns='jabber:iq:search'>
            <item jid='juliet@capulet.com'>
                <first>Juliet</first>
                <last>Capulet</last>
                <nick>JuliC</nick>
                <email>juliet@shakespeare.lit</email>
            </item>
            <item jid='tybalt@shakespeare.lit'>
                <first>Tybalt</first>
                <last>Capulet</last>
                <nick>ty</nick>
                <email>tybalt@shakespeare.lit</email>
            </item>
        </query>
    </iq>
    '''
    ret = jnod.Node('iq').set_attr('from', extra.users_directory, type='result', id=uid, to=to)
    q = ret.add_child('query').set_attr('xmlns', 'jabber:iq:search')
    for i in ritems:
        item = q.add_child('item').set_attr(jid=i.jid)
        if i.ssoid:
            item.add_child('nick').set_text(i.ssoid)
        if i.email:
            item.add_child('email').set_text(i.email)
        item.add_child('last').set_text(i.lastname)
        item.add_child('first').set_text(i.firstname)
    return dtypes.SendForward(ret)


def handle_search_result(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq componentid='...' from='node' id='uid' to='resource' type='result'>
        <query xmlns='jabber:iq:search'>
            <x type='result' xmlns='jabber:x:data'>
                <field type='hidden' var='FORM_TYPE'>
                    <value>http://webex.com/connect/gs</value>
                </field>
                <reported>
                    ...
                </reported>
                <item>
                    <field var='email'><value>...</value></field>
                    <field var='ssoid'><value>...</value></field>
                    <field var='displayname'><value>...</value></field>
                    <field var='firstname'><value>...</value></field>
                    <field var='lastname'><value>...</value></field>
                    <field var='phonenum_home'><value/></field>
                    <field var='phonenum_mobile'><value/></field>
                    <field var='phonenum_office'><value>...</value></field>
                    <field var='phonenum_csf'><value>...</value></field>
                    <field var='pictureurl'><value>...</value></field>
                    <field var='username'><value>...</value></field>
                    <field var='jobtitle'><value/></field>
                    <field var='jid'><value>...</value></field>
                    <field var='userid'><value>...</value></field>
                </item>
            </x>
        </query>
    </iq>
    '''
    if data.Tag != 'iq' or not data.has_attribs(type='result', from_=extra.users_directory, to=any, id=any):
        return
    query = data.node('query', attr={'xmlns': 'jabber:iq:search'})
    if query is None:
        return
    x = query.node('x', attr={'xmlns': 'jabber:x:data', 'type': 'result'})
    if x is None:
        return
    field = x.node('field', attr={'type': 'hidden', 'var': 'FORM_TYPE'})
    if field is None:
        return
    items = x.nodes('item')
    if not items:
        return
    to = data.get('to')
    uid = data.get('id')
    ritems = [parse_search_result(extra, uid, to, i) for i in items]
    if uid in extra.not_vcard:
        extra.not_vcard.remove(uid)
        return search_result(uid, to, extra, ritems)
    r = ritems[0]
    if r.pictureurl:
        ''' try to download picture and deliver it as binval '''
        ret = dtypes.DoNothing()
        downloader = decisco.extensions.imagedesc.ImageDescription(r, extra)
        downloader.run()
        extra.log.debug('Retrieving image: %s', r.pictureurl)
        # extra.show_who_call_send = True
    else:
        ret = dtypes.SendForward(r.get_vcard_response())
    return ret


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_server_handler(handle_search_result)
    extra.register_client_handler(vcard_request)
