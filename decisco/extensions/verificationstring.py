import base64
import hashlib


class Identity(object):
    def __init__(self, category, type, lang, name):
        self.type = type
        self.category = category
        self.lang = lang
        self.name = name

    def value(self):
        return '%s/%s/%s/%s' % (self.category, self.type, self.lang, self.name)


def mk_identity(name=''):
    return [
        # <identity category='client' name='Telepathy Gabble 0.18.4' type='pc' />
        Identity('client', 'pc', '', 'Decisco (%s)' % name)
    ]


def mk_verification_string(ids, servicediscovery):
    S = ''
    '''
    Sort the service discovery identities [15] by category and then by type and then by xml:lang (if it exists),
    formatted as CATEGORY '/' [TYPE] '/' [LANG] '/' [NAME]. [16] Note that each slash is included even if the LANG or
    NAME is not included (in accordance with XEP-0030, the category and type MUST be included).

    For each identity, append the 'category/type/lang/name' to S, followed by the '<' character.
    '''
    ids = sorted(ids)
    for id_ in ids:
        S += id_.value()
    S += '<'
    '''
    Sort the supported service discovery features. [17]
    For each feature, append the feature to S, followed by the '<' character.
    '''
    servicediscovery = sorted(servicediscovery)
    S += '<'.join(servicediscovery) + '<'

    '''
    If the service discovery information response includes XEP-0128 data forms, sort the forms by the FORM_TYPE
    (i.e., by the XML character data of the <value/> element).
    For each extended service discovery information form:
        Append the XML character data of the FORM_TYPE field's <value/> element, followed by the '<' character.
        Sort the fields by the value of the "var" attribute.
        For each field other than FORM_TYPE:
            Append the value of the "var" attribute, followed by the '<' character.
        Sort values by the XML character data of the <value/> element.
        For each <value/> element, append the XML character data, followed by the '<' character.
        Ensure that S is encoded according to the UTF-8 encoding (RFC 3629 [18]).
    '''
    '''
    Compute the verification string by hashing S using the algorithm specified in the 'hash' attribute
    (e.g., SHA-1 as defined in RFC 3174 [19]). The hashed data MUST be generated with binary output and encoded using
    Base64 as specified in Section 4 of RFC 4648 [20] (note: the Base64 output MUST NOT include whitespace and MUST
    set padding bits to zero). [21]
    '''
    if bytes is not str:
        S = S.encode('utf8', errors='ignore')
    ret = base64.b64encode(hashlib.sha1(S).digest())
    if bytes is not str:
        ret = ret.decode('utf8', errors='ignore')
    return ret


DEFAULT_CAPABILITIES = [
    'urn:xmpp:bob',
    'urn:xmpp:jingle:1',
    'urn:xmpp:jingle:apps:rtp:1',
    'urn:xmpp:jingle:apps:rtp:audio',
    'urn:xmpp:jingle:apps:rtp:video',
    'urn:xmpp:jingle:transports:ice-udp:1',
    'urn:xmpp:jingle:transports:raw-udp:1',
    'http://www.google.com/xmpp/protocol/session',
    'http://www.google.com/xmpp/protocol/voice/v1',
    'http://www.google.com/xmpp/protocol/video/v1',
    'http://www.google.com/xmpp/protocol/camera/v1',
    'jabber:iq:last',
    'jabber:iq:version',
    'http://jabber.org/protocol/si',
    'http://jabber.org/protocol/muc',
    'http://jabber.org/protocol/ibb',
    'http://jabber.org/protocol/caps',
    'http://jabber.org/protocol/xhtml-im',
    'http://jabber.org/protocol/disco#info',
    'http://jabber.org/protocol/chatstates',
    'http://jabber.org/protocol/disco#items',
    'http://jabber.org/protocol/bytestreams',
    'http://jabber.org/protocol/nick',
    'http://jabber.org/protocol/nick+notify',
    'http://jabber.org/protocol/si/profile/file-transfer',
    ]
DEFAULT_CLIENT = 'Unknown'
