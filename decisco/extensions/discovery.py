import decisco.jnod as jnod
import decisco.extensions.const as const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes


disco_protocols = (const.disco_info, const.disco_items, )


def disco_localhost_to_org(sockets: dextra.ExtraStuff, data: jnod.Node):
    '''
    NOTE: Affected client: Kopete

    <iq xmlns="jabber:client" type="get" to="localhost" id="aadfa">
        <query xmlns="http://jabber.org/protocol/disco#info"/>
    </iq>
    <iq xmlns="jabber:client" type="get" to="localhost" id="aae0a">
        <query xmlns="http://jabber.org/protocol/disco#items"/>
    </iq>
    '''
    if data.Tag != 'iq' or not data.has_attribs(to='localhost', xmlns='jabber:client'):
        return
    query = data.node('query', attr={'xmlns': disco_protocols})
    if query is None:
        return
    data.set_attr('to', sockets.options.organization)
    sockets.localhost_disco_requests = True
    return dtypes.SendForward(data)


def disco_org_to_localhost(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    NOTE: Affected client: Kopete

    <iq xmlns="jabber:client" type="result" from="example.com" id="aadfa">
        <query xmlns="http://jabber.org/protocol/disco#info">...</query>
    </iq>
    <iq xmlns="jabber:client" type="result" from="example.com" id="aae0a">
        <query xmlns="http://jabber.org/protocol/disco#items">...</query>
    </iq>
    '''
    org = extra.options.organization
    if data.Tag != 'iq' or not data.has_attribs(from_=org, type='result', xmlns="jabber:client"):
        return
    query = data.node('query', attr={'xmlns': disco_protocols})
    if query is None:
        return
    if extra.localhost_disco_requests:
        data.set_attr('from', 'localhost')
    return dtypes.SendForward(data)


def disco_info_result_mark_user_directory(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='consvr.isj7.webex.com' id='purplef8c62450' to='john.smith@example.com/xmpp' type='result'>
        <query xmlns='http://jabber.org/protocol/disco#info'>
            <identity category='component' name='Connection Server Manager' type='generic'/>
            <feature var='http://jabber.org/protocol/disco#info'/>
            <feature var='http://jabber.org/protocol/disco#items'/>
            <feature var='http://webex.com/connect/cs'/>
        </query>
    </iq>
    '''
    if data.Tag != 'iq':
        return
    if data.get('type', '') != 'result':
        return
    # if data.get('from', None) != extra.users_directory:
    #     return
    q = data.node('query', {'xmlns': const.disco_info})
    if q is None:
        return
    ident = q.node('identity', {'category': 'component', 'name': 'Connection Server Manager', 'type': 'generic'})
    if ident is None or data.get('from', None) is None:
        return
    extra.users_directory = data.get('from')
    search = q.node('feature', {'var': 'jabber:iq:search'})
    if search is None:
        q.add_child('feature').set_attr('var', 'jabber:iq:search')
    identity = q.node('identity', {'category': 'directory', 'type': 'user'})
    if identity is None:
        q.add_child('identity').set_attr(category='directory', type='user')
    return dtypes.SendForward(data.to_bytes())


def add_vcard_temp_feature(sockets: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='example.com' id='id03' to='adam@example.com/adam_resource' type='result'>
        <query xmlns='http://jabber.org/protocol/disco#info'>...
    '''
    if data.Tag != 'iq' or not data.has_attribs(type='result', from_=sockets.options.organization):
        return
    query = data.node('query', attr={'xmlns': const.disco_info})
    if query is None:
        return
    if len(query.attribs) != 1:
        return
    feature = query.node('feature', {'var': 'vcard-temp'})
    if feature is not None:
        return
    query.add_child('feature', var='vcard-temp')
    return dtypes.SendForward(data)


def get_bind_name(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq id='...' type='result'>
        <bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'>
            <jid>jid/resource_name</jid>
        </bind>
    </iq> '''
    if data.Tag != 'iq' or not data.has_attr('type', value='result'):
        return
    bind = data.node('bind', {'xmlns': "urn:ietf:params:xml:ns:xmpp-bind"})
    if bind is None:
        return
    jid = bind.node('jid')
    extra.bindedjid = jid.text


def expand_client_capabilities(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='self_binded_jid' id='...' to='...' type='result'>
        <query ...>
            <identity category='client' name='Cisco Jabber 11.6.0' type='im' />
            ...
            <feature var='http://webex.com/connect/customcaps/ds' />
            <feature var='http://webex.com/connect/customcaps/jinglecmd' />
            <feature var='http://webex.com/connect/customcaps/picture-share' />
            <feature var='http://webex.com/connect/customcaps/picture-share-mix' />
            ...
        </query>
    </iq>
    '''
    if data.Tag != 'iq' or not data.has_attribs(from_=extra.bindedjid, type='result'):
        return
    q = data.node('query', {'xmlns': const.disco_info, 'node': any})
    if q is None:
        return
    # if const.cisco_jabber not in q.get('node'):
    #     return
    caps = [const.cisco_desktopshare, const.webex_ds, const.webex_picture_share, const.webex_picture_share_mix]
    for cap in caps:
        n = q.node('feature', {'var': cap})
        if n is None:
            q.add_child('feature', var=cap)
    return dtypes.SendForward(data)


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_server_handler(
        disco_org_to_localhost, disco_info_result_mark_user_directory, add_vcard_temp_feature, get_bind_name)
    extra.register_client_handler(disco_localhost_to_org, expand_client_capabilities)
