import decisco.jnod as jnod
import decisco.extensions.const as const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes


def file_transfer_replace_localhost(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    NOTE: Affected client: Kopete
    <iq xmlns="jabber:client" type="set" to="to_resource" id="...">
        <query xmlns=".../bytestreams" mode="tcp" sid="...">
            <streamhost port="8010" host="127.0.0.1" jid="from_resource"/>
            <fast xmlns="http://affinix.com/jabber/stream"/>
        </query>
    </iq>
    '''
    if data.Tag != 'iq':
        return
    if not data.has_attr('to'):
        return
    if not data.has_attr('type', value='set'):
        return
    query = data.node('query')
    if query is None:
        return
    if not query.has_attr('xmlns', value=const.bytestreams):
        return
    streamhost = query.node('streamhost')
    if streamhost is None:
        return
    if streamhost.has_attr('host', value='127.0.0.1') or streamhost.has_attr('host', value='localhost'):
        streamhost.set_attr('host', extra.ip_address)
        return dtypes.SendForward(data)


def remove_ibb_method_from_neg_offer(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    NOTE: Affected clients: telepathy xmpp

    Official cisco client anounces ibb file transfer method by when other end selects that method nothing happens and
    actuall file transfer is stalled, it is also possible that cisco server drops ibb file transfers.

    <iq from='...' id='...' to='...' type='set'>
        <si id='' profile='http://jabber.org/protocol/si/profile/file-transfer' xmlns='http://jabber.org/protocol/si'>
            <file name='...' size='...' xmlns='http://jabber.org/protocol/si/profile/file-transfer'/>
            <feature xmlns='http://jabber.org/protocol/feature-neg'>
                <x type='form' xmlns='jabber:x:data'>
                    <field type='list-single' var='stream-method'>
                        <option label='ibb'><value>http://jabber.org/protocol/ibb</value></option>
                        <option label='oob'><value>jabber:iq:oob</value></option>
                        <option label='s5b'><value>http://jabber.org/protocol/bytestreams</value></option>
                        <value/>
                    </field>
                </x>
            </feature>
        </si>
    </iq>
    '''
    if data.Tag != 'iq':
        return
    if not data.has_attr('type', value='set'):
        return
    si = data.node('si', {'profile': const.protocol_si_ft, 'xmlns': const.protocol_si})
    if si is None:
        return
    feature = si.node('feature', {'xmlns': const.feature_neg})
    if feature is None:
        return
    x = feature.node('x', {'type': 'form', 'xmlns': 'jabber:x:data'})
    if x is None:
        return
    field = x.node('field', {'type': 'list-single', 'var': 'stream-method'})
    if field is None:
        return
    for index, c in enumerate(field.childs):
        if c.Tag != 'option':
            continue
        v = c.node('value')
        if c.has_attr('label', value='ibb') or (v is not None and v.text == const.protocol_ibb):
            field.remove_node(index)
    return dtypes.SendForward(data)


def invalid_jingle_session_response(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    NOTE: Affected clients: telepathy xmpp
    Telepathy xmpp errors non standard jingle file transfer attempt and the other side doesn't like that so replace it
    with jingle 'session-terminate' event

    <iq type="error" from="identity1" to="identity2" id="id1">
        <jingle action="session-initiate" initiator="identity2" sid="SID5" xmlns="urn:xmpp:jingle:1">
            <content creator="initiator">
                <description num="141" xmlns="http://webex.com/connect/commands"/>
            </content>
            <x xmlns="http://protocols.cisco.com/csg/log-notification">...</x>
        </jingle>
        <error code="400" type="modify">
            <bad-request xmlns="urn:ietf:params:xml:ns:xmpp-stanzas"/>
            <text xmlns="urn:ietf:params:xml:ns:xmpp-stanzas">'name' attribute unset</text>
        </error>
    </iq>

    replace above with

    <iq type='result' id='id1' from='identity1' to='identity2'/>
    <iq type='set' id='id2' from='identity1' to='identity2'>
        <jingle xmlns='urn:xmpp:jingle:1' action='session-terminate' initiator='identity2' responder='identity1'
                sid='SID12'>
            <reason><unsupported-applications/></reason>
        </jingle>
    </iq>
    '''
    if data.Tag != 'iq':
        return
    if not data.has_attr('type', value='error'):
        return
    jingle = data.node("jingle", {'action': "session-initiate", 'xmlns': "urn:xmpp:jingle:1"})
    if jingle is None:
        return
    error = data.node("error", {'code': '400', 'type': 'modify'})
    if error is None:
        return
    identity2 = data.get('to')
    id1 = data.get('id')
    sid = jingle.get('sid')
    identity1 = extra.get_self_identity()
    resp = jnod.Node("iq").set_attr('from', identity1, type='result', to=identity2, id=id1)
    terminate = jnod.Node("iq").set_attr('from', identity1, to=identity2, type='set', id=extra.get_next_uid())
    jingle2 = terminate.add_child('jingle').set_attr(
        xmlns='urn:xmpp:jingle:1', action='session-terminate', initiator=identity2, responder=identity1, sid=sid)
    jingle2.add_child('reason').add_child('unsupported-applications')
    return dtypes.SendForward(resp.to_bytes() + terminate.to_bytes())


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_server_handler(remove_ibb_method_from_neg_offer)
    extra.register_client_handler(file_transfer_replace_localhost, invalid_jingle_session_response)
