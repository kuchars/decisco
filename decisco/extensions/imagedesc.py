import re
import ssl
import base64

import decisco.utils
import decisco.dsockets
import decisco.extensions
import decisco.extensions.vcard
import decisco.extensions.extra


def exception_safe(func):
    def wrapper(*args, **kwargs):
        ret = None
        self = args[0]
        try:
            ret = func(*args, **kwargs)
        except Exception:
            self.log.exception("Method %s failed" % func.__name__)
        return ret
    return wrapper


class ImageDescription(object):
    PORT_IN_HOST = re.compile(r'.*:(\d+)$')

    def __init__(self, user_data: 'decisco.extensions.vcard.VcardData', parent: decisco.extensions.extra.ExtraStuff):
        object.__init__(self)
        self.parent = parent
        self.running = False
        assert(isinstance(user_data, decisco.extensions.vcard.VcardData))
        self.user_data = user_data
        self.data = bytes()
        self.code = None
        self.content_type = None
        self.content_length = 0
        self.got_empty_line = False
        self.collected_data_len = 0
        self.log = parent.options.get_logger('ImageDescription')
        self.out = None
        self.extra = None

    def __close(self):
        self.parent.selector.unregister(self.extra)
        self.extra.close()
        self.out = None
        self.extra = None

    def url(self):
        return self.user_data.pictureurl

    def __start_download(self, url):
        surl = url.split('/')
        if len(surl) < 4:
            self.log.warning("To little elements in url: %s" % surl)
            return False
        port = 80
        self.log.debug("url: %s" % surl)
        if surl[0] == 'https:':
            port = 443
            self.ssl = True
            self.log.debug("use ssl")
        else:
            self.ssl = False
        addr = surl[2]
        m = self.PORT_IN_HOST.match(addr)
        if m is not None:
            host_port = m.group(1)
            if str(port) != host_port:
                port = int(host_port)
            addr = addr[:-1 - len(host_port)]
        resource = '/'.join(surl[3:])
        resource = decisco.utils.quote(resource)
        self.extra.connect((addr, port))
        data = bytes('GET /%s HTTP/1.1\r\nHost: %s\r\nAccept: */*\r\n\r\n' % (resource, addr), 'utf-8')
        self.log.debug(data)
        self.out.schedule(data)
        return True

    def run(self):
        shared_obj = self.parent.parent
        tracer = shared_obj.tracing
        extra = decisco.dsockets.ProxySslSocket(self.parent.options, name='decisco<->imgsvr', tracer=tracer)
        self.extra = extra
        self.out = decisco.dsockets.OutBuffer(extra, self.parent.selector, shared_obj.options.jam_thres)
        if not self.__start_download(self.url()):
            self.out = None
            return self.__send_response()
        extra.set_tcp_no_delay()
        extra.set_error_receiving()
        extra.set_tcp_user_timeout()
        self.parent.selector.register(extra, extra.name, self.__ready_read, self.__ready_write).\
            for_inout().set_close_action(self.__done)

    def __ready_read(self):
        count = 1024
        try:
            data = self.extra.recv_bytes(count)
        except ssl.SSLWantReadError:
            return
        except Exception:
            self.log.exception("Failed to read http stream, %s" % self.url())
            self.__done()
            return
        ret = data
        while len(data) == count:
            try:
                data = self.extra.recv_bytes(count)
            except ssl.SSLWantReadError:
                break
            except Exception:
                self.log.exception("Failed to read http stream, %s" % self.url())
                self.__done()
                return
            ret += data
        try:
            self.__handle_data(ret)
        except Exception:
            self.log.exception("Processing data - failed, %s" % self.url())

    @exception_safe
    def __ready_write(self):
        self.out.send()

    def __handle_return_code(self, line):
        ar = line.split(' ')
        try:
            self.code = int(ar[1])
        except Exception:
            pass
        if 'HTTP/1' not in ar[0] or self.code != 200:
            self.log.debug("expected 200(OK) Http response got: %s" % line)
            ''' HACK for now remove picture url is we are unable to download it
            as kopete try to do the same and blocks '''
            self.user_data.pictureurl = None
            self.__done()
            return True
        return False

    def __parse_http_header(self, data):
        start = 0
        while True and not self.got_empty_line:
            i = data.find(b'\r\n', start)
            if i != -1:
                line = data[start:i].decode('utf-8')
                if self.code is None:
                    if self.__handle_return_code(line):
                        break
                    continue
                ct = 'Content-Type: '
                cl = 'Content-Length: '
                if ct in line:
                    off = line.find(ct) + len(ct)
                    self.content_type = line[off:]
                    self.log.debug("content_type: %s", self.content_type)
                elif cl in line:
                    off = line.find(cl) + len(cl)
                    self.content_length = int(line[off:])
                    self.log.debug("Length: %s", self.content_length)
                elif line == '':
                    start += 2
                    self.log.debug("Got Empty line: %s, %s", start, len(data))
                    self.got_empty_line = True
                    break
                start += (len(line) + 2)
            else:
                self.log.error('couldn\'t find HTTP header, %s' % self.url())
                break
        return start

    def __append_data(self, data):
        self.collected_data_len += len(data)
        self.data += data
        tup = (self.collected_data_len, self.content_length)
        self.log.debug("got %s out of %s", *tup)
        if self.collected_data_len >= self.content_length:
            self.__done()

    def __handle_data(self, data):
        start = self.__parse_http_header(data)
        if start == 0:
            if self.got_empty_line:
                self.__append_data(data)
            elif self.code is not None:
                self.__done()
            else:
                dptr = data[:max(len(data), 20)]
                self.log.error("Failed to parse: col_len: %s, data: %s", self.collected_data_len, dptr)
                self.__done()
            return
        dat = data[start:]
        if self.content_length and len(dat) > self.content_length:
            dat = dat[:self.content_length]
        self.collected_data_len += len(dat)
        self.data += dat
        if self.content_length:
            if self.content_length == self.collected_data_len:
                self.__done()
            return
        self.log.error(
            "No data finish by sending vcard response")
        self.__done()

    def __send_response(self):
        content = self.user_data.get_vcard_response()
        self.parent.emit('altered_to_client', content.to_bytes())

    def __done(self, *args):
        if self.extra is None:
            return
        success = self.content_type is not None and (self.collected_data_len >= self.content_length)
        if success:
            self.log.debug('got photo in binary form %s', self.user_data.pictureurl)
            pdtr = base64.b64encode(self.data).decode('utf-8')
            self.user_data.picture = (self.content_type, pdtr)
        self.__send_response()
        self.__close()
