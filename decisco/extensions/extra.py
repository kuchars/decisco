import sys
import decisco.jnod as jnod
import decisco.utils
import decisco.colors as dcolors
import decisco.signal as dsignal
import decisco.dsockets as dsockets
import decisco.extensions.types
import decisco.extensions.const as const


def get_data(pdata, log, name):
    parser = jnod.Parser()
    data = parser.parse(pdata)
    if data is None:
        pdata = pdata.strip()
        if pdata:
            s = 'Failed to parse(experimental.%s)' % name
            log.warning("%s%s:\n%s%s", dcolors.cRed, s, pdata, dcolors.cDefault)
        return False, False
    return [data] + parser.extras, parser.problems


class CustomDataContainer(object):
    def __init__(self):
        object.__init__(self)


class ExtraStuff(dsignal.Signals):
    CLIENT = 0
    SERVER = 1

    def __init__(self, parent: 'SharedResource', selector: dsockets.SocketSelector):
        dsignal.Signals.__init__(self)
        self.parent = parent
        self.selector = selector
        self.options = parent.options
        self.ip_address = None
        self.uid_counter = 100
        self.localhost_disco_requests = False
        self.jabberid = None
        self.bindedjid = None
        self.log = self.options.get_logger('ExtraStuff')
        self.not_vcard = set()
        self.roster_list = {}
        # TODO get consvr.isj7.webex.com from other place
        self.users_directory = 'consvr.isj7.webex.com'
        self.client_handlers = []
        self.server_handlers = []
        self.payload = CustomDataContainer()
        self.ext_modules = []
        self.ext_modules_time_stamp = {}

    def get_container(self, container_type):
        ret = getattr(self.payload, container_type.__name__)
        assert isinstance(ret, container_type)
        return ret

    def register_container(self, container_type):
        old_container = getattr(self.payload, container_type.__name__, None)
        if old_container is not None and hasattr(old_container, '_save'):
            old_container._save()
        new_instance = container_type(self.options)
        if hasattr(new_instance, '_load'):
            new_instance._load()
        elif hasattr(new_instance, '_load_from_old') and old_container is not None:
            new_instance._load_from_old(old_container)
        setattr(self.payload, container_type.__name__, new_instance)

    def get_self_identity(self):
        return self.bindedjid or self.jabberid

    def register_module(self, module_name: str):
        module = sys.modules[module_name]
        self.ext_modules.append(module)
        timestamp = decisco.utils.get_python_file_and_mtime(module)
        self.ext_modules_time_stamp[module_name] = timestamp

    def register_client_handler(self, *handler):
        self.client_handlers.extend(list(handler))

    def register_server_handler(self, *handler):
        self.server_handlers.extend(list(handler))

    def got_ip_address_slot(self, ip: str):
        self.log.info('Got external ip address: %s', ip)
        self.ip_address = ip

    def get_next_uid(self) -> str:
        x = self.uid_counter
        self.uid_counter += 1
        return 'uid:decisco:%s' % hex(x)

    def _generic_handle_data(self, data: jnod.Node, handlers: list):
        for func in handlers:
            ret = func(self, data)
            if ret is not None:
                return ret

    def _process_nodes(self, nodes: list, handlers: list, problems: bool, pdata: bytes) -> list:
        proc_ret = []
        alterations = False
        for node in nodes:
            ret = self._generic_handle_data(node, handlers)
            if ret is None:
                proc_ret.append(decisco.extensions.types.PassThrough(node))
            elif isinstance(ret, decisco.extensions.types.DoNothing):
                proc_ret.append(ret)
                alterations = True
            else:
                alterations = True
                self.log.debug("Processed '%s' -> '%s'", node, ret)
                proc_ret.append(ret)
        if not alterations:
            proc_ret = False
        if problems and proc_ret:
            self.log.warning("%sparsed with problems:\n%s%s", dcolors.cRed, pdata, dcolors.cDefault)
        # if getattr(self, 'show_who_call_send', False):
        #     print("show_who_call_send - %s" % proc_ret)
        return proc_ret

    def _client_experimental(self, pdata: bytes):
        nodes, problems = get_data(pdata, self.log, 'client')
        if nodes is False:
            return False
        return self._process_nodes(nodes, self.client_handlers, problems, pdata)

    def _cisco_experimental(self, pdata: bytes):
        nodes, problems = get_data(pdata, self.log, 'server')
        if nodes is False:
            return False
        return self._process_nodes(nodes, self.server_handlers, problems, pdata)

    def _process_extra_data(self, data: list, forward: dsockets.OutBuffer, back: dsockets.OutBuffer):
        if not data:
            return False
        for e in data:
            if isinstance(e, decisco.extensions.types.SendForward):
                # print("forward.schedule(%s)" % e.data)
                forward.schedule(e.data)
            elif isinstance(e, decisco.extensions.types.SendBack):
                # print("back.schedule(%s)" % e.data)
                back.schedule(e.data)
            elif isinstance(e, decisco.extensions.types.PassThrough):
                # print("forward.schedule(%s)" % e.data)
                forward.schedule(e.data)
            elif isinstance(e, decisco.extensions.types.DoNothing):
                # print("do_nothing()")
                pass
            else:
                self.log.error("Unknown type: %s", type(e))
        return True

    def _handle_experimental(self, pdata: bytes, method, name: str) -> list:
        if not self.options.experimental:
            return []
        try:
            return method(pdata)
        except Exception:
            self.log.exception("%s experimental", name)
            self.log.debug(pdata)
        return []

    def process_experimental(self, data: bytes, direction: int, forward: dsockets.OutBuffer, back: dsockets.OutBuffer):
        if direction == self.CLIENT:
            m, name = self._client_experimental, 'client'
        elif direction == self.SERVER:
            m, name = self._cisco_experimental, 'server'
        else:
            raise RuntimeError("Unknown direction")
        edata = self._handle_experimental(data, m, name)
        if self._process_extra_data(edata, forward, back):
            return True
        return False

    def try_reload_extensions(self):
        for module in self.ext_modules:
            mod_name = module.__name__
            cur_timestamp = self.ext_modules_time_stamp[mod_name]
            new_timestamp = decisco.utils.get_mtime(module)
            if cur_timestamp == new_timestamp:
                continue
            try:
                self.log.info('reload %s', mod_name)
                importlib.reload(module)
                self.ext_modules_time_stamp[mod_name] = new_timestamp
                # TODO register new containers
            except Exception:
                self.log.exception("Failed to reload %s", mod_name)
