import re
import logging
import unittest
import unittest.mock
from decisco.jnod import Node as N
import decisco.extensions.const as const
import decisco.extensions.extra
import decisco.extensions.types as types
import decisco.extensions.screenshot as screenshot
import decisco.extensions.loader as dloader


SCREENSHOT_MESSAGE = b'''
<message id='id00' from='adam@example.com' to='ewa@example.com' type='chat' xml:lang='en'>
    <body></body>
    <x type='330' xmlns='http://webex.com/connect/imcmd'/>
    <html xmlns='http://jabber.org/protocol/xhtml-im'>
        <body xmlns='http://www.w3.org/1999/xhtml'>
            <span style=''>
                <div>
                    <div style=''>
                        <img alt='Screen capture' id='imgId00' name='connect_screen_capture' src='img.png'/>
                    </div>
                </div>
            </span>
        </body>
    </html>
</message>
'''
SCREENSHOT_FILETRANSFER_PUSH = b'''
<iq id='id01' from='adam@example.com' to='ewa@example.com' type='set' xml:lang='en'>
    <si id='sid00' profile='http://jabber.org/protocol/si/profile/file-transfer' xmlns='http://jabber.org/protocol/si'>
    <file name='imgId00' size='100' transfer-type='picture-share' xmlns='http://jabber.org/protocol/si/profile/file-transfer'/>
    <feature xmlns='http://jabber.org/protocol/feature-neg'>
        <x type='form' xmlns='jabber:x:data'>
            <field type='list-single' var='stream-method'>
                <option label='ibb'><value>http://jabber.org/protocol/ibb</value></option>
                <option label='oob'><value>jabber:iq:oob</value></option>
                <option label='s5b'><value>http://jabber.org/protocol/bytestreams</value></option>
                <value/>
            </field>
        </x>
    </feature>
    </si>
</iq>
'''
SCREENSHOT_FILETRANSFER_PUSH_RESP = b'''
<iq to="adam@example.com" type="result" id="id01">
    <si xmlns="http://jabber.org/protocol/si">
        <feature xmlns="http://jabber.org/protocol/feature-neg">
            <x xmlns="jabber:x:data" type="submit">
                <field var="stream-method"><value>http://jabber.org/protocol/bytestreams</value></field>
            </x>
        </feature>
    </si>
</iq>
'''
STREAMHOST_SELECTED = b'''
<iq id='id02' from='adam@example.com' to='ewa@example.com' type='set' xml:lang='en'>
    <query mode='tcp' sid='sid00' xmlns='http://jabber.org/protocol/bytestreams'>
        <streamhost host='isj7jft601-s.webexconnect.com' jid='proxy.isj7.webex.com' port='443'/>
    </query>
</iq>
'''
STREAMHOST_USED = b'''
<iq id='id02' from='ewa@example.com' to='adam@example.com' type='result'>
    <query xmlns='http://jabber.org/protocol/bytestreams' sid='sid00'>
        <streamhost-used jid="proxy.isj7.webex.com"/>
    </query>
</iq>
'''

'''
  <message to="john.smith@example.com/resource_name1" id="uid:56619a8f:00002318:00000042" type="chat">
    <body> </body>
    <x xmlns="http://webex.com/connect/imcmd" type="330"/>
    <html xmlns="http://jabber.org/protocol/xhtml-im">
      <body xmlns="http://www.w3.org/1999/xhtml">
        <span style="font-family:Segoe UI;color:#1a1a1a;font-size:10pt;font-weight:normal;font-style:normal;text-decoration:none;">
          <div>
            <div style="overflow-x:auto; overflow-y:hidden;padding-bottom:1.3em">
              <img alt="Screen capture" name="connect_screen_capture" id="20151204_150015198.png" src="...\20151204_150015198.png"/>
            </div>
          </div>
        </span>
      </body>
    </html>
  </message>

  <iq to="john.smith@example.com/resource_name1" id="uid:56619a8f:00000b7d:00000045" type="set">
    <jingle xmlns="urn:xmpp:jingle:1" action="session-initiate" initiator="adam.smith@example.com/resource_name" sid="SID1">
      <content creator="initiator">
        <description xmlns="http://webex.com/connect/commands" num="141"/>
      </content>
      <x xmlns="http://protocols.cisco.com/csg/log-notification">Start sending screen capture '20151204_150015198.png(33363 bytes)'.</x>
    </jingle>
  </iq>
  <iq from="john.smith@example.com/resource_name1" id="uid:56619a8f:00000b7d:00000045" to="adam.smith@example.com/resource_name" type="result" xml:lang="en"/>

  <iq to="john.smith@example.com/resource_name1" id="uid:56619a8f:0000127d:00000046" type="set">
    <si xmlns="http://jabber.org/protocol/si" id="uid:56619a8f:00004217:00000047" profile="http://jabber.org/protocol/si/profile/file-transfer">
      <file xmlns="http://jabber.org/protocol/si/profile/file-transfer" name="20151204_150015198.png" size="33363" transfer-type="picture-share"/>
      <feature xmlns="http://jabber.org/protocol/feature-neg">
        <x xmlns="jabber:x:data" type="form">
          <field type="list-single" var="stream-method">
            <option label="ibb">
              <value>http://jabber.org/protocol/ibb</value>
            </option>
            <option label="oob">
              <value>jabber:iq:oob</value>
            </option>
            <option label="s5b">
              <value>http://jabber.org/protocol/bytestreams</value>
            </option>
            <value/>
          </field>
        </x>
      </feature>
    </si>
  </iq>
  <iq from="john.smith@example.com/resource_name1" id="uid:56619a8f:0000127d:00000046" to="adam.smith@example.com/resource_name" type="result" xml:lang="en">
    <si xmlns="http://jabber.org/protocol/si">
      <feature xmlns="http://jabber.org/protocol/feature-neg">
        <x xmlns="jabber:x:data" type="submit">
          <field type="text-single" var="stream-method">
            <value>http://jabber.org/protocol/bytestreams</value>
          </field>
        </x>
      </feature>
    </si>
  </iq>


  <iq to="john.smith@example.com/resource_name1" id="uid:56619a8f:00006a2b:00000048" type="set">
    <query xmlns="http://jabber.org/protocol/bytestreams" sid="uid:56619a8f:00004217:00000047" mode="tcp">
      <streamhost jid="proxy.isj7.webex.com" host="isj7jft601-s.webexconnect.com" port="443"/>
    </query>
  </iq>
  <iq from="john.smith@example.com/resource_name1" id="uid:56619a8f:00006a2b:00000048" to="adam.smith@example.com/resource_name" type="result" xml:lang="en">
    <query xmlns="http://jabber.org/protocol/bytestreams" sid="uid:56619a8f:00004217:00000047">
      <streamhost-used jid="proxy.isj7.webex.com"/>
    </query>
  </iq>

  <iq to="proxy.isj7.webex.com" id="uid:56619a8f:00000029:00000049" type="set">
    <query xmlns="http://jabber.org/protocol/bytestreams" sid="uid:56619a8f:00004217:00000047">
      <activate>john.smith@example.com/resource_name1</activate>
    </query>
  </iq>
  <iq from="proxy.isj7.webex.com" id="uid:56619a8f:00000029:00000049" to="adam.smith@example.com/resource_name" type="result" xml:lang="en">
    <query xmlns="http://jabber.org/protocol/bytestreams" sid="uid:56619a8f:00004217:00000047">
      <activate>john.smith@example.com/resource_name1</activate>
    </query>
  </iq>

  <iq to="john.smith@example.com/resource_name1" id="uid:56619a8f:00007f34:0000004a" type="set">
    <jingle xmlns="urn:xmpp:jingle:1" action="session-terminate" initiator="john.smith@example.com/resource_name1" responder="adam.smith@example.com/resource_name" sid="SID1">
      <content creator="initiator">
        <description xmlns="http://webex.com/connect/commands" num="141"/>
      </content>
      <x xmlns="http://protocols.cisco.com/csg/log-notification">Successfully sent screen capture '20151204_150015198.png(33363 bytes)'.</x>
    </jingle>
  </iq>
  <iq from="john.smith@example.com/resource_name1" id="uid:56619a8f:00007f34:0000004a" to="adam.smith@example.com/resource_name" type="result" xml:lang="en"/>


  <iq from="john.smith@example.com/resource_name1" id="uid:565ff8ee:000033ec:00000712" to="adam.smith@example.com/resource_name" type="set" xml:lang="en">
    <jingle xmlns="urn:xmpp:jingle:1" action="session-initiate" initiator="john.smith@example.com/resource_name1" sid="SID4">
      <content creator="initiator">
        <description xmlns="http://webex.com/connect/commands" num="141"/>
      </content>
      <x xmlns="http://protocols.cisco.com/csg/log-notification">Start sending file '20151204_150015198.png(33363 bytes)'.</x>
    </jingle>
  </iq>
  <iq to="john.smith@example.com/resource_name1" id="uid:565ff8ee:000033ec:00000712" type="result"/>

  <iq from="john.smith@example.com/resource_name1" id="uid:565ff8ee:000075d4:00000713" to="adam.smith@example.com/resource_name" type="set" xml:lang="en">
    <si xmlns="http://jabber.org/protocol/si" id="uid:565ff8ee:00004ba5:00000714" profile="http://jabber.org/protocol/si/profile/file-transfer">
      <file xmlns="http://jabber.org/protocol/si/profile/file-transfer" name="20151204_150015198.png" size="33363"/>
      <feature xmlns="http://jabber.org/protocol/feature-neg">
        <x xmlns="jabber:x:data" type="form">
          <field type="list-single" var="stream-method">
            <option label="ibb">
              <value>http://jabber.org/protocol/ibb</value>
            </option>
            <option label="oob">
              <value>jabber:iq:oob</value>
            </option>
            <option label="s5b">
              <value>http://jabber.org/protocol/bytestreams</value>
            </option>
            <value/>
          </field>
        </x>
      </feature>
    </si>
  </iq>
  <iq to="john.smith@example.com/resource_name1" id="uid:565ff8ee:000075d4:00000713" type="result">
    <si xmlns="http://jabber.org/protocol/si">
      <feature xmlns="http://jabber.org/protocol/feature-neg">
        <x xmlns="jabber:x:data" type="submit">
          <field type="text-single" var="stream-method">
            <value>http://jabber.org/protocol/bytestreams</value>
          </field>
        </x>
      </feature>
    </si>
  </iq>

  <iq from="john.smith@example.com/resource_name1" id="uid:565ff8ee:00005649:00000715" to="adam.smith@example.com/resource_name" type="set" xml:lang="en">
    <query xmlns="http://jabber.org/protocol/bytestreams" mode="tcp" sid="uid:565ff8ee:00004ba5:00000714">
      <streamhost host="169.254.9.158" jid="john.smith@example.com/resource_name1" port="37200"/>
      <streamhost host="10.154.34.85" jid="john.smith@example.com/resource_name1" port="37200"/>
      <streamhost host="10.154.56.115" jid="john.smith@example.com/resource_name1" port="37200"/>
      <streamhost host="isj7jft602-s.webexconnect.com" jid="proxy.isj7.webex.com" port="443"/>
    </query>
  </iq>
  <iq to="john.smith@example.com/resource_name1" from="adam.smith@example.com/resource_name" id="uid:565ff8ee:00005649:00000715" type="result">
    <query xmlns="http://jabber.org/protocol/bytestreams" sid="uid:565ff8ee:00004ba5:00000714">
      <streamhost-used jid="proxy.isj7.webex.com"/>
    </query>
  </iq>

  <iq to="john.smith@example.com/resource_name1" id="uid:56619a8f:000032ef:0000004c" type="set">
    <si xmlns="http://jabber.org/protocol/si" profile="http://jabber.org/protocol/si/profile/file-transfer" status="0" notifyid="uid:565ff8ee:00004ba5:00000714"/>
  </iq>

  <iq from="john.smith@example.com/resource_name1" id="uid:565ff8ee:00003604:00000717" to="adam.smith@example.com/resource_name" type="set" xml:lang="en">
    <jingle xmlns="urn:xmpp:jingle:1" action="session-terminate" initiator="adam.smith@example.com/resource_name" responder="john.smith@example.com/resource_name1" sid="SID4">
      <content creator="initiator">
        <description xmlns="http://webex.com/connect/commands" num="141"/>
      </content>
      <x xmlns="http://protocols.cisco.com/csg/log-notification">Successfully sent file '20151204_150015198.png(33363 bytes)'.</x>
    </jingle>
  </iq>
  <iq to="john.smith@example.com/resource_name1" id="uid:565ff8ee:00003604:00000717" type="result"/>

  <iq from="john.smith@example.com/resource_name1" id="uid:565ff8ee:00007b2a:0000070f" to="adam.smith@example.com/resource_name" type="set" xml:lang="en">
    <si xmlns="http://jabber.org/protocol/si" notifyid="uid:56619a8f:00004217:00000047" profile="http://jabber.org/protocol/si/profile/file-transfer" status="0"/>
  </iq>
'''

ROSTER = b'''
<iq from='adam@example.com/adam_resource' id='id007' type='result'>
    <query xmlns='jabber:iq:roster'>
        <item jid='ewa@example.com' name='Ewa Smith' subscription='both'><group>Friends</group></item>
    </query>
</iq>
'''

P = b'''
<presence from='adam@example.com/adam_resource'>
    <priority>5</priority>
</presence>
'''
P_changed = b'''
<presence from='adam@example.com/adam_resource'>
    <priority>5</priority>
    <x xmlns="vcard-temp:x:update"><photo>8a568f38ff0236a157497a4e10b3b8d078c0f40a</photo></x>
</presence>
'''
P_changed_with_nick = b'''
<presence from='adam@example.com/adam_resource'>
    <priority>5</priority>
    <x xmlns="vcard-temp:x:update"><photo>8a568f38ff0236a157497a4e10b3b8d078c0f40a</photo></x>
    <nick xmlns="http://jabber.org/protocol/nick">Adam Smith</nick>
</presence>
'''
P_with_photo = b'''
<presence from='adam@example.com/adam_resource'>
    <priority>5</priority>
    <x xmlns="vcard-temp:x:update"><photo>9999999999999999999999999999999999999999</photo></x>
</presence>
'''

BIND = b'''
<iq id='id009' type='result'>
    <bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'>
        <jid>adam@example.com/adam_resource</jid>
    </bind>
</iq>
'''


VERSION = '+8Ybz686j5Ht/yiuQEcYX19hOsE='
ORG_VERSION = 'aaaaaaaaaaaaaaaaaaaaaaaaaaa='
SELF_PUBLISH_CAPS = bytes('''
<presence>
    <priority>1</priority>
    <c node='http://pidgin.im/' xmlns='http://jabber.org/protocol/caps' hash='sha-1' ver='%s'/>
</presence>
''' % ORG_VERSION, 'utf-8')
SELF_PUBLISH_CAPS_MOD = bytes('''
<presence>
    <priority>1</priority>
    <c node='http://pidgin.im/' xmlns='http://jabber.org/protocol/caps' hash='sha-1' ver='%s'/>
</presence>
''' % VERSION, 'utf-8')


def CLIENT_FEATURES_REQ(s):
    return bytes('''
<iq from='adam@example.com' id='id008' to='adam@example.com/adam_resource' type='get'>
    <query node='http://pidgin.im/#%s' xmlns='http://jabber.org/protocol/disco#info'/>
</iq>
''' % s, 'utf-8')


def CLIENT_FEATURES_RESP(*args):
    return bytes('''
<iq to='adam@example.com' id='id008' from='adam@example.com/adam_resource' type='result'>
    <query node='http://pidgin.im/#%s' xmlns='http://jabber.org/protocol/disco#info'>
      <identity category='client' type='pc' name='%s'/>
      %s
    </query>
</iq>
''' % args, 'utf-8')


def LOCALHOST_REQ(*args):
    return bytes('''
<iq xmlns="jabber:client" type="get" to="%s" id="id005">
    <query xmlns="http://jabber.org/protocol/%s"/>
</iq>
''' % args, 'utf-8')


def LOCATION_RESP(*args):
    return bytes('''
<iq xmlns="jabber:client" type='result' from='%s' id="id005">
    <query xmlns="http://jabber.org/protocol/%s">...</query>
</iq>
''' % args, 'utf-8')


def consvr_SERVER_INFO(s=''):
    return bytes('''
<iq from='consvr.isj7.webex.com' id='id04' to='adam@example.com/adam_resource' type='result'>
    <query xmlns='http://jabber.org/protocol/disco#info'>
        <identity category='component' name='Connection Server Manager' type='generic'/>
        <feature var='http://jabber.org/protocol/disco#info'/>
        <feature var='http://jabber.org/protocol/disco#items'/>
        <feature var='http://webex.com/connect/cs'/>%s
    </query>
</iq>
''' % s, 'utf-8')


def DOMAIN_INFO(s):
    return bytes('''
<iq from='example.com' id='id03' to='adam@example.com/adam_resource' type='result'>
    <query xmlns='http://jabber.org/protocol/disco#info'>
        <identity category='server' name='jabberd 7.28.1.41644' type='im'/>
        <feature var='http://jabber.org/protocol/disco#items'/>
        <feature var='http://jabber.org/protocol/disco#info'/>
        <feature var='urn:xmpp:sm:3'/>
        <feature var='http://protocols.cisco.com/mdm:1'/>
        <feature var='http://protocols.cisco.com/push:2'/>
        <feature var='jabber:iq:roster'/>
        <feature var='jabber:iq:time'/>
        <feature var='urn:xmpp:time'/>
        <feature var='jabber:iq:version'/>
        <feature var='urn:xmpp:sift:1'/>
        <feature var='urn:xmpp:sift:recipients:all'/>
        <feature var='urn:xmpp:sift:senders:all'/>
        <feature var='urn:xmpp:sift:senders:others'/>
        <feature var='urn:xmpp:sift:senders:self'/>
        <feature var='urn:xmpp:sift:stanzas:presence'/>
        <feature var='urn:xmpp:sift:stanzas:message'/>
        <feature var='urn:xmpp:sift:stanzas:iq'/>
        <feature var='jabber:iq:privacy'/>
        <feature var='jabber:iq:private'/>
        <feature var='http://webex.com/connect/temp-presence'/>
        <x type='result' xmlns='jabber:x:data'>
            <field type='hidden' var='FORM_TYPE'><value>http://webex.com/connect/temp-presence</value></field>
            <field label='Outbound Limit' var='temp-presence#max-outbound-subs'><value>750</value></field>
            <field label='Inbound Limit' var='temp-presence#max-inbound-subs'><value>2000</value></field>
        </x>
        <identity category='pubsub' type='pep'/>
        <feature var='http://jabber.org/protocol/pubsub#access-presence'/>
        <feature var='http://jabber.org/protocol/pubsub#auto-create'/>
        <feature var='http://jabber.org/protocol/pubsub#auto-subscribe'/>
        <feature var='http://jabber.org/protocol/pubsub#create-nodes'/>
        <feature var='http://jabber.org/protocol/pubsub#create-and-configure'/>
        <feature var='http://jabber.org/protocol/pubsub#config-node'/>
        <feature var='http://jabber.org/protocol/pubsub#delete-nodes'/>
        <feature var='http://jabber.org/protocol/pubsub#filtered-notifications'/>
        <feature var='http://jabber.org/protocol/pubsub#item-ids'/>
        <feature var='http://jabber.org/protocol/pubsub#last-published'/>
        <feature var='http://jabber.org/protocol/pubsub#manage-subscriptions'/>
        <feature var='http://jabber.org/protocol/pubsub#member-affiliation'/>
        <feature var='http://jabber.org/protocol/pubsub#modify-affiliations'/>
        <feature var='http://jabber.org/protocol/pubsub#presence-notifications'/>
        <feature var='http://jabber.org/protocol/pubsub#publish'/>
        <feature var='http://jabber.org/protocol/pubsub#purge-nodes'/>
        <feature var='http://jabber.org/protocol/pubsub#retract-items'/>
        <feature var='http://jabber.org/protocol/pubsub#retrieve-affiliations'/>
        <feature var='http://jabber.org/protocol/pubsub#retrieve-default'/>
        <feature var='http://jabber.org/protocol/pubsub#retrieve-items'/>
        <feature var='http://jabber.org/protocol/pubsub#subscription-notifications'/>
        <feature var='http://jabber.org/protocol/pubsub#persistent-items'/>%s
    </query>
</iq>
''' % s, 'utf-8')


def CLIENT_CAPABILITIES(s=''):
    return bytes('''
<iq from='adam@example.com/adam_resource' id='id02' to='example.com' type='result'>
    <query xmlns='http://jabber.org/protocol/disco#info' node='some_node'>
        <identity category='client' name='Client Name' type='pc' />
        <feature var='http://jabber.org/protocol/si'/>
        <feature var='http://jabber.org/protocol/bytestreams'/>%s
    </query>
</iq>
''' % s, 'utf-8')


SEARCH_FIELDS = b'''
<iq type='get' id='id04' to='consvr.isj7.webex.com'><query xmlns='jabber:iq:search'/></iq>
'''
SEARCH_FIELDS_RESP = b'''
<iq type='result' id='id04' from='consvr.isj7.webex.com' to='adam@example.com'>
    <query xmlns='jabber:iq:search'>
        <instructions>Provide user name for any matching Jabber users.</instructions>
        <first/>
        <last/>
        <nick/>
        <email/>
    </query>
</iq>
'''
SEARCH_USER = b'''
<iq type='set' id='id009' to='consvr.isj7.webex.com'>
    <query xmlns='jabber:iq:search'><nick>asmith</nick></query>
</iq>
'''
SEARCH_FWD = b'''
<iq type='set' id='id009' to='consvr.isj7.webex.com'>
    <query xmlns='jabber:iq:search'>
    <x xmlns='jabber:x:data' type='submit'>
        <field var='FORM_TYPE' type='hidden'><value>http://webex.com/connect/cs</value></field>
        <field var='psusername'><value>asmith</value></field>
        <field var='count'><value>21</value></field>
    </x>
    </query>
</iq>
'''
VCARD_REQ = b'''
<iq xmlns="jabber:client" type="get" to="ewa@example.com" id="id006">
    <vCard xmlns="vcard-temp"/>
</iq>
'''
VCARD_REQ_FWD = b'''
<iq id="id006" to="consvr.isj7.webex.com" type="set">
    <query xmlns="jabber:iq:search">
        <x type="submit" xmlns="jabber:x:data">
            <field type="hidden" var="FORM_TYPE"><value>http://webex.com/connect/cs</value></field>
            <field var="jidoremail"><value>ewa@example.com</value></field>
        </x>
    </query>
</iq>
'''

def VCARD_RESP(s):
    return bytes('''
<iq componentid='...' from='consvr.isj7.webex.com' to='adam@example.com' id='id006' type='result'>
    <query xmlns='jabber:iq:search'>
        <x type='result' xmlns='jabber:x:data'>
            <field type='hidden' var='FORM_TYPE'>
                <value>http://webex.com/connect/gs</value>
            </field>
            <reported>
                <field label='Email' type='text-single' var='email'/>
                <field label='SSO ID' type='text-single' var='ssoid'/>
                <field label='Display Name' type='text-single' var='displayname'/>
                <field label='First Name' type='text-single' var='firstname'/>
                <field label='Last Name' type='text-single' var='lastname'/>
                <field label='Phone Number (home)' type='text-single' var='phonenum_home'/>
                <field label='Phone Number (mobile)' type='text-single' var='phonenum_mobile'/>
                <field label='Phone Number (office)' type='text-single' var='phonenum_office'/>
                <field label='Phone Number (csf)' type='text-single' var='phonenum_csf'/>
                <field label='Picture URL' type='text-single' var='pictureurl'/>
                <field label='User Name' type='text-single' var='username'/>
                <field label='Job Title' type='text-single' var='jobtitle'/>
                <field label='JID' type='text-single' var='jid'/>
                <field label='User ID' type='text-single' var='userid'/>
            </reported>
            <item>
                <field var='email'><value>ewa@example.com</value></field>
                <field var='ssoid'><value>esmith</value></field>
                <field var='displayname'><value>Ewa Smith</value></field>
                <field var='firstname'><value>Ewa</value></field>
                <field var='lastname'><value>Smith</value></field>
                <field var='phonenum_home'><value/></field>
                <field var='phonenum_mobile'><value>555777555</value></field>
                <field var='phonenum_office'><value>555888555</value></field>
                <field var='phonenum_csf'><value>888555</value></field>
                <field var='pictureurl'><value>%s</value></field>
                <field var='username'><value>ewa@example.com</value></field>
                <field var='jobtitle'><value/></field>
                <field var='jid'><value>ewa@example.com</value></field>
                <field var='userid'><value>U5RFMG0U7EMEF4AOJWDUMJU86I</value></field>
            </item>
        </x>
    </query>
</iq>
''' % s, 'utf-8')


VCARD_RESP_FWD = b'''
<iq from="ewa@example.com" to="adam@example.com" type="result" id="id006">
    <vCard xmlns="vcard-temp">
        <FN>Ewa Smith</FN>
        <N><FAMILY>Smith</FAMILY><GIVEN>Ewa</GIVEN></N>
        <EMAIL><INTERNET/><PREF/><USERID>ewa@example.com</USERID></EMAIL>
        <NICKNAME>esmith</NICKNAME>
        <JABBERID>ewa@example.com</JABBERID>
        <TEL><WORK/><VOICE/><NUMBER>555888555</NUMBER></TEL>
        <TEL><CELL/><VOICE/><NUMBER>555777555</NUMBER></TEL>
    </vCard>
</iq>
'''
USER_SEARCH_RESP = b'''
<iq id="id006" to="adam@example.com" type="result" from="consvr.isj7.webex.com">
    <query xmlns="jabber:iq:search">
        <item jid="ewa@example.com">
            <nick>esmith</nick>
            <email>ewa@example.com</email>
            <last>Smith</last>
            <first>Ewa</first>
        </item>
    </query>
</iq>
'''


class TS(unittest.TestCase):
    def setUp(self):
        ''' options '''
        self.experimental = True
        self.cisco_features = True
        self.organization = 'example.com'
        self.local_mes_storage = 'storage.pkl'
        self.get_logger = lambda *x: logging
        ''' parent '''
        self.options = self
        ''' sut '''
        self.sut = decisco.extensions.extra.ExtraStuff(self, self)
        dloader.load(self.sut)
        ''' outputs '''
        self.out_server = unittest.mock.MagicMock()
        self.out_client = unittest.mock.MagicMock()
        self.out_server.schedule = unittest.mock.MagicMock()
        self.out_client.schedule = unittest.mock.MagicMock()

    def assert_no_calls(self, on):
        # print(on.schedule.call_args_list)
        self.assertEqual(0, len(on.schedule.call_args_list))

    def assert_calls(self, on, *args):
        on = on.schedule
        parser = decisco.jnod.Parser()
        expected_data = [parser.parse(d) for d in args]
        for d in on.call_args_list:
            self.assertIsInstance(d[0][0], bytes)
        data = [parser.parse(d[0][0]) for d in on.call_args_list]
        # print("expected_data=", expected_data)
        # print("data=", data)
        self.assertEqual(expected_data, data)
        on.reset_mock()

    def invoke_server(self, data: bytes):
        ret = self.sut.process_experimental(data, self.sut.SERVER, self.out_client, self.out_server)
        if not ret:
            self.out_client.schedule(data)
        return ret

    def invoke_client(self, data: bytes):
        ret = self.sut.process_experimental(data, self.sut.CLIENT, self.out_server, self.out_client)
        if not ret:
            self.out_server.schedule(data)
        return ret

    def test_screenshot_simple(self):
        self.assertTrue(self.invoke_server(SCREENSHOT_MESSAGE))
        self.assert_no_calls(self.out_client)
        self.assert_no_calls(self.out_server)

        self.assertTrue(self.invoke_server(SCREENSHOT_FILETRANSFER_PUSH))
        self.assert_no_calls(self.out_client)
        self.assert_calls(self.out_server, SCREENSHOT_FILETRANSFER_PUSH_RESP)

        self.assertTrue(self.invoke_server(STREAMHOST_SELECTED))
        self.assert_no_calls(self.out_client)
        self.assert_calls(self.out_server, STREAMHOST_USED)

    def test_roster_remember_roster(self):
        self.assertFalse(self.invoke_server(ROSTER))
        self.assert_calls(self.out_client, ROSTER)
        self.assert_no_calls(self.out_server)
        self.assertEqual(1, len(self.sut.roster_list))
        jid = 'ewa@example.com'
        self.assertIn(jid, self.sut.roster_list)
        self.assertIn(jid, self.sut.roster_list[jid].jid)
        self.assertIn('Friends', self.sut.roster_list[jid].group)
        self.assertIn('Ewa Smith', self.sut.roster_list[jid].name)
        self.assertIn('both', self.sut.roster_list[jid].subscription)

    def test_roster_presence_user_not_in_roster(self):
        self.assertFalse(self.invoke_server(P))
        self.assert_calls(self.out_client, P)
        self.assert_no_calls(self.out_server)

    def test_roster_presence_user_in_roster_not_name_update(self):
        import decisco.extensions.roster
        jid = 'adam@example.com'
        roster_element = decisco.extensions.roster.RosterElement(jid, 'asmith', None, 'Friends')
        self.sut.roster_list[jid] = roster_element
        self.assertTrue(self.invoke_server(P))
        self.assert_calls(self.out_client, P_changed)
        self.assert_no_calls(self.out_server)

    def test_roster_presence_user_in_roster_name_update(self):
        import decisco.extensions.roster
        jid = 'adam@example.com'
        roster_element = decisco.extensions.roster.RosterElement(jid, 'asmith', None, 'Friends')
        roster_element.name = 'Adam Smith'
        roster_element.update_name = True
        self.sut.roster_list[jid] = roster_element
        self.assertTrue(self.invoke_server(P))
        self.assert_calls(self.out_client, P_changed_with_nick)
        self.assert_no_calls(self.out_server)

    def test_roster_presence_user_in_roster_with_photo(self):
        import decisco.extensions.roster
        jid = 'adam@example.com'
        roster_element = decisco.extensions.roster.RosterElement(jid, 'asmith', None, 'Friends')
        self.sut.roster_list[jid] = roster_element
        self.assertFalse(self.invoke_server(P_with_photo))
        self.assert_calls(self.out_client, P_with_photo)
        self.assert_no_calls(self.out_server)
        self.assertEqual('9999999999999999999999999999999999999999', roster_element.avatar)

    def test_discovery_bind_resource_to_jid(self):
        self.assertFalse(self.invoke_server(BIND))
        self.assert_calls(self.out_client, BIND)
        self.assert_no_calls(self.out_server)
        self.assertEqual('adam@example.com/adam_resource', self.sut.bindedjid)

    def test_cisco_self_public_client_capabilities(self):
        self.assertTrue(self.invoke_client(SELF_PUBLISH_CAPS))
        self.assert_calls(self.out_server, SELF_PUBLISH_CAPS_MOD)
        self.assert_no_calls(self.out_client)

    def test_cisco_capabilities_request_forward_modified(self):
        import decisco.extensions.cisco
        info = self.sut.get_container(decisco.extensions.cisco.UserCapabilities)
        info.caps_node = 'http://pidgin.im/'
        info.decisco_ver_string = VERSION
        info.org_ver_string = ORG_VERSION

        self.assertTrue(self.invoke_server(CLIENT_FEATURES_REQ(VERSION)))
        self.assert_calls(self.out_client, CLIENT_FEATURES_REQ(ORG_VERSION))
        self.assert_no_calls(self.out_server)

    def test_cisco_capabilities_response_forward_modified(self):
        import decisco.extensions.cisco
        import decisco.extensions.verificationstring
        info = self.sut.get_container(decisco.extensions.cisco.UserCapabilities)
        info.caps_node = 'http://pidgin.im/'
        info.decisco_ver_string = VERSION
        info.org_ver_string = ORG_VERSION
        info.caps_request_id.add('id008')

        feature_var = 'http://jabber.org/protocol/disco#items'
        feature = "<feature var='%s'/>" % feature_var
        client_name = 'Client Name'
        ret_features = decisco.extensions.verificationstring.DEFAULT_CAPABILITIES + info.CISCO_CAPS
        ret_features = '    %s\n'.join(["<feature var='%s'/>" % f for f in sorted(ret_features)])
        self.assertTrue(self.invoke_client(CLIENT_FEATURES_RESP(ORG_VERSION, client_name, feature)))
        self.assert_calls(self.out_server, CLIENT_FEATURES_RESP(VERSION, 'Decisco (Unknown)', ret_features))
        self.assert_no_calls(self.out_client)
        self.assertEqual(1, len(info.identity))
        self.assertEqual(1, len(info.features))
        self.assertEqual('Client Name', info.identity[0].name)
        self.assertEqual(feature_var, info.features[0])

    def test_discovery_request_to_localhost(self):
        service_names = ['disco#info', 'disco#items']
        for sn in service_names:
            self.discovery_request_to_localhost(sn)

    def discovery_request_to_localhost(self, service_name):
        loc = 'localhost'
        self.assertTrue(self.invoke_client(LOCALHOST_REQ(loc, service_name)))
        self.assert_calls(self.out_server, LOCALHOST_REQ(self.organization, service_name))
        self.assert_no_calls(self.out_client)
        self.assertTrue(self.sut.localhost_disco_requests)

    def test_discovery_response_to_localhost(self):
        service_names = ['disco#info', 'disco#items']
        for sn in service_names:
            self.discovery_response_to_localhost(sn)

    def discovery_response_to_localhost(self, service_name):
        loc = 'localhost'
        self.sut.localhost_disco_requests = True
        self.assertTrue(self.invoke_server(LOCATION_RESP(self.organization, service_name)))
        self.assert_calls(self.out_client, LOCATION_RESP(loc, service_name))
        self.assert_no_calls(self.out_server)

    def test_discovery_add_info_for_consvr_isj7_webex_com(self):
        extra = "<feature var='jabber:iq:search'/><identity category='directory' type='user'/>"
        self.assertTrue(self.invoke_server(consvr_SERVER_INFO()))
        self.assert_calls(self.out_client, consvr_SERVER_INFO(extra))
        self.assert_no_calls(self.out_server)

    def test_discovery_domain_info(self):
        self.assertTrue(self.invoke_server(DOMAIN_INFO('')))
        self.assert_calls(self.out_client, DOMAIN_INFO("<feature var='vcard-temp'/>"))
        self.assert_no_calls(self.out_server)

    def test_discovery_expand_client_caps(self):
        caps = [const.cisco_desktopshare, const.webex_ds, const.webex_picture_share, const.webex_picture_share_mix]
        extra = '    %s\n'.join('<feature var="%s"/>' % f for f in caps)
        self.sut.bindedjid = 'adam@example.com/adam_resource'
        self.assertTrue(self.invoke_client(CLIENT_CAPABILITIES()))
        self.assert_calls(self.out_server, CLIENT_CAPABILITIES(extra))
        self.assert_no_calls(self.out_client)

    def test_search_get_search_fields(self):
        self.sut.jabberid = 'adam@example.com'
        self.assertTrue(self.invoke_client(SEARCH_FIELDS))
        self.assert_calls(self.out_client, SEARCH_FIELDS_RESP)
        self.assert_no_calls(self.out_server)

    def test_search_for_user(self):
        self.sut.jabberid = 'adam@example.com'
        self.assertTrue(self.invoke_client(SEARCH_USER))
        self.assert_calls(self.out_server, SEARCH_FWD)
        self.assert_no_calls(self.out_client)
        self.assertIn('id009', self.sut.not_vcard)

    def test_vcard_request(self):
        self.assertTrue(self.invoke_client(VCARD_REQ))
        self.assert_calls(self.out_server, VCARD_REQ_FWD)
        self.assert_no_calls(self.out_client)

    def test_vcard_response_no_image(self):
        self.assertTrue(self.invoke_server(VCARD_RESP('')))
        self.assert_calls(self.out_client, VCARD_RESP_FWD)
        self.assert_no_calls(self.out_server)

    @unittest.mock.patch('decisco.extensions.imagedesc.ImageDescription')
    def test_vcard_response_with_image(self, img_descriptor):
        img = 'https://dms.webexconnect.com:443/.../ewa.smith.jpg'
        self.assertTrue(self.invoke_server(VCARD_RESP(img)))
        self.assert_no_calls(self.out_client)
        self.assert_no_calls(self.out_server)
        img_descriptor.assert_called_with(unittest.mock.ANY, self.sut)
        img_descriptor.return_value.run.assert_called_once_with()
        vcard_info = img_descriptor.call_args_list[0][0][0]
        self.assertEqual(img, vcard_info.pictureurl)

    def test_vcard_response_for_user_search(self):
        frm = 'ewa@example.com'
        self.sut.not_vcard.add('id006')
        self.sut.roster_list[frm] = decisco.extensions.roster.RosterElement(frm, frm, 'both', 'Friends')
        img = 'https://dms.webexconnect.com:443/.../ewa.smith.jpg'
        self.assertTrue(self.invoke_server(VCARD_RESP(img)))
        self.assert_calls(self.out_client, USER_SEARCH_RESP)
        self.assert_no_calls(self.out_server)
        # img_descriptor.assert_called_with(unittest.mock.ANY, self.sut)
        # img_descriptor.return_value.run.assert_called_once_with()
        # vcard_info = img_descriptor.call_args_list[0][0][0]
        # self.assertEqual(img, vcard_info.pictureurl)
