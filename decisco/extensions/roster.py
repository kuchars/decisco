import hashlib
import decisco.jnod as jnod
import decisco.extensions.const as const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes


class RosterElement(object):
    def __init__(self, jid, name, sub, grp):
        object.__init__(self)
        self.jid = jid
        self.name = name
        self.group = grp
        self.subscription = sub
        self.avatar = None
        self.update_name = False
        self.vcard_avatar = None

    def get_vcard_avatar(self):
        if self.vcard_avatar is None:
            o = hashlib.new('sha1')
            o.update(self.jid.encode('ascii', errors='ignore'))
            self.vcard_avatar = o.hexdigest()
        return self.vcard_avatar


def parse_roster(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='john.smith@example.com/kde-telepathy-464946' id='540727704885' type='result'>
    <query xmlns='jabber:iq:roster'>...
    '''
    if data.Tag != 'iq' or not data.has_attr('type', value='result'):
        return
    query = data.node('query')
    if query is None or not query.has_attr('xmlns', value='jabber:iq:roster'):
        return
    for item_node in query.childs:
        if item_node.Tag != 'item':
            continue
        jid = item_node.get('jid', None)
        name = item_node.get('name', None)
        sub = item_node.get('subscription', None)
        grp = item_node.node('group')
        grp = None if grp is None else grp.text
        re = RosterElement(jid, name, sub, grp)
        if re.jid is None:
            extra.log.info("jid is empty for roster item: %s", item_node)
            continue
        extra.roster_list[re.jid] = re
    extra.log.debug("loaded roster info %s - jids", len(extra.roster_list))


def alter_presence(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <presence from='adam@example.com/adam_resource' to='ewa@example.com/ewa_resource'>
        <priority>1</priority>
        <c ext='voice-v1 camera-v1 video-v1' hash='sha-1' node='http://pidgin.im/'
            ver='AcN1/PEN8nq7AHD+9jpxMV4U6YM=' xmlns='http://jabber.org/protocol/caps'/>
        <x xmlns='vcard-temp:x:update'><photo>a518f361cb0e08d394b8605cd3c2211c01850a78</photo></x>
        <x from='adam@example.com/adam_resource' stamp='2018-05-23T06:07:02Z' xmlns='jabber:x:delay'/>
    </presence>
    '''
    if data.Tag != 'presence' or not data.childs or not data.has_attr('from'):
        return
    jid = data.get('from').rsplit('/', 1)[0]
    if jid not in extra.roster_list:
        return
    re = extra.roster_list[jid]
    # if re.vcard_avatar is not None or re.avatar is not None or re.update_name is False:
    #     return
    x = data.node('x', {'xmlns': 'vcard-temp:x:update'})
    altered = False
    if x is not None:
        photo = x.node('photo')
        if photo is not None:
            if photo.text:
                ''' update avatar info '''
                re.avatar = photo.text
            elif re.vcard_avatar is None:
                photo.set_text(re.get_vcard_avatar())
                altered = True
        elif re.vcard_avatar is None and re.avatar is None:
            photo = x.add_child('photo')
            photo.set_text(re.get_vcard_avatar())
            altered = True
    elif re.vcard_avatar is None and re.avatar is None:
        x = data.add_child('x').set_attr('xmlns', 'vcard-temp:x:update')
        x.add_child('photo').set_text(re.get_vcard_avatar())
        altered = True
    if re.update_name:
        re.update_name = False
        nick = data.node('nick')
        if nick is None:
            nick = data.add_child('nick').set_attr('xmlns', const.nick)
        extra.log.info("Changing nick '%s' -> '%s'" % (jid, re.name))
        nick.set_text(re.name)
        altered = True
    if altered:
        return dtypes.SendForward(data)


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_server_handler(parse_roster, alter_presence)
    extra.register_client_handler()
