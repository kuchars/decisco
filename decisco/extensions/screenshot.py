import hashlib
import decisco.jnod as jnod
import decisco.extensions.const as const
import decisco.extensions.extra as dextra
import decisco.extensions.types as detypes


class ScreenShotTransfer(object):
    def __init__(self, img_id, msg_node):
        object.__init__(self)
        self.img_id = img_id
        self.msg_node = msg_node
        self.si_ft_sessionId = None
        self.si_jid = None
        self.si_host = None
        self.si_port = None


class ScreenShotDownloads(object):
    def __init__(self, *args):
        object.__init__(self)
        self.img_ids = {}
        self.sessions = {}

    def register_screenshot(self, img_id, data):
        self.img_ids[img_id] = ScreenShotTransfer(img_id, data)

    def register_session_id(self, img_id, si_sessionId):
        obj = self.img_ids[img_id]
        obj.si_ft_sessionId = si_sessionId
        self.sessions[si_sessionId] = obj

    def set_transfer(self, si_sessionId, jid, host, port):
        obj = self.sessions[si_sessionId]
        obj.si_jid = jid
        obj.si_host = host
        obj.si_port = port


def translate_screenshot_message(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <message from='...' id='...' to='...' type='chat' xml:lang='en'>
      <body></body>
      <x type='330' xmlns='http://webex.com/connect/imcmd'/>
      <html xmlns='http://jabber.org/protocol/xhtml-im'>
        <body xmlns='http://www.w3.org/1999/xhtml'>
          <span ...>
            <div>
              <div ...>
                <img alt='Screen capture' id='img_id' name='connect_screen_capture' src='...'/>
              </div>
            </div>
          </span>
        </body>
      </html>
    </message>
    '''
    '''
    <message from='...' to='...' type='chat'>
      <body>Screen capture</body>
      <html xmlns='http://jabber.org/protocol/xhtml-im'>
        <body xmlns='http://www.w3.org/1999/xhtml'>
          <span ...>
            <div>
              <div ...>
                <img alt='A spot' src='cid:sha1+8f35fef110ffc5df08d579a50083ff9308fb6242@bob.xmpp.org'/>
              </div>
            </div>
          </span>
        </body>
      </html>
    </message>
    '''
    if data.Tag != 'message' or not data.has_attr('type', value='chat'):
        return
    html = data.node('html', {'xmlns': const.xhtml_im})
    if html is None:
        return
    img = data.node('img', attr={'name': 'connect_screen_capture', 'id': any}, recursive=True)
    if img is None:
        return
    img_id = img.get('id')
    if img.has_attr('alt'):
        body = data.node('body')
        if body is not None:
            body.text = ('' if body.text is None else (body.text + '. ')) + img.get('alt')
    img.remove_attr('id')
    img.remove_attr('name')
    img.set_attr('src', 'cid:sha1+%s@bob.xmpp.org')

    extra.get_container(ScreenShotDownloads).register_screenshot(img_id, data)
    return detypes.DoNothing()


def hide_screen_shot_upload(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='idn1' id='id_' to='idn2' type='set' xml:lang='en'>
      <si id='sessionId' profile='http://jabber.org/protocol/si/profile/file-transfer'
                xmlns='http://jabber.org/protocol/si'>
        <file name='registered_screeshot_id' size='\d+' transfer-type='picture-share'
                xmlns='http://jabber.org/protocol/si/profile/file-transfer'/>
        <feature xmlns='http://jabber.org/protocol/feature-neg'>
            <x type='form' xmlns='jabber:x:data'>
                <field type='list-single' var='stream-method'>
                    <option label='ibb'><value>http://jabber.org/protocol/ibb</value></option>
                    <option label='oob'><value>jabber:iq:oob</value></option>
                    <option label='s5b'><value>http://jabber.org/protocol/bytestreams</value></option>
                    <value/>
                </field>
            </x>
        </feature>
      </si>
    </iq>
    '''
    if data.Tag != 'iq' and not data.has_attribs(type='set', id=any):
        return
    si = data.node('si', attr={'id': any, 'profile': const.protocol_si_ft, 'xmlns': const.protocol_si})
    if si is None:
        return
    file = si.node('file', attr={'name': any, 'transfer-type': 'picture-share', 'xmlns': const.protocol_si_ft})
    if file is None:
        return
    feature = si.node('feature', attr={'xmlns': const.feature_neg})
    if feature is None:
        return
    x = feature.node('x', attr={'type': 'form', 'xmlns': 'jabber:x:data'})
    if x is None:
        return
    ''''
    <iq type='result' to='idn1' id='e0109adc:9a24:4e79:81ea:c9d0e196e7e2'>
        <si xmlns='http://jabber.org/protocol/si'>
            <feature xmlns='http://jabber.org/protocol/feature-neg'>
                <x xmlns='jabber:x:data' type='submit'>
                    <field var='stream-method'><value>http://jabber.org/protocol/bytestreams</value></field>
                </x>
            </feature>
        </si>
    </iq>
    '''
    extra.get_container(ScreenShotDownloads).register_session_id(file.get('name'), si.get('id'))
    id_ = data.get('id')
    to_ = data.get('from')
    ret = jnod.Node('iq', to=to_, id=id_, type='result')
    si = ret.add_child('si', xmlns=const.protocol_si)
    feature = si.add_child('feature', xmlns=const.feature_neg)
    x = feature.add_child('x', xmlns='jabber:x:data', type='submit')
    x.add_child('field', var='stream-method').add_child('value').set_text(const.bytestreams)
    return detypes.SendBack(ret)


def hide_screen_shot_upload2(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq from='idn1' id='id_' to='idn2' type='set' xml:lang='en'>
      <query mode='tcp' sid='sessionId' xmlns='http://jabber.org/protocol/bytestreams'>
        <streamhost host='isj7jft601-s.webexconnect.com' jid='proxy.isj7.webex.com' port='443'/>
      </query>
    </iq>
    '''
    if data.Tag != 'iq' and not data.has_attribs(type='set', id=any):
        return
    query = data.node('query', attr={'sid': any, 'xmlns': const.bytestreams})
    if query is None:
        return
    # <streamhost host='isj7jft601-s.webexconnect.com' jid='proxy.isj7.webex.com' port='443'/>
    streamhost = query.node('streamhost', attr={'host': any, 'jid': any})
    if streamhost is None:
        return
    id_ = data.get('id')
    to_ = data.get('from')
    sid = query.get('sid')
    jid = streamhost.get('jid')
    from_ = data.get('to', None)
    host = streamhost.get('host')
    port = int(streamhost.get('port', 1080))

    extra.get_container(ScreenShotDownloads).set_transfer(sid, jid, host, port)

    ret = jnod.Node('iq', to=to_, id=id_, type='result')
    if from_ is not None:
        ret.set_attr('from', from_)
    q = ret.add_child('query', xmlns=const.bytestreams, sid=sid)
    q.add_child('streamhost-used', jid=jid)
    return detypes.SendBack(ret)


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_container(ScreenShotDownloads)
    extra.register_server_handler(translate_screenshot_message, hide_screen_shot_upload, hide_screen_shot_upload2)
    extra.register_client_handler()
