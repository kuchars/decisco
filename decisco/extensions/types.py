import decisco.jnod


class PassThrough(object):
    def __init__(self, data):
        object.__init__(self)
        if isinstance(data, decisco.jnod.Node):
            data = data.to_bytes()
        else:
            assert isinstance(data, bytes)
        self.data = data

    def __repr__(self):
        return '%s<%s>' % (type(self).__name__, str(self.data))


class SendForward(PassThrough):
    pass


class SendBack(PassThrough):
    pass


class DoNothing(object):
    def __repr__(self):
        return '%s<>' % type(self).__name__
