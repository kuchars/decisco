import importlib
import decisco.extensions.extra as dextra
# extensions
import decisco.extensions.cisco as e_cisco
import decisco.extensions.vcard as e_vcard
import decisco.extensions.roster as e_roster
import decisco.extensions.search as e_search
import decisco.extensions.discovery as e_discovery
import decisco.extensions.screenshot as e_screenshot
import decisco.extensions.filetransfer as e_filetransfer
import decisco.extensions.messagequeue as e_messagequeue


def load(extra: dextra.ExtraStuff):
    if extra.options.cisco_features:
        e_cisco.register_extension(extra)
        e_screenshot.register_extension(extra)
    e_messagequeue.register_extension(extra)
    e_vcard.register_extension(extra)
    e_roster.register_extension(extra)
    e_search.register_extension(extra)
    e_discovery.register_extension(extra)
    e_filetransfer.register_extension(extra)
