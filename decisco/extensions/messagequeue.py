import os
import pickle
import datetime
import collections
import decisco.args
import decisco.jnod as jnod
import decisco.extensions.const as const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes


class OfflineMessage(object):
    def __init__(self, message: str, mes_id: str):
        object.__init__(self)
        self.message = message
        self.timestamp = datetime.datetime.now()
        self.message_id = mes_id

    def make_message_to_server(self, contact: str, from_contact: str):
        m = jnod.Node('message', type='chat', to=contact)
        m.set_attr('from', from_contact)
        m.add_child('body').set_text(self.message)
        ts = self.timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')
        m.add_child('delay', xmlns=const.delay, stamp=ts).set_attr('from', from_contact).set_text('Offline Storage')
        return m


class LocalOfflineStorage(object):
    def __init__(self, options: decisco.args.DeciscoArgs):
        object.__init__(self)
        self.offline_storage = os.path.abspath(os.path.expanduser(options.local_mes_storage))
        self.storage = collections.defaultdict(collections.deque)

    def _load(self):
        if not os.path.exists(self.offline_storage):
            return
        with open(self.offline_storage, 'rb') as f:
            self.storage = pickle.load(f)
        for rec, messages in self.storage.items():
            for dmes in messages:
                dmes.message_id = None

    def _save(self):
        with open(self.offline_storage, 'wb') as f:
            pickle.dump(self.storage, f)

    def add_message_to_queue(self, receipient: str, message: str, message_id: str):
        if '/' in receipient:
            receipient = receipient.split('/')[0]
        om = OfflineMessage(message, message_id)
        self.storage[receipient].append(om)
        print(om)


def catch_message_delivery_error(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    NOTE: Affected clients: all

    https://xmpp.org/extensions/xep-0203.html - ofline storage

    Cisco server do not allow sending messages to offline users - try to remember undelivered messages and resend them
    when contact becomes online

    <message from='john.smith@example.com' id='...' to='adam.smith@example.com/resource_name' type='error'>
        <body>Message Text ...</body>
        <active xmlns='http://jabber.org/protocol/chatstates'/>
        <error code='503' type='cancel'>
            <service-unavailable xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
        </error>
    </message>
    '''
    if data.Tag != 'message':
        return
    if not data.has_attr('type', value='error'):
        return
    err = data.node('error', type='cancel', code='503')
    if err is None:
        return
    feature = err.node('service-unavailable', xmlns=const.stanzas)
    if feature is None:
        return
    x = data.node('body')
    if x is None:
        return
    loc_store = extra.get_container(LocalOfflineStorage)
    loc_store.add_message_to_queue(data.get('from', None), x.text, data.get('id', None))
    return dtypes.DoNothing()  # suppress errornous response message
    # return dtypes.SendForward(str(data))


def contact_become_available(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <presence from='petri.jappila@nokia.com/jabber_28805' to='lukasz.kucharski@nokia.com' type='unavailable'/>

    <presence
            from='mateusz.legan@nokia.com/jabber_6086'
            to='lukasz.kucharski@nokia.com'
            xml:lang='en'>
        <show>away</show>
        <priority>0</priority>
        <x var='128' xmlns='http://webex.com/connect/customstatus'/>
        <systemstatuschange xmlns='urn:xmpp:systemstatuschange'/>
        <c hash='sha-1' node='http://protocols.cisco.com/jabber?v=12.0.1&amp;p=win' ver='Z4YwdiztnjoXaGjj71lMQC9KsNA=' xmlns='http://jabber.org/protocol/caps'/>
        <x from='mateusz.legan@nokia.com/jabber_6086' stamp='2019-01-02T11:33:00Z' xmlns='jabber:x:delay'/>
    </presence>
    <presence from='michal.orynicz@nokia.com/xmpp' to='lukasz.kucharski@nokia.com'>
    '''
    if data.Tag != 'presence':
        return
    contact = data.get('from', None)
    if contact is None:
        return
    if data.get('type', None) == 'unavailable':
        return
    if '/' in contact:
        contact = contact.split('/', 1)[0]
    loc_store = extra.get_container(LocalOfflineStorage)
    messages = loc_store.storage.get(contact, None)
    if messages is None or not messages:
        return
    while messages:
        assert isinstance(messages, collections.deque)
        dmesg = messages.popleft()
        assert isinstance(dmesg, OfflineMessage)
        to_cisco_server = dmesg.make_message_to_server(contact, extra.jabberid)
        extra.emit('altered_to_server', str(to_cisco_server))
    return


def register_extension(extra: dextra.ExtraStuff):
    extra.register_container(LocalOfflineStorage)
    extra.register_server_handler(catch_message_delivery_error, contact_become_available)
    # extra.register_client_handler(catch_message_delivery_error)


g = decisco.args.add_option_group('Local message storage')
g.add_argument(
    '--local-mes-storage', dest='local_mes_storage', default='~/.decisco_offline_storage.pkl',
    help='location of the file storing messages, %(default)s')


'''
https://xmpp.org/extensions/xep-0144.html#modify
<message from='horatio@denmark.lit' to='hamlet@denmark.lit'>
  <x xmlns='http://jabber.org/protocol/rosterx'>53
    <item action='modify'
          jid='rosencrantz@denmark.lit'
          name='Rosencrantz'>
      <group>Retinue</group>
    </item>
    <item action='modify'
          jid='guildenstern@denmark.lit'
          name='Guildenstern'>
      <group>Retinue</group>
    </item>
  </x>
</message>
'''