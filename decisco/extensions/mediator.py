import decisco.dsockets
import decisco.extensions.extra


class MediatorInfo(object):
    def __init__(self, *args):
        object.__init__(self)


class Mediator(object):
    def __init__(self):
        object.__init__(self)
        # port='443'
        # jid='proxy.isj7.webex.com'
        # host='isj7jft601-s.webexconnect.com'

    def mk_connect(self, host, port=443):
        pass


def register_extension(extra: decisco.extensions.extra.ExtraStuff):
    extra.register_container(MediatorInfo)
    extra.register_server_handler()
    extra.register_client_handler()


if __name__ == '__main__':
    class Opt(object):
        def __init__(self):
            object.__init__(self)
            self.AppName = 'Mediator'
            self.connected_directly = True
            self.proxy_host = None
            self.proxy_port = None
            self.use_proxy = False

    class T(object):
        def __init__(self):
            object.__init__(self)

        def addReadTrace(self, ret_data, name):
            pass

        def addSendTrace(self, send_data, name):
            pass

    # s.set_tcp_no_delay()
    # s.set_error_receiving()
    # s.set_tcp_user_timeout()
    import ssl
    import socket

    PORT = 443
    HOST = 'isj7jft601-s.webexconnect.com'

    S5B_CLIENT_INIT = b"\x05\x02\x00\x02"

    # s = socket.create_connection((HOST, PORT), 20)
    s = decisco.dsockets.ProxySslSocket(Opt(), name='mediator', tracer=T())
    s.connect((HOST, PORT))

    s.send(b"\x05\x02\x00\x02")

    while True:
        try:
            resp = s.recv()
            print("Data: %r" % resp)
            break
        except ssl.SSLWantReadError:
            pass
