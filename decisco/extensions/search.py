import decisco.jnod as jnod
import decisco.extensions.const
import decisco.extensions.extra as dextra
import decisco.extensions.types as dtypes
import decisco.extensions.utils as deutils


def get_search_fields(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq type='get' id='...' to='consvr.isj7.webex.com'><query xmlns='jabber:iq:search'/></iq>
    '''
    if data.Tag != 'iq' or not data.has_attribs(type='get', id=any, to=extra.users_directory):
        return
    q = data.node('query', {'xmlns': 'jabber:iq:search'})
    if q is None or q.childs:
        return
    to = data.get('to')
    i = data.get('id')
    resp = decisco.jnod.Node('iq').set_attr('from', to, type='result', to=extra.jabberid, id=i)
    q = resp.add_child('query', xmlns='jabber:iq:search')
    q.add_child('instructions').set_text("Provide user name for any matching Jabber users.")
    q.add_child('first')
    q.add_child('last')
    q.add_child('nick')
    q.add_child('email')
    extra.log.info("respond with %s" % resp)
    return dtypes.SendBack(resp)


def search_for_users(extra: dextra.ExtraStuff, data: jnod.Node):
    '''
    <iq type='set' id='purple5ea08cd9' to='consvr.isj7.webex.com'>
        <query xmlns='jabber:iq:search'><last>query_text</last></query>
    </iq>
    '''
    if data.Tag != 'iq' or not data.has_attribs(type='set', to=extra.users_directory):
        return
    q = data.node('query', {'xmlns': 'jabber:iq:search'})
    if q is None or not q.childs:
        return
    query_text = []
    to = data.get('to')
    for n in q.childs:
        if not n.text:
            continue
        query_text.append(n.text)

    i = data.get('id')
    extra.not_vcard.add(i)
    add_field = deutils.add_field
    fwd = jnod.Node('iq').set_attr(to=to, id=i, type='set')
    fq = fwd.add_child('query', xmlns='jabber:iq:search')
    x = fq.add_child('x', xmlns='jabber:x:data', type='submit')
    add_field(x, 'FORM_TYPE', type='hidden').set_text(decisco.extensions.const.webex_form_type)
    add_field(x, 'psusername').set_text(' '.join(query_text))
    add_field(x, 'count').set_text('21')
    return dtypes.SendForward(fwd)


def register_extension(extra: dextra.ExtraStuff):
    extra.register_module(__name__)
    extra.register_server_handler()
    extra.register_client_handler(get_search_fields, search_for_users)
