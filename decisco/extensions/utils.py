import decisco.jnod as jnod


def add_field(x: jnod.Node, var: str, **kwargs):
    return x.add_child('field').set_attr('var', var, **kwargs).add_child('value')
