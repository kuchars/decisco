import decisco.shared
import decisco.colors as colors
import decisco.dsockets as dsockets
import decisco.dexceptions as dexceptions


class Client2Decisco(object):
    def __init__(self, shared: decisco.shared.SharedResource, client: dsockets.Socket):
        object.__init__(self)
        self.shared = shared
        self.client = client
        self.auth_sended = False
        self.auth_feedback = None
        self.auth_feedback_failed = False
        self.log = self.shared.options.get_logger('client<->decisco')
        self.data = dsockets.OutBuffer(self.client, self.shared.selector, self.shared.options.jam_thres)
        self._to_ = self.shared.options.organization
        self.shared.register_slot('server_finished_auth', self.__server_finished_auth_slot)
        self.shared.register_slot('client_disconnect', self.__client_disconnect)
        self.shared.selector.register(self.client, self.client.name, self.ready_read, self.ready_write).\
            set_close_action(self.__on_socket_error)

    def __on_socket_error(self, sock: dsockets.Socket):
        self.__client_disconnect()
        self.shared.emit('stop_setup', delayed=True)

    def cleanup(self, socked_del=True):
        self.shared.selector.unregister(self.client)
        self.shared.unregister_slot('server_finished_auth')
        ret = self.client
        if not socked_del:
            self.client = None
        if self.client is not None:
            self.client.close()
        return ret

    def __client_disconnect(self):
        self.data.schedule(decisco.shared.get_connection_timeout_error())

    def __send_auth_response_to_client(self):
        if not self.auth_sended or self.auth_feedback is None:
            return
        self.data.schedule(self.auth_feedback)
        if self.auth_feedback_failed:
            self.data.schedule(b'</stream:stream>\n')
            ''' request renew of webex token in case it has expired '''
            self.shared.client_auth.need_renewal()
        else:
            data = self.data
            self.data = None
            self.shared.emit('start_forwarding', data)

    def __server_finished_auth_slot(self, data: bytes, failed: bool):
        assert isinstance(data, bytes)
        self.log.info("server finished authentication with %s", data)
        self.auth_feedback = data
        self.auth_feedback_failed = failed
        self.__send_auth_response_to_client()
        # self.data.schedule(data)

    def __get_server_hello_message(self, fromTo: str, id_val="0000") -> bytes:
        d = {'svr': fromTo, 'id': id_val}
        return bytes(
            '<?xml version="1.0"?>\n<stream:stream from="%(svr)s" id="%(id)s" '
            'xmlns:stream="http://etherx.jabber.org/streams" version="1.0" xmlns="jabber:client">\n' % d, 'utf-8')

    def __get_supported_auth(self) -> bytes:
        return bytes(
            '<stream:features><mechanisms xmlns="urn:ietf:params:xml:ns:xmpp-sasl"><mechanism>PLAIN'
            '</mechanism></mechanisms></stream:features>\n', 'utf-8')

    def __got_start_token(self, data: bytes):
        p = decisco.jnod.Parser()
        data = p.parse(data)
        if data.Tag != 'stream:stream':
            self.log.error("Error expected 'stream:stream' got %s", data.Tag)
            return
        self._to_ = data.get('to', self._to_)
        self.shared.emit('got_fromTo', self._to_)
        self.data.schedule(self.__get_server_hello_message(self._to_))
        self.data.schedule(self.__get_supported_auth())

    def __got_authentication_request(self, data: bytes):
        ''' Client send password, use that data if needed to acquire jabber configuration '''
        self.log.debug('Client: send auth data - skipping')
        try:
            conf = self.shared.client_auth.authenticated_client(data, self._to_)
            if conf is not None:
                self.log.info('emitting got_conf signal')
                self.shared.emit('got_conf', conf)
            else:
                ''' Failed to retrieve token '''
                self.log.info('do nothing - deadlock - should not happen')
                self.data.schedule(decisco.shared.get_connection_timeout_error())
                self.shared.emit('stop_setup', delayed=True)
            self.auth_sended = True
            self.__send_auth_response_to_client()
        except dexceptions.CouldNotRenewToken as e:
            self.log.error('Unable to renew token: %s', e)
            self.data.schedule(e.args[0])
            self.shared.emit('stop_setup', delayed=True)

    def __process_client_data(self, data: bytes):
        self.log.debug("%sClient->Decisco:\n%s%s", colors.cYellow, data, colors.cDefault)
        if b"<stream:stream" in data and b"to=" in data:
            self.__got_start_token(data)
        elif b"<auth" in data:
            self.__got_authentication_request(data)

    def ready_read(self):
        data = self.client.read()
        self.__process_client_data(data)

    def ready_write(self):
        self.data.send()
